<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;

class MedicalSupply extends Model
{
    
	protected $fillable = ['fabricante', 'numlote', 'motivo', 'dosis', 'fechavencimiento', 'fechainicio', 'fechafin', 'tipoinsumo', 'frecuencia', 'atc_id', 'notificacion_id', 'viadministracion_id', 'unidadmedida_id'];

	protected $table = "medical_supplies";

	 public $timestamps = false;

	public function notifications(){
        return $this->belongsTo('SINRAIM\Notification');
    }

    public function medica_lassagnas(){
        return $this->hasOne('SINRAIM\MedicaLassagna','medicalsupplie_id','id');
    }
}
