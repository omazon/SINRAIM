<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;

class AdverseReaction extends Model
{
    
	protected $fillable = ['fechainicio', 'fechafin', 'desenlace', 'intensidad', 'descripcion_id', 'notificacion_id'];

	protected $table = "adverse_reactions";

	public $timestamps = false;

	public function notifications(){
        return $this->belongsTo('SINRAIM\Notification');
    }
}
