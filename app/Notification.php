<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notification extends Model
{
    protected $table = "notifications";

    protected $fillable = ['estado', 'peso', 'ingresohospital', 'user_id', 'patient_id','hospital_id'];

    public function risks(){
        return $this->belongsToMany('SINRAIM\Risk','notification_risk','notificacion_id','risk_id');
    }

    public function medical_supplies(){
        return $this->hasMany('SINRAIM\MedicalSupply','notificacion_id','id');
    }

    public function adverse_reactions(){
        return $this->hasMany('SINRAIM\AdverseReaction','notificacion_id','id');
    }

    public static function vernotificacion()
    {
         return \DB::table('notifications')
                        ->join('medical_supplies', 'medical_supplies.notificacion_id', '=', 'notifications.id')
                        ->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')
                        ->join('adverse_reactions', 'adverse_reactions.notificacion_id', '=', 'notifications.id')
                        ->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')
                        ->select('notifications.*','notifications.id as idnotificacion','adverse_reactions.*','medical_supplies.*','atcs.descripcion as nombreinsumo','medical_diccionaries.descripcion as nombrereaccion')
                        ->groupBy('notifications.id')
                        ->get(); 
    }

}
