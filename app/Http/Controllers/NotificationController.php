<?php

namespace SINRAIM\Http\Controllers;

use Illuminate\Http\Request;
use SINRAIM\Http\Requests;
use SINRAIM\Http\Requests\NotificationCreateRequest;
use SINRAIM\Http\Controllers\Controller;
use SINRAIM\Patient;
use SINRAIM\User;
use SINRAIM\UnidadMedida;
use SINRAIM\ViaAdministracion;
use SINRAIM\Notification;
use SINRAIM\Atc;
use SINRAIM\Risk;
use SINRAIM\MedicalSupply;
use SINRAIM\AdverseReaction;
use SINRAIM\MedicaLassagna;
use Illuminate\Routing\Route;
use Illuminate\Database\Eloquent\Collection;
use Session;
use Input;
use Redirect;
use DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class NotificationController extends Controller
{
    /**
     * Busqueda desde modelo para Metodos de Actualizar, editar y eliminar.
     *
     * 
     */
    public function __construct(){
        $this->middleware('auth');
        $this->beforeFilter('@find',['only' => ['edit','update','destroy']]);
    }

    public function find(Route $route){
        $this->notification = Notification::find($route->getParameter('notificacion'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications      = Notification::vernotificacion();
        
       return view('Notification.index',compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         //Recibir del metodo ajax el contenido del arreglo y convertirlo a objeto JSON     
        $arraynotificacion = json_decode(($request->getContent()), true);
        $TableDataconfirmacion      = $arraynotificacion["TableDataconfirmacion"];
        $TableDatafactoriesgos      = $arraynotificacion["TableDatafactoriesgos"];
        $TableDatainsumo            = $arraynotificacion["TableDatainsumo"];
        $TableDatareaccion          = $arraynotificacion["TableDatareaccion"];
        $TableDatalassagnas         = $arraynotificacion["TableDatalassagnas"];

        //return response()->json($TableDataconfirmacion);
       

        foreach ($TableDataconfirmacion as $objnotificacion) 
        {

            $idnotificador          = $objnotificacion["idnotificador"];
            $idpaciente             = $objnotificacion["idpaciente"];
            $estadonotificacion     = $objnotificacion["estadonotificacion"];
            $ingresohospital        = $objnotificacion["ingresohospital"];
            $pesopaciente           = $objnotificacion["pesopaciente"];
            $idhospitalnoti         = $objnotificacion["idhospitalnoti"];

            //return response()->json($idhospitalnoti);

            $notificacion=Notification::create([
                'estado'            =>   $estadonotificacion, 
                'ingresohospital'   =>   $ingresohospital,
                'peso'              =>   $pesopaciente,
                'user_id'           =>   $idnotificador,
                'patient_id'        =>   $idpaciente,
                'hospital_id'       =>   $idhospitalnoti,
                ]);
                $notificacion->save();
        } 

        foreach ($TableDatafactoriesgos as $objfactoriesgos) 
        {
            $factoriesgos           = $objfactoriesgos["factoriesgos"];
            foreach ($factoriesgos as $objfactoriesgoind) 
            {
                $risk_id   = $objfactoriesgoind[0];
                $notificacion->risks()->attach($risk_id);
                //var_dump($risk_id);
            }
        }

        
        foreach ($TableDatainsumo as $objinsumomedico) 
        {
            
            $codmedicamento         = $objinsumomedico["codmedicamento"];
            $dosispaciente          = $objinsumomedico["dosispaciente"];
            $unidadmedida_id        = $objinsumomedico["unidadmedida_id"];
            $frecuenciapaciente     = $objinsumomedico["frecuenciapaciente"];
            $tipoinsumo             = $objinsumomedico["tipoinsumo"];
            $viadministracion_id    = $objinsumomedico["viadministracion_id"];
            $fechainiciomedicamento = $objinsumomedico["fechainiciomedicamento"];
            $fechafinmedicamento    = $objinsumomedico["fechafinmedicamento"];
            $fechavencimiento       = $objinsumomedico["fechavencimiento"];
            $fabricante             = $objinsumomedico["fabricante"];
            $numlote                = $objinsumomedico["numlote"];           
            $motivoprescripcion     = $objinsumomedico["motivoprescripcion"]; 
            $notificacion_id        = $notificacion->id;


            $medicalsupply = new MedicalSupply;
            $medicalsupply->atc_id               = $codmedicamento;
            $medicalsupply->dosis                = $dosispaciente;
            $medicalsupply->unidadmedida_id      = $unidadmedida_id;
            $medicalsupply->frecuencia           = $frecuenciapaciente;
            $medicalsupply->tipoinsumo           = $tipoinsumo;
            $medicalsupply->viadministracion_id  = $viadministracion_id;
            $medicalsupply->fechainicio          = $fechainiciomedicamento;
            $medicalsupply->fechafin             = $fechafinmedicamento;
            $medicalsupply->fechavencimiento     = $fechavencimiento;
            $medicalsupply->fabricante           = $fabricante;
            $medicalsupply->numlote              = $numlote;
            $medicalsupply->motivo               = $motivoprescripcion;
            $medicalsupply->notificacion_id      = $notificacion_id;
            
            $medicalsupply->save();               

        } 

         foreach ($TableDatareaccion as $objreaccionadversa) 
        {
                                   
            $fechainicioreaccion        = $objreaccionadversa["fechainicioreaccion"];
            $fechafinreaccion           = $objreaccionadversa["fechafinreaccion"];
            $desenlacereaccion          = $objreaccionadversa["desenlacereaccion"];
            $intensidadreaccion         = $objreaccionadversa["intensidadreaccion"];
            $codreaccionadversa         = $objreaccionadversa["codreaccionadversa"];
            $notificacion_id            = $notificacion->id;


            $adversereaction = new AdverseReaction;
            $adversereaction->fechainicio       = $fechainicioreaccion;
            $adversereaction->fechafin          = $fechafinreaccion;
            $adversereaction->desenlace         = $desenlacereaccion;
            $adversereaction->intensidad        = $intensidadreaccion;
            $adversereaction->descripcion_id    = $codreaccionadversa;
            $adversereaction->notificacion_id   = $notificacion_id;
            
            $adversereaction->save();                 

        }

        foreach ($TableDatalassagnas as $objlassagnas) 
        {
            
            $lassagna1              = $objlassagnas["lassagna1"];
            $lassagna2              = $objlassagnas["lassagna2"];
            $lassagna3              = $objlassagnas["lassagna3"];
            $lassagna4              = $objlassagnas["lassagna4"];
            $lassagna5              = $objlassagnas["lassagna5"];
            $lassagna6              = $objlassagnas["lassagna6"];
            $lassagna7              = $objlassagnas["lassagna7"];
            $lassagna8              = $objlassagnas["lassagna8"];
            $lassagna9              = $objlassagnas["lassagna9"];
            $lassagna10             = $objlassagnas["lassagna10"];
            $resultadolassagnaind   = $objlassagnas["resultadolassagnaind"]; 
            

            $medicalsupplie_id  = $medicalsupply->id;


            if ($resultadolassagnaind >= 9)
                 $estadolassagna = "PROBADA";
            else if ($resultadolassagnaind >= 5 && $resultadolassagnaind <= 8)
                 $estadolassagna = "PROBABLE";
            else if ($resultadolassagnaind >= 1 && $resultadolassagnaind <= 4)
                 $estadolassagna = "POSIBLE";
            else
                $estadolassagna = "DUDOSA";
            


            $medica_lassagnas = new MedicaLassagna;
            $medica_lassagnas->Pregunta1                = $lassagna1;
            $medica_lassagnas->Pregunta2                = $lassagna2;
            $medica_lassagnas->Pregunta3                = $lassagna3;
            $medica_lassagnas->Pregunta4                = $lassagna4;
            $medica_lassagnas->Pregunta5                = $lassagna5;
            $medica_lassagnas->Pregunta6                = $lassagna6;
            $medica_lassagnas->Pregunta7                = $lassagna7;
            $medica_lassagnas->Pregunta8                = $lassagna8;
            $medica_lassagnas->Pregunta9                = $lassagna9;
            $medica_lassagnas->Pregunta10               = $lassagna10;
            $medica_lassagnas->Resultado_valor          = $resultadolassagnaind;
            $medica_lassagnas->estadolassagna           = $estadolassagna;
            $medica_lassagnas->medicalsupplie_id        = $medicalsupplie_id;

            //return response()->json($medica_lassagnas);
            
            $medica_lassagnas->save();                  

        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notifications      = Notification::where('id','=',$id)->get(); 
        $insumomedicos      = MedicalSupply::where('notificacion_id', '=', $id)->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->get();

       //$estadolassagnas     = MedicalSupply::select('atc_id')->where('notificacion_id', '=', $id)->join('medica_lassagnas', 'medical_supplies.id', '=', 'medica_lassagnas.medicalsupplie_id')->get();
       //dd($estadolassagnas);

        $reaccionadversas   = AdverseReaction::where('notificacion_id', '=', $id)->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->get();

        $arraynoti          = Notification::find($id); 
        $paciente_id        =$arraynoti->patient_id; 
        $user_id            =$arraynoti->user_id;

        $pacientes          =Patient::where('id','=',$paciente_id)->get(); 
        $usuarios           =User::where('id','=',$user_id)->get(); 
        $factoresderiesgos  = Notification::select('risks.name as factoresderiesgos')->where('notificacion_id', '=', $id)->join('notification_risk', 'notification_risk.notificacion_id', '=', 'notifications.id')->join('risks', 'notification_risk.risk_id', '=', 'risks.id')->groupBy('risks.name')->get();
        //dd($factoresderiesgos);

        return view('Notification.notificacionind',compact('notifications','insumomedicos','reaccionadversas','pacientes','usuarios','factoresderiesgos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Autocompletado de los medicamentos 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function autocompletemedicamento(Request $request)
    {
        $data = Atc::select("descripcion as name","idatc as id")->where("descripcion","LIKE", "%{$request->input('query')}%")->take(10)->get();

        return response()->json($data);        
        
    }

    /**
     * Autocompletado de los factores de riesgos con select2 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function factoriesgos(Request $request)
    { 

        $term = $request->term ?: '';
        $factoriesgos = Risk::where('name', 'like', $term.'%')->lists('name', 'id');
        $valid_tags = [];
        foreach ($factoriesgos as $id => $factoriesgo) {
            $valid_tags[] = ['id' => $id, 'text' => $factoriesgo];
        }
        return \Response::json($valid_tags);    
        
    }


    public function crearnotificacion($id)
    {

        $unidadmedida_id = UnidadMedida::lists('name','id');
        $viadministracion_id = ViaAdministracion::lists('name','id');
        $idnotificador = Auth::user()->id; 
        $idhospitalnoti= Auth::user()->hospital_id; 
        $patients       = Patient::find($id);
        //Guardar los objetos ID y nombre en variables que seran utilizadas en la vista de forma independiente.
        $idpaciente     = $patients->id; 

        return view('Notification.create',compact('unidadmedida_id','viadministracion_id','idnotificador','idpaciente','idhospitalnoti'));
    }


    public function evaluarnotificacionind($id)
    {

        $notifications          = Notification::find($id);
        $idnotifications        = $notifications->id;
        $estadonotificacion     = $notifications->estado;
        $insumomedicos          = MedicalSupply::where('notificacion_id', '=', $id)->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->lists('descripcion', 'idatc');
        $reaccionadversas       = AdverseReaction::where('notificacion_id', '=', $id)->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->lists('descripcion', 'iddescripcion');
         $insumoarray           =  array();
         $reaccionarray         =  array();
         foreach ($insumomedicos as $id => $insumomedico) 
            {
                $insumoarray[]  =  [ $id => $insumomedico];
            }
         foreach ($reaccionadversas as $id => $reaccionadversa) 
            {
                $reaccionarray[]=  [ $id => $reaccionadversa]; 
            }

        
         
        $idinsumoarray      = Input::get('idinsumoarray');        
        $idreaccionarray    = Input::get('idreaccionarray');

          $nombreinsumoevals  = MedicalSupply::select('atcs.descripcion')->where('atc_id', '=', $idinsumoarray)->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->groupBy('atcs.descripcion')->get();
        $arraynombreinsumoeval    =  array();
        foreach ($nombreinsumoevals as $nombreinsumoeval) 
            {
                $arraynombreinsumoeval[]=  $nombreinsumoeval->descripcion; 
            }
         if(empty($arraynombreinsumoeval[0])){
                $nombreinsumoeval  = "No existen Registros en notificaciones sobre Insumo medico";
            }
            else{
                 $nombreinsumoeval    = $arraynombreinsumoeval[0]; 
            }         
        $nombrereaccionevals  = AdverseReaction::select('medical_diccionaries.descripcion')->where('descripcion_id', '=', $idreaccionarray)->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->groupBy('medical_diccionaries.descripcion')->get();
        $arraynombrereaccioneval    =  array();
        foreach ($nombrereaccionevals as $nombrereaccioneval) 
            {
                $arraynombrereaccioneval[]=  $nombrereaccioneval->descripcion; 
            }
           if(empty($arraynombrereaccioneval[0])){
                $nombrereaccioneval  = "No existen Registros en notificaciones sobre Reaccion Adversa";
            }
            else{
                 $nombrereaccioneval = $arraynombrereaccioneval[0];
            } 
        


        $notifications      = Notification::all()->count();
        $totalnotinsumo     = MedicalSupply::where('atc_id', '=', $idinsumoarray)->count();
        $totalnotireaccion  = AdverseReaction::where('descripcion_id', '=', $idreaccionarray)->count();
        $totalcoincidencia  = DB::table('notifications')->join('medical_supplies', 'medical_supplies.notificacion_id', '=', 'notifications.id')->join('adverse_reactions', 'adverse_reactions.notificacion_id', '=', 'notifications.id')->where('atc_id', '=', $idinsumoarray)->where('descripcion_id', '=', $idreaccionarray)->count();




        return view('Notification.evaluarnotificacionind',compact('insumoarray','reaccionarray','idnotifications','notifications','totalnotinsumo','totalnotireaccion','totalcoincidencia','estadonotificacion','nombreinsumoeval','nombrereaccioneval'));
    }


    public function guardarestadonotificacionind(Request $request, $id)
    {    
        $notifications          = Notification::find($id);
        $idnotifications        = $notifications->id;
        //$data = Input::all();
        $estadofinalnoti=$notifications->estado = Input::get('estadofinalnoti'); 
        //dd($estadofinalnoti);
        $motivocambioestado=$notifications->observaciones = Input::get('motivocambioestado');

        $fechamodificacion= Carbon::now();
        $fechactualizacionoti=$fechamodificacion->toDateTimeString();

        DB::update('UPDATE notifications set estado=? where id=?',array($estadofinalnoti[0],$idnotifications));
        DB::update('UPDATE notifications set observaciones=? where id=?',array($motivocambioestado[0],$idnotifications));
        DB::update('UPDATE notifications set updated_at=? where id=?',array($fechactualizacionoti,$idnotifications));

       $notifications      = Notification::vernotificacion();        
       return view('Notification.index',compact('notifications'));
    }


    public function evaluaralgoritmoprr()
    {     
        
        $idinsumoarray      = Input::get('codmedicamento');        
        $idreaccionarray    = Input::get('codreaccionadversa');

        $nombreinsumoevals  = MedicalSupply::select('atcs.descripcion')->where('atc_id', '=', $idinsumoarray)->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->groupBy('atcs.descripcion')->get();
        $arraynombreinsumoeval    =  array();
        foreach ($nombreinsumoevals as $nombreinsumoeval) 
            {
                $arraynombreinsumoeval[]=  $nombreinsumoeval->descripcion; 
            }
         if(empty($arraynombreinsumoeval[0])){
                $nombreinsumoeval  = "No existen Registros en notificaciones sobre Insumo medico";
            }
            else{
                 $nombreinsumoeval    = $arraynombreinsumoeval[0]; 
            }         
        $nombrereaccionevals  = AdverseReaction::select('medical_diccionaries.descripcion')->where('descripcion_id', '=', $idreaccionarray)->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->groupBy('medical_diccionaries.descripcion')->get();
        $arraynombrereaccioneval    =  array();
        foreach ($nombrereaccionevals as $nombrereaccioneval) 
            {
                $arraynombrereaccioneval[]=  $nombrereaccioneval->descripcion; 
            }
           if(empty($arraynombrereaccioneval[0])){
                $nombrereaccioneval  = "No existen Registros en notificaciones sobre Reaccion Adversa";
            }
            else{
                 $nombrereaccioneval = $arraynombrereaccioneval[0];
            } 
        


        $notifications        = Notification::all()->count();
        $totalnotinsumo       = MedicalSupply::where('atc_id', '=', $idinsumoarray)->count();
        $totalnotireaccion    = AdverseReaction::where('descripcion_id', '=', $idreaccionarray)->count();
        $totalcoincidencia    = DB::table('notifications')->join('medical_supplies', 'medical_supplies.notificacion_id', '=', 'notifications.id')->join('adverse_reactions', 'adverse_reactions.notificacion_id', '=', 'notifications.id')->where('atc_id', '=', $idinsumoarray)->where('descripcion_id', '=', $idreaccionarray)->count();

        /*$totalnotcoincidencia = DB::table('notifications')
            ->whereNotExists(function ($query) {
                $idinsumoarray      = Input::get('codmedicamento');        
                $idreaccionarray    = Input::get('codreaccionadversa');
                 $totalcoincidencia    = DB::table('notifications')->join('medical_supplies', 'medical_supplies.notificacion_id', '=', 'notifications.id')->join('adverse_reactions', 'adverse_reactions.notificacion_id', '=', 'notifications.id')->where('atc_id', '=', $idinsumoarray)->where('descripcion_id', '=', $idreaccionarray)->count();
                $query=$totalcoincidencia;
            })
            ->count();*/

    $notificacioninsumos       = MedicalSupply::select('notificacion_id')->where('atc_id', '=', $idinsumoarray)->get();
    $notificacionreaccions     = AdverseReaction::select('notificacion_id')->where('descripcion_id', '=', $idreaccionarray)->get();
    $resultadonotificacions    = Notification::select('id')->get();

    $totalnotificacioninsumo    =  array();
    $totalnotificacionreaccion  =  array();
    $totalnotificacion          =  array();

        foreach ($notificacioninsumos as $notificacioninsumo) 
            { 
                $totalnotificacioninsumo[]       = $notificacioninsumo->notificacion_id; 
            }

         foreach ($notificacionreaccions as $notificacionreaccion) 
                {
                    $totalnotificacionreaccion[] = $notificacionreaccion->notificacion_id; 
                }

        foreach ($resultadonotificacions as $resultadonotificacion) 
                {
                    $totalnotificacion[] = $resultadonotificacion->id; 
                }

      //$resultadofinal = $totalnotificacioninsumo[0]

       $resultadonocoincidencias = array_diff($totalnotificacion,$totalnotificacioninsumo, $totalnotificacionreaccion);
       $conteoresultadonocoincidencias=count($resultadonocoincidencias);

       //dd($conteoresultadonocoincidencias);

       if(empty($idinsumoarray) || empty($idreaccionarray)){
                $algoritmoPRR  = "No hay datos para evaluar";
                $chicuadrado  = "No hay datos para evaluar";
            }
            else{
                 $coincidencia_insumo_AmasB      = $totalcoincidencia+$totalnotinsumo;
                 $nocoincidencia_reaccion_CmasD  = $totalnotireaccion+$conteoresultadonocoincidencias;
                 $coincidencia_reaccion_AmasC    = $totalcoincidencia+$totalnotireaccion;
                 $nocoincidencia_insumo_BmasD    = $totalnotinsumo+$conteoresultadonocoincidencias;

                  if($coincidencia_insumo_AmasB==0 || $nocoincidencia_reaccion_CmasD==0){
                        $algoritmoPRR  = "No se encuentran los datos en reaccion o insumo medico";
                        $chicuadrado  = "No se encuentran los datos en reaccion o insumo medico";
                    }
                    else{
                         $coincidenciainsumo   = $totalcoincidencia/$coincidencia_insumo_AmasB;
                         $coincidenciareaccion = $totalnotireaccion/$nocoincidencia_reaccion_CmasD;

                          if($coincidenciareaccion==0){
                                $algoritmoPRR  = "No se encuentran los datos en reaccion o insumo medico";
                                $chicuadrado  = "No se encuentran los datos en reaccion o insumo medico";
                            }
                            else{
                                 $algoritmoPRR=round(($coincidenciainsumo/$coincidenciareaccion),2);
                                 $frecuenciaesperada_A = round((($coincidencia_insumo_AmasB*$coincidencia_reaccion_AmasC)/$notifications),2);
                                 $frecuenciaesperada_B = round((($coincidencia_insumo_AmasB*$nocoincidencia_insumo_BmasD)/$notifications),2);
                                 $frecuenciaesperada_C = round((($nocoincidencia_reaccion_CmasD*$coincidencia_reaccion_AmasC)/$notifications),2);
                                 $frecuenciaesperada_D = round((($nocoincidencia_reaccion_CmasD*$nocoincidencia_insumo_BmasD)/$notifications),2);

                                 $chicuadrado = round(((pow(($totalcoincidencia-$frecuenciaesperada_A),2)/$frecuenciaesperada_A)+(pow(($totalnotinsumo-$frecuenciaesperada_B),2)/$frecuenciaesperada_B)+(pow(($totalnotireaccion-$frecuenciaesperada_C),2)/$frecuenciaesperada_C)+(pow(($conteoresultadonocoincidencias-$frecuenciaesperada_D),2)/$frecuenciaesperada_D)),2);
                            } 
                    } 


                
                 
            } 

       


       //dd($conteoresultadonocoincidencias);        


       return view('Notification.evaluaralgoritmoprr',compact('insumoarray','reaccionarray','notifications','totalnotinsumo','totalnotireaccion','totalcoincidencia','nombreinsumoeval','nombrereaccioneval','totalnotcoincidencia','conteoresultadonocoincidencias','algoritmoPRR','chicuadrado'));
    }



}
