<?php

namespace SINRAIM\Http\Controllers;

use Illuminate\Http\Request;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Redirect;
use DB;
use SINRAIM\Patient;
use SINRAIM\User;
use SINRAIM\Hospital;
use SINRAIM\Municipality;
use SINRAIM\Department;
use SINRAIM\Notification;
use SINRAIM\MedicalSupply;
use Carbon\Carbon;

class InformesController extends Controller
{
  


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('Chart.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


     public function informespacientes(Request $request)
    {

        $patients = Patient::select('genero')->selectRaw('count(*) as cantidad')->groupBy('genero')->get();
        return json_encode($patients);
    }


    public function informesdepartamentos(Request $request){
        $departamento= User::select('departments.name')->selectRaw('COUNT(*) AS nombre')->join('notifications','notifications.user_id','=','users.id')->join('hospitals','hospitals.id', '=', 'users.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->get();
        return json_encode($departamento);
    }


    public function informesanhos(Request $request){
        $chartDatas = Notification::select([
            DB::raw('MONTHNAME(created_at) AS mes'),
            DB::raw('COUNT(id) AS cuenta'),
         ])
         ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
         ->groupBy('mes')
         ->orderBy('mes', 'ASC')
         ->get()
         ->toArray();
         //dd($chartDatas);
        return json_encode($chartDatas);
    }

    public function informescuatro(Request $request){
       
        $insumomedicos = MedicalSupply::select('atcs.descripcion')->selectRaw('count(atc_id) as conteo')->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->groupBy('atcs.descripcion')->orderBy('conteo','desc')->take(5)->get()->toArray();
         //dd($insumomedicos);
        return json_encode($insumomedicos);
    }

    
}
