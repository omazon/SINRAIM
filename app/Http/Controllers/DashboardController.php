<?php

namespace SINRAIM\Http\Controllers;

use Illuminate\Http\Request;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Controllers\Controller;
use SINRAIM\Notification;
use SINRAIM\MedicalSupply;
use SINRAIM\AdverseReaction;
use SINRAIM\Atc;
use SINRAIM\User;
use SINRAIM\Hospital;
use SINRAIM\Municipality;
use SINRAIM\Department;
use DB;
use Input;

class DashboardController extends Controller
{

    public function dashboard()
    {  
        
        $nombredepartamento       = Input::get('departamentos');
        $departamentos            = Notification::where('departments.name', '=', $nombredepartamento)->selectRaw('COUNT(*) AS cantidadnoti')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->get();


        $notideparprobadas        = Notification::where('estado', '=', 'PROBADA')->selectRaw('COUNT(*) AS conteoprobadas')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        $notideparprobables       = Notification::where('estado', '=', 'PROBABLE')->selectRaw('COUNT(*) AS conteoprobables')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        $notideparposibles       = Notification::where('estado', '=', 'POSIBLE')->selectRaw('COUNT(*) AS conteoposibles')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        $notidepardudosas       = Notification::where('estado', '=', 'DUDOSA')->selectRaw('COUNT(*) AS conteodudosa')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        //dd($notideparposibles);



        $insumomedicos             = MedicalSupply::select('atcs.descripcion')->selectRaw('count(atc_id) as conteo')->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->groupBy('atcs.descripcion')->orderBy('conteo','desc')->take(3)->get();
        $reaccionadversas           = AdverseReaction::select('medical_diccionaries.descripcion')->selectRaw('count(descripcion_id) as conteoreaccion')->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->groupBy('medical_diccionaries.descripcion')->orderBy('conteoreaccion','desc')->take(3)->get();
         $notificationsusers        = Notification::select('users.name','users.path')->selectRaw('count(notifications.id) as conteonotificacion')->join('users', 'notifications.user_id', '=', 'users.id')->groupBy('users.name')->orderBy('conteonotificacion','desc')->take(3)->get();


        $cantidadnotiarray    =  array();
          foreach ($departamentos as $departamento) 
            {
                $cantidadnotiarray[]  =  $departamento->cantidadnoti;
            }
        $cantidadnoti    = $cantidadnotiarray[0];


        $cantidadnotiprobadaarray    =  array();
          foreach ($notideparprobadas as $notideparprobada) 
            {
                $cantidadnotiprobadaarray[]  =  $notideparprobada->conteoprobadas;
            }
        $cantidadnotiprobadas    = $cantidadnotiprobadaarray[0];


        $cantidadnotiprobablearray    =  array();
          foreach ($notideparprobables as $notideparprobable) 
            {
                $cantidadnotiprobablearray[]  =  $notideparprobable->conteoprobables;
            }
        $cantidadnotiprobables    = $cantidadnotiprobablearray[0];


        $cantidadnotiposiblesarray    =  array();
          foreach ($notideparposibles as $notideparposible) 
            {
                $cantidadnotiposiblesarray[]  =  $notideparposible->conteoposibles;
            }
        $cantidadnotiposibles    = $cantidadnotiposiblesarray[0];


         $cantidadnotidudosasarray    =  array();
          foreach ($notidepardudosas as $notidepardudosa) 
            {
                $cantidadnotidudosasarray[]  =  $notidepardudosa->conteodudosa;
            }
        $cantidadnotidudosas   = $cantidadnotidudosasarray[0];


        if($cantidadnoti==0){
            $porcentajeprobadas     = "0";
            $porcentajeprobables    = "0";
            $porcentajeposibles     = "0";
            $porcentajedudosas      = "0";
        }
        else{
            $porcentajeprobadas     = round((($cantidadnotiprobadas*100)/$cantidadnoti),2);
            $porcentajeprobables    = round((($cantidadnotiprobables*100)/$cantidadnoti),2);
            $porcentajeposibles     = round((($cantidadnotiposibles*100)/$cantidadnoti),2);
            $porcentajedudosas      = round((($cantidadnotidudosas*100)/$cantidadnoti),2);
        }





             $topinsumoconteos       =  array();
             $topinsumonombres       =  array();
             foreach ($insumomedicos as $insumomedico) 
                {
                    $topinsumonombres[]  =  $insumomedico->descripcion;
                    $topinsumoconteos[]  =  $insumomedico->conteo;
                }



            
              
                       
             
            if(empty($topinsumonombres[0])){
                $topunoinsumonombres  = "No hay Notificaciones para el Top";
            }
            else{
                 $topunoinsumonombres   = $topinsumonombres[0];    
            } 
            if(empty($topinsumonombres[1])){
                $topdosinsumonombres  = "No hay Notificaciones para el Top";
            }
            else{
                $topdosinsumonombres    = $topinsumonombres[1];      
            } 
             if(empty($topinsumonombres[2])){
                $toptresinsumonombres  = "No hay Notificaciones para el Top";
            }
            else{
                $toptresinsumonombres   = $topinsumonombres[2];     
            } 


             
            if(empty($topinsumoconteos[0])){
                $topunoinsumoconteos  = "0";
            }
            else{
                $topunoinsumoconteos  = $topinsumoconteos[0];      
            } 
            if(empty($topinsumoconteos[1])){
                $topdosinsumoconteos  = "0";
            }
            else{
                $topdosinsumoconteos    = $topinsumoconteos[1];      
            } 
             if(empty($topinsumoconteos[2])){
                $toptresinsumoconteos  = "0";
            }
            else{
                $toptresinsumoconteos   = $topinsumoconteos[2];      
            } 



             $topreaccionconteos    =  array();
             $topreaccionnombres    =  array();
             foreach ($reaccionadversas as $reaccionadversa) 
                {
                    $topreaccionnombres[]  =  $reaccionadversa->descripcion;
                    $topreaccionconteos[]  =  $reaccionadversa->conteoreaccion;
                }
             

            if(empty($topreaccionnombres[0])){
                $topunoreaccionnombres  = "No hay Notificaciones para el Top";
            }
            else{
                 $topunoreaccionnombres   = $topreaccionnombres[0];  
            } 
            if(empty($topreaccionnombres[1])){
                $topdosreaccionnombres  = "No hay Notificaciones para el Top";
            }
            else{
                $topdosreaccionnombres   = $topreaccionnombres[1];       
            } 
             if(empty($topreaccionnombres[2])){
                $toptresreaccionnombres  = "No hay Notificaciones para el Top";
            }
            else{
                $toptresreaccionnombres  = $topreaccionnombres[2];     
            } 


             

            if(empty($topreaccionconteos[0])){
                $topunoreaccionconteos  = "0";
            }
            else{
                $topunoreaccionconteos   = $topreaccionconteos[0];     
            } 
            if(empty($topreaccionconteos[1])){
                $topdosreaccionconteos  = "0";
            }
            else{
                $topdosreaccionconteos   = $topreaccionconteos[1];     
            } 
             if(empty($topreaccionconteos[2])){
                $toptresreaccionconteos  = "0";
            }
            else{
                $toptresreaccionconteos  = $topreaccionconteos[2];     
            } 





             $topuserconteos    =  array();
             $topusernombres    =  array();
             $topuserpath       =  array();
             foreach ($notificationsusers as $notificationsuser) 
                {
                    $topusernombres[]  =  $notificationsuser->name;
                    $topuserconteos[]  =  $notificationsuser->conteonotificacion;
                    $topuserpath   []  =  $notificationsuser->path;
                }

             
              

            if(empty($topusernombres[0])){
                $topunousernombres  = "No hay Notificaciones para el Top";
            }
            else{
                $topunousernombres   = $topusernombres[0];  
            } 
            if(empty($topusernombres[1])){
                $topdosusernombres  = "No hay Notificaciones para el Top";
            }
            else{
                $topdosusernombres   = $topusernombres[1];       
            } 
            if(empty($topusernombres[2])){
                $toptresusernombres  = "No hay Notificaciones para el Top";
            }
            else{
                $toptresusernombres   = $topusernombres[2];  
            } 
            

             
              

            if(empty($topuserconteos[0])){
                $topunouserconteos  = "0";
            }
            else{
                $topunouserconteos   = $topuserconteos[0];    
            } 
            if(empty($topuserconteos[1])){
                $topdosuserconteos  = "0";
            }
            else{
                $topdosuserconteos   = $topuserconteos[1];     
            } 
            if(empty($topuserconteos[2])){
                $toptresuserconteos  = "0";
            }
            else{
                $toptresuserconteos   = $topuserconteos[2];    
            } 



             
              

            if(empty($topuserpath[0])){
                $topunouserpath  = "defaultuser.jpg";
            }
            else{
                $topunouserpath      = $topuserpath[0];  
            } 
            if(empty($topuserpath[1])){
                $topdosuserpath  = "defaultuser.jpg";
            }
            else{
                $topdosuserpath      = $topuserpath[1];     
            } 
            if(empty($topuserpath[2])){
                $toptresuserpath  = "defaultuser.jpg";
            }
            else{
                $toptresuserpath      = $topuserpath[2];  
            } 
             



        //dd($cantidadnoti);


       return view('Dashboard.dashboard', compact('notifications','topunoinsumonombres','topunoinsumoconteos','topdosinsumonombres','topdosinsumoconteos','toptresinsumonombres','toptresinsumoconteos','topunoreaccionnombres','topdosreaccionnombres','toptresreaccionnombres','topunoreaccionconteos','topdosreaccionconteos','toptresreaccionconteos','topunousernombres','topdosusernombres','toptresusernombres','topunouserconteos','topdosuserconteos','toptresuserconteos','topunouserpath','topdosuserpath','toptresuserpath' ,'cantidadnoti','nombredepartamento','cantidadnotiprobadas','cantidadnotiprobables','cantidadnotiposibles','cantidadnotidudosas','porcentajeprobadas','porcentajeprobables','porcentajeposibles','porcentajedudosas'));
    }
    public function dashboard2()
    {

        $nombredepartamento       = Input::get('departamentos');
        $departamentos            = Notification::where('departments.name', '=', $nombredepartamento)->selectRaw('COUNT(*) AS cantidadnoti')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->get();


        $notideparprobadas        = Notification::where('estado', '=', 'PROBADA')->selectRaw('COUNT(*) AS conteoprobadas')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        $notideparprobables       = Notification::where('estado', '=', 'PROBABLE')->selectRaw('COUNT(*) AS conteoprobables')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        $notideparposibles       = Notification::where('estado', '=', 'POSIBLE')->selectRaw('COUNT(*) AS conteoposibles')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        $notidepardudosas       = Notification::where('estado', '=', 'DUDOSA')->selectRaw('COUNT(*) AS conteodudosa')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('departments.name', '=', $nombredepartamento)->get();
        //dd($notideparposibles);



        $insumomedicos             = MedicalSupply::select('atcs.descripcion')->selectRaw('count(atc_id) as conteo')->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->groupBy('atcs.descripcion')->orderBy('conteo','desc')->take(3)->get();
        $reaccionadversas           = AdverseReaction::select('medical_diccionaries.descripcion')->selectRaw('count(descripcion_id) as conteoreaccion')->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->groupBy('medical_diccionaries.descripcion')->orderBy('conteoreaccion','desc')->take(3)->get();
        $notificationsusers        = Notification::select('users.name','users.path')->selectRaw('count(notifications.id) as conteonotificacion')->join('users', 'notifications.user_id', '=', 'users.id')->groupBy('users.name')->orderBy('conteonotificacion','desc')->take(3)->get();


        $cantidadnotiarray    =  array();
        foreach ($departamentos as $departamento)
        {
            $cantidadnotiarray[]  =  $departamento->cantidadnoti;
        }
        $cantidadnoti    = $cantidadnotiarray[0];


        $cantidadnotiprobadaarray    =  array();
        foreach ($notideparprobadas as $notideparprobada)
        {
            $cantidadnotiprobadaarray[]  =  $notideparprobada->conteoprobadas;
        }
        $cantidadnotiprobadas    = $cantidadnotiprobadaarray[0];


        $cantidadnotiprobablearray    =  array();
        foreach ($notideparprobables as $notideparprobable)
        {
            $cantidadnotiprobablearray[]  =  $notideparprobable->conteoprobables;
        }
        $cantidadnotiprobables    = $cantidadnotiprobablearray[0];


        $cantidadnotiposiblesarray    =  array();
        foreach ($notideparposibles as $notideparposible)
        {
            $cantidadnotiposiblesarray[]  =  $notideparposible->conteoposibles;
        }
        $cantidadnotiposibles    = $cantidadnotiposiblesarray[0];


        $cantidadnotidudosasarray    =  array();
        foreach ($notidepardudosas as $notidepardudosa)
        {
            $cantidadnotidudosasarray[]  =  $notidepardudosa->conteodudosa;
        }
        $cantidadnotidudosas   = $cantidadnotidudosasarray[0];


        if($cantidadnoti==0){
            $porcentajeprobadas     = "0";
            $porcentajeprobables    = "0";
            $porcentajeposibles     = "0";
            $porcentajedudosas      = "0";
        }
        else{
            $porcentajeprobadas     = round((($cantidadnotiprobadas*100)/$cantidadnoti),2);
            $porcentajeprobables    = round((($cantidadnotiprobables*100)/$cantidadnoti),2);
            $porcentajeposibles     = round((($cantidadnotiposibles*100)/$cantidadnoti),2);
            $porcentajedudosas      = round((($cantidadnotidudosas*100)/$cantidadnoti),2);
        }





        $topinsumoconteos       =  array();
        $topinsumonombres       =  array();
        foreach ($insumomedicos as $insumomedico)
        {
            $topinsumonombres[]  =  $insumomedico->descripcion;
            $topinsumoconteos[]  =  $insumomedico->conteo;
        }







        if(empty($topinsumonombres[0])){
            $topunoinsumonombres  = "No hay Notificaciones para el Top";
        }
        else{
            $topunoinsumonombres   = $topinsumonombres[0];
        }
        if(empty($topinsumonombres[1])){
            $topdosinsumonombres  = "No hay Notificaciones para el Top";
        }
        else{
            $topdosinsumonombres    = $topinsumonombres[1];
        }
        if(empty($topinsumonombres[2])){
            $toptresinsumonombres  = "No hay Notificaciones para el Top";
        }
        else{
            $toptresinsumonombres   = $topinsumonombres[2];
        }



        if(empty($topinsumoconteos[0])){
            $topunoinsumoconteos  = "0";
        }
        else{
            $topunoinsumoconteos  = $topinsumoconteos[0];
        }
        if(empty($topinsumoconteos[1])){
            $topdosinsumoconteos  = "0";
        }
        else{
            $topdosinsumoconteos    = $topinsumoconteos[1];
        }
        if(empty($topinsumoconteos[2])){
            $toptresinsumoconteos  = "0";
        }
        else{
            $toptresinsumoconteos   = $topinsumoconteos[2];
        }



        $topreaccionconteos    =  array();
        $topreaccionnombres    =  array();
        foreach ($reaccionadversas as $reaccionadversa)
        {
            $topreaccionnombres[]  =  $reaccionadversa->descripcion;
            $topreaccionconteos[]  =  $reaccionadversa->conteoreaccion;
        }


        if(empty($topreaccionnombres[0])){
            $topunoreaccionnombres  = "No hay Notificaciones para el Top";
        }
        else{
            $topunoreaccionnombres   = $topreaccionnombres[0];
        }
        if(empty($topreaccionnombres[1])){
            $topdosreaccionnombres  = "No hay Notificaciones para el Top";
        }
        else{
            $topdosreaccionnombres   = $topreaccionnombres[1];
        }
        if(empty($topreaccionnombres[2])){
            $toptresreaccionnombres  = "No hay Notificaciones para el Top";
        }
        else{
            $toptresreaccionnombres  = $topreaccionnombres[2];
        }




        if(empty($topreaccionconteos[0])){
            $topunoreaccionconteos  = "0";
        }
        else{
            $topunoreaccionconteos   = $topreaccionconteos[0];
        }
        if(empty($topreaccionconteos[1])){
            $topdosreaccionconteos  = "0";
        }
        else{
            $topdosreaccionconteos   = $topreaccionconteos[1];
        }
        if(empty($topreaccionconteos[2])){
            $toptresreaccionconteos  = "0";
        }
        else{
            $toptresreaccionconteos  = $topreaccionconteos[2];
        }





        $topuserconteos    =  array();
        $topusernombres    =  array();
        $topuserpath       =  array();
        foreach ($notificationsusers as $notificationsuser)
        {
            $topusernombres[]  =  $notificationsuser->name;
            $topuserconteos[]  =  $notificationsuser->conteonotificacion;
            $topuserpath   []  =  $notificationsuser->path;
        }




        if(empty($topusernombres[0])){
            $topunousernombres  = "No hay Notificaciones para el Top";
        }
        else{
            $topunousernombres   = $topusernombres[0];
        }
        if(empty($topusernombres[1])){
            $topdosusernombres  = "No hay Notificaciones para el Top";
        }
        else{
            $topdosusernombres   = $topusernombres[1];
        }
        if(empty($topusernombres[2])){
            $toptresusernombres  = "No hay Notificaciones para el Top";
        }
        else{
            $toptresusernombres   = $topusernombres[2];
        }





        if(empty($topuserconteos[0])){
            $topunouserconteos  = "0";
        }
        else{
            $topunouserconteos   = $topuserconteos[0];
        }
        if(empty($topuserconteos[1])){
            $topdosuserconteos  = "0";
        }
        else{
            $topdosuserconteos   = $topuserconteos[1];
        }
        if(empty($topuserconteos[2])){
            $toptresuserconteos  = "0";
        }
        else{
            $toptresuserconteos   = $topuserconteos[2];
        }






        if(empty($topuserpath[0])){
            $topunouserpath  = "defaultuser.jpg";
        }
        else{
            $topunouserpath      = $topuserpath[0];
        }
        if(empty($topuserpath[1])){
            $topdosuserpath  = "defaultuser.jpg";
        }
        else{
            $topdosuserpath      = $topuserpath[1];
        }
        if(empty($topuserpath[2])){
            $toptresuserpath  = "defaultuser.jpg";
        }
        else{
            $toptresuserpath      = $topuserpath[2];
        }




        //dd($cantidadnoti);


        return view('Dashboard.dashboard2', compact('notifications','topunoinsumonombres','topunoinsumoconteos','topdosinsumonombres','topdosinsumoconteos','toptresinsumonombres','toptresinsumoconteos','topunoreaccionnombres','topdosreaccionnombres','toptresreaccionnombres','topunoreaccionconteos','topdosreaccionconteos','toptresreaccionconteos','topunousernombres','topdosusernombres','toptresusernombres','topunouserconteos','topdosuserconteos','toptresuserconteos','topunouserpath','topdosuserpath','toptresuserpath' ,'cantidadnoti','nombredepartamento','cantidadnotiprobadas','cantidadnotiprobables','cantidadnotiposibles','cantidadnotidudosas','porcentajeprobadas','porcentajeprobables','porcentajeposibles','porcentajedudosas'));
    }
 
    
}
