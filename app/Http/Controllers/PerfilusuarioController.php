<?php

namespace SINRAIM\Http\Controllers;

use Illuminate\Http\Request;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Controllers\Controller;
use SINRAIM\Hospital;
use SINRAIM\User;
use SINRAIM\Specialty;
use SINRAIM\Profession;
use Input;
use Auth;
use DB;

class PerfilusuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idnotificador   = Auth::user()->id;
        $idhospital      = Auth::user()->hospital_id; 

        $nombrehospitals = User::select('hospitals.name as nombrehospital')->where('hospital_id', '=', $idhospital)->join('hospitals', 'users.hospital_id', '=', 'hospitals.id')->groupBy('hospitals.name')->get();

        $nombrehospitalarray    =  array();
          foreach ($nombrehospitals as $nombrehospital) 
            {
                $nombrehospitalarray[]  =  $nombrehospital->nombrehospital;
            }
        $nombrehospital    = $nombrehospitalarray[0];


        $nombreprofesionperfils  = User::select('professions.name as nombreprofesion')->where('user_id', '=', $idnotificador)->join('user_profession', 'user_profession.user_id', '=', 'users.id')->join('professions', 'user_profession.profession_id', '=', 'professions.id')->groupBy('professions.name')->get();
        $nombrespecialidadperfils  = User::select('specialties.name as nombrespecialidad')->where('user_id', '=', $idnotificador)->join('user_specialty', 'user_specialty.user_id', '=', 'users.id')->join('specialties', 'user_specialty.specialty_id', '=', 'specialties.id')->groupBy('specialties.name')->get();

        $hospital_id = Hospital::lists('name','id');
        return view('PerfilUsuario.perfilusuario',compact('hospital_id','nombreprofesionperfils','nombrespecialidadperfils','nombrehospital'));
    }

    public function cambiarpassword()
    {            

        $password      = Input::get('password');
        $password      = bcrypt($password );
        $idnotificador = Auth::user()->id;
        DB::update('UPDATE users set password=? where id=?',array($password,$idnotificador));

        return back();        

    }

    public function cambiarcontactos()
    {     
        $idnotificador   = Auth::user()->id;  
        $telefono        = Input::get('telefono'); 

        DB::update('UPDATE users set telefono=? where id=?',array($telefono,$idnotificador));
        return back();

    }


    public function cambiarhospital()
    {     
        $idnotificador   = Auth::user()->id;  
        $idhospitalperfil= Input::get('idhospitalperfil');  

        DB::update('UPDATE users set hospital_id=? where id=?',array($idhospitalperfil,$idnotificador));
        return back();

    }

    public function subirarchivo(Request $request)
    {  

      
       $path = Input::file('path');
       $aleatorio = str_random(7);
       $nombre = $aleatorio.'-'.$path->getClientOriginalName();
       $idnotificador   = Auth::user()->id;        

       $path->move('archivos', $nombre);
       $idnotificador   = Auth::user()->id;
       DB::update('UPDATE users set path=? where id=?',array($nombre,$idnotificador));
       
       return back();
    
    }



    public function portada(Request $request)
    {      

        $portadas = Input::get('portadas');
        $idnotificador   = Auth::user()->id;     

        DB::update('UPDATE users set portada=? where id=?',array($portadas,$idnotificador));

         return back();

    }


    public function especialidades(Request $request)
    {  

        $term = $request->term ?: '';
        $especialidades = Specialty::where('name', 'like', $term.'%')->lists('name', 'id');
        $valid_tags = [];
        foreach ($especialidades as $id => $especialidade) {
            $valid_tags[] = ['id' => $id, 'text' => $especialidade];
        }
        return \Response::json($valid_tags);        

    }

    public function profesiones(Request $request)
    {  

        $term = $request->term ?: '';
        $profesiones = Profession::where('name', 'like', $term.'%')->lists('name', 'id');
        $valid_tags = [];
        foreach ($profesiones as $id => $profesione) {
            $valid_tags[] = ['id' => $id, 'text' => $profesione];
        }
        return \Response::json($valid_tags);        

    }


    public function datosprofesionales(Request $request)
    {  

        $usuarioauth = Auth::user(); 
        $idnotificador   = Auth::user()->id; 

        $arrayprofesion = json_decode(($request->getContent()), true);
        $TableDataprofesiones      = $arrayprofesion["TableDataprofesiones"];
         foreach ($TableDataprofesiones as $objprofesiones) 
                {
                    $profesiones           = $objprofesiones["profesiones"];
                    foreach ($profesiones as $objprofesionesind) 
                    {
                        $profession_id   = $objprofesionesind[0];
                        $usuarioauth->professions()->attach($profession_id);
                    }
                }
        return back();
    
    }


    public function datosespecialidades(Request $request)
    {  

        $usuarioauth = Auth::user(); 
        $idnotificador   = Auth::user()->id; 
        $arrayespecialidades = json_decode(($request->getContent()), true);

         $TableDataespecialidades      = $arrayespecialidades["TableDataespecialidades"];
         foreach ($TableDataespecialidades as $objespecialidades) 
                {
                    $especialidades           = $objespecialidades["especialidades"];
                    foreach ($especialidades as $objespecialidadesind) 
                    {
                        $especialty_id   = $objespecialidadesind[0];
                        $usuarioauth->specialties()->attach($especialty_id);
                    }
                }
        return back();
    
    }

   
}
