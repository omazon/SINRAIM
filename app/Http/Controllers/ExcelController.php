<?php

namespace SINRAIM\Http\Controllers;

use Illuminate\Http\Request;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Controllers\Controller;
use SINRAIM\Patient;
use SINRAIM\User;
use SINRAIM\UnidadMedida;
use SINRAIM\ViaAdministracion;
use SINRAIM\Notification;
use SINRAIM\Atc;
use SINRAIM\Risk;
use SINRAIM\MedicalSupply;
use SINRAIM\AdverseReaction;
use SINRAIM\MedicaLassagna;
use Input;
use DB;
use Maatwebsite\Excel\Facades\Excel;


class ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importarexcelusuario()
    {

        if(Input::hasFile('importarusuario')){
            $path = Input::file('importarusuario')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    $insert[] = [
                        'name'          =>  $value->name,
                        'last_name'     =>  $value->last_name,
                        'username'      =>  $value->username,
                        'password'      =>  bcrypt($value->password),                
                        'genero'        =>  $value->genero,
                        'path'          =>  $value->path,
                        'portada'       =>  $value->portada,
                        'telefono'      =>  $value->telefono,
                        'email'         =>  $value->email,
                        'hospital_id'   =>  $value->hospital_id,
                        'roles_id'      =>  $value->roles_id
                        ];
                }
                if(!empty($insert)){
                    DB::table('users')->insert($insert);
                    return back()->with('message','Felicidades!. Haz ingresado multiples usuarios de forma masiva con exito!!!.');
                }
               
            }
        }
        return back()->with('message-error','Error! los usuarios no se han guardado por que falta informacion, revisa y prueba nuevamente!!!.');
        
    }


    public function exportarexcelusuario($type)
    {

       $data = User::get()->toArray();
        return Excel::create('Usuarios_Excel', function($excel) use ($data) {
            $excel->sheet('Lista_de_Usuarios', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function exportarPDFusuario()
    {
       $data = User::get()->toArray();
       return Excel::create('Usuarios_PDF', function($excel) use ($data) {
        $excel->sheet('Lista_de_Usuarios', function($sheet) use ($data)
        {
            $sheet->fromArray($data);
            $sheet->setOrientation('landscape');
        });
       })->download("pdf");
    }



     public function exportarexcelnotificacion($id)
    {

        $datanotificacion   = Notification::select('notifications.id as Número','notifications.estado as Estado','users.name as Notificador','patients.name as Paciente','notifications.observaciones as Observaciones','notifications.ingresohospital as Requirio_Ingreso','notifications.peso as Peso','hospitals.name as Hospital','notifications.created_at as Fecha_Creacion','notifications.updated_at as Fecha_Actualizacion')->join('users', 'notifications.user_id', '=', 'users.id')->join('patients', 'notifications.patient_id', '=', 'patients.id')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('notifications.id', '=', $id)->get();

        $datainsumo   = MedicalSupply::select('medical_supplies.atc_id as Cod_ATC','atcs.descripcion as Insumo_Medico','medical_supplies.dosis as Dosis','unidad_medidas.name as Unidad_Medida','medical_supplies.frecuencia as Frecuencia','medical_supplies.tipoinsumo as Tipo_Insumo','via_administracions.name as Via_Administracion','medical_supplies.fechainicio as Inicio_Toma_Insumo','medical_supplies.fechafin as Fin_Toma_Insumo','medical_supplies.fechavencimiento as Fecha_Vencimiento','medical_supplies.fabricante as Fabricante','medical_supplies.numlote as Numero_Lote','medical_supplies.motivo as Motivo')->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->join('notifications', 'medical_supplies.notificacion_id', '=', 'notifications.id')->join('unidad_medidas', 'medical_supplies.unidadmedida_id', '=', 'unidad_medidas.id')->join('via_administracions', 'medical_supplies.viadministracion_id', '=', 'via_administracions.id')->where('notifications.id', '=', $id)->get();

        $datareaccion   = AdverseReaction::select('adverse_reactions.descripcion_id as Cod_Reaccion','medical_diccionaries.descripcion as Reaccion_Adversa','adverse_reactions.fechainicio as Inicio_Reaccion_Adversa','adverse_reactions.fechafin as Fin_Reaccion_Adversa','adverse_reactions.desenlace as Desenlace','adverse_reactions.intensidad as Intensidad')->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->join('notifications', 'adverse_reactions.notificacion_id', '=', 'notifications.id')->where('notifications.id', '=', $id)->get();

        //dd($datareaccion);

       
        return Excel::create("Notificacion_Numero: ".$id, function($excel) use ($datanotificacion,$datainsumo,$datareaccion) {
            $excel->sheet('Notificacion', function($sheet) use ($datanotificacion)
            {
                $sheet->fromArray($datanotificacion);
            });

            $excel->sheet('Insumo_Medico', function($sheet) use ($datainsumo)
            {
                $sheet->fromArray($datainsumo);
            });

            $excel->sheet('Reaccion_Adversa', function($sheet) use ($datareaccion)
            {
                $sheet->fromArray($datareaccion);
            });
        })->download("xlsx");
    }




     public function exportarpdfnotificacion($id)
    {

        $datanotificacion   = Notification::select('notifications.id as Número','notifications.estado as Estado','users.name as Notificador','patients.name as Paciente','notifications.observaciones as Observaciones','notifications.ingresohospital as Requirio_Ingreso','notifications.peso as Peso','hospitals.name as Hospital','notifications.created_at as Fecha_Creacion','notifications.updated_at as Fecha_Actualizacion')->join('users', 'notifications.user_id', '=', 'users.id')->join('patients', 'notifications.patient_id', '=', 'patients.id')->join('hospitals','hospitals.id', '=', 'notifications.hospital_id')->join('municipalities','municipalities.id', '=', 'hospitals.municipality_id')->join('departments','departments.id', '=', 'municipalities.department_id')->where('notifications.id', '=', $id)->get();

        $datainsumo   = MedicalSupply::select('medical_supplies.atc_id as Cod_ATC','atcs.descripcion as Insumo_Medico','medical_supplies.dosis as Dosis','unidad_medidas.name as Unidad_Medida','medical_supplies.frecuencia as Frecuencia','medical_supplies.tipoinsumo as Tipo_Insumo','via_administracions.name as Via_Administracion','medical_supplies.fechainicio as Inicio_Toma_Insumo','medical_supplies.fechafin as Fin_Toma_Insumo','medical_supplies.fechavencimiento as Fecha_Vencimiento','medical_supplies.fabricante as Fabricante','medical_supplies.numlote as Numero_Lote','medical_supplies.motivo as Motivo')->join('atcs', 'medical_supplies.atc_id', '=', 'atcs.idatc')->join('notifications', 'medical_supplies.notificacion_id', '=', 'notifications.id')->join('unidad_medidas', 'medical_supplies.unidadmedida_id', '=', 'unidad_medidas.id')->join('via_administracions', 'medical_supplies.viadministracion_id', '=', 'via_administracions.id')->where('notifications.id', '=', $id)->get();

        $datareaccion   = AdverseReaction::select('adverse_reactions.descripcion_id as Cod_Reaccion','medical_diccionaries.descripcion as Reaccion_Adversa','adverse_reactions.fechainicio as Inicio_Reaccion_Adversa','adverse_reactions.fechafin as Fin_Reaccion_Adversa','adverse_reactions.desenlace as Desenlace','adverse_reactions.intensidad as Intensidad')->join('medical_diccionaries', 'adverse_reactions.descripcion_id', '=', 'medical_diccionaries.iddescripcion')->join('notifications', 'adverse_reactions.notificacion_id', '=', 'notifications.id')->where('notifications.id', '=', $id)->get();

        //dd($datareaccion);

       
        return Excel::create("Notificacion_Numero: ".$id, function($excel) use ($datanotificacion,$datainsumo,$datareaccion) {
            $excel->sheet('Notificacion', function($sheet) use ($datanotificacion)
            {
                $sheet->fromArray($datanotificacion);
                $sheet->setOrientation('landscape');
            });

            $excel->sheet('Insumo_Medico', function($sheet) use ($datainsumo)
            {
                $sheet->fromArray($datainsumo);
                $sheet->setOrientation('landscape');
            });

            $excel->sheet('Reaccion_Adversa', function($sheet) use ($datareaccion)
            {
                $sheet->fromArray($datareaccion);
                $sheet->setOrientation('landscape');
            });
        })->download("pdf");
    }


     public function importarexcelATC()
    {

        if(Input::hasFile('importarATC')){
            $path = Input::file('importarATC')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    $insert[] = [
                        'codatc'            =>  $value->codatc,
                        'descripcion'       =>  $value->descripcion,
                        'viadministracion'  =>  $value->viadministracion,
                        'principioactivo'   =>  $value->principioactivo,                
                        'unidadmedida'      =>  $value->unidadmedida,
                        'unidadreferencia'  =>  $value->unidadreferencia,
                        'formafarmaceutica' =>  $value->formafarmaceutica
                        ];
                }
                if(!empty($insert)){
                    DB::table('atcs')->insert($insert);
                    return back()->with('message','Felicidades!. Haz ingresado multiples insumos medicos de forma masiva con exito!!!.');
                }
            }
        }
        return back()->with('message-error','Error! los insumos medicos no se han guardado por que falta informacion, revisa y prueba nuevamente!!!.');
    }
   
}
