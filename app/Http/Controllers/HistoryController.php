<?php

namespace SINRAIM\Http\Controllers;

use Illuminate\Http\Request;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Controllers\Controller;
use SINRAIM\History;
use SINRAIM\Patient;
use Illuminate\Routing\Route;
use Redirect;
use DB;

class HistoryController extends Controller
{

     /**
     * Busqueda desde modelo para Metodos de Actualizar, editar y eliminar.
     *
     * 
     */
     public function __construct(){
        $this->middleware('auth');
        $this->beforeFilter('@find',['only' => ['edit','update','destroy']]);
    }

    public function find(Route $route){
        $this->histories = History::find($route->getParameter('historia'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         //Recibir del metodo ajax el contenido del arreglo y convertirlo a objeto JSON     
        $antecedentes = json_decode($request->getContent());
        //return response()->json($antecedentes);

        //Recorrer el JSON guardando los objetos en variables para luego enviarlo
        //al modelo History y guardarlo en la BD
        foreach ($antecedentes as $obj) 
        {
            $familiar        = $obj->familiar;
            $edad            = $obj->edad;
            $vida            = $obj->vida;
            $patient_id      = $obj->patient_id;
            $descripcion_id  = $obj->descripcion_id;
            History::create([
                'familiar'          =>  $familiar, 
                'edad'              =>   $edad,
                'vida'              =>   $vida,
                'patient_id'        =>   $patient_id,
                'descripcion_id'    =>   $descripcion_id,

                ]);

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($this->histories);
        return response()->json($this->histories);
        //Recibe el ID del paciente a editar y luego envia el objeto al ajax.
        /*return response()->json([
            'familiar' => $this->histories->familiar,
            'edad' => $this->histories->edad,
            'vida' => $this->histories->vida,
            'id' => $this->histories->id
            ]);*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //Recibir el ID y la validacion para luego actualizar en la BD 
        $this->histories->fill($request->all());
        $this->histories->save();

        return response()->json([
                    "mensaje"=> "Listo"
                ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //recibir del modelo con el ID el antecedente que debe ser eliminado y borrarlo
        //si este proceso se ejecuta mediante ajax enviar el mensaje.
        $this->histories->delete();
        return response()->json([
                "mensaje"=> "Borrado"
            ]);
    }
}
