<?php namespace SINRAIM\Http\Controllers;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Requests\UserCreateRequest;
use SINRAIM\Http\Requests\UserUpdateRequest;
use SINRAIM\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SINRAIM\Hospital;
use SINRAIM\Role;
use SINRAIM\User;
use Illuminate\Routing\Route;
use Redirect;
use DB;


class UserController extends Controller {



	 public function __construct(){
	 	$this->middleware('auth');
        $this->beforeFilter('@find',['only' => ['edit','update','destroy']]);
    }

    public function find(Route $route){
        $this->users = User::find($route->getParameter('usuario'));
    }


	/**
	 * Display a index of the resource.
	 *
	 * @return Response
	 */	
	public function index(Request $request)
	{
		
		 if ($request->ajax()) {
            $users = DB::table('users')->get();
            return response()->json($users);
        }
		return view('User.create');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	//Metodo que permite llamar a la vista de creacion de nuevo usuario
	//Metodo que nos muestra la vista de los registros de los usuarios registrados
	public function create(Request $request)
	{

		$users = DB::table('users')->get();
		$hospital_id = Hospital::lists('name','id');		
		$roles_id = Role::lists('name','id');
		return view('User.create',compact('users','hospital_id','roles_id'));

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	//Medtodo que permite alamacenar las variables recibidas de la vista hacia la Base de datos
	public function store(Request $request)
	{

		 $v = \Validator::make($request->all(), [
            
            'name'              =>  'required',
            'name' 				=>  'required',
			'last_name' 		=>  'required',
			'username' 			=>  'required',
			'password' 			=>  'required',				
			'genero' 			=>  'required',
			'path' 				=>  'required',
			'portada'			=>  'required',
			'telefono' 			=>  'required',
			'email' 			=>  'required',
			'hospital_id' 		=>  'required',
			'roles_id' 			=>  'required'
        ]);
 
        if ($v->fails())
        {
            return redirect('/usuario/create')->with('message-error','Error! El Usuario no se ha guardado por que falta informacion, revisa el formulario de nuevo usuario!!!.')->withInput()->withErrors($v->errors());
           //return redirect()->back()->withInput()->withErrors($v->errors());
        }

		$users=User::create([
				'name' =>  $request['name'],
				'last_name' =>  $request['last_name'],
				'username' =>  $request['username'],
				'password' =>  $request['password'],				
				'genero' =>  $request['genero'],
				'path' =>  $request['path'],
				'portada' =>  $request['portada'],
				'telefono' =>  $request['telefono'],
				'email' =>  $request['email'],
				'hospital_id' =>  $request['hospital_id'],
				'roles_id' =>  $request['roles_id'],
			]);

		$users->save();
		return redirect('/usuario/create')->with('message','Felicidades!, Tu nuevo usuario ha sido guardado con exito!!!.');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$hospital_id = Hospital::lists('name','id');		
		$roles_id = Role::lists('name','id');
		return response()->json($this->users);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$hospital_id = Hospital::lists('name','id');		
		$roles_id = Role::lists('name','id');
		$this->users->fill($request->all());
		$this->users->save();

		return response()->json([
					"mensaje"=> "Listo"
				]);
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->users->delete();
		return response()->json([
				"mensaje"=> "Borrado"
			]);
			
	}

}
