<?php

namespace SINRAIM\Http\Controllers;

use Illuminate\Http\Request;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Controllers\Controller;
use SINRAIM\Atc;
use SINRAIM\MedicalDiccionary;
use DB;
use Input;

class AtcmeddraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('AtcMeddra.atcmeddra');
    }

    public function atc()
    {     
        $buscaratcs = DB::table('atcs')->take(5)->get();
        return view('AtcMeddra.atc',compact('buscaratcs'));
    }

    public function buscaratc()
    {     
        $codmedicamento     = Input::get('codmedicamento'); 
        $buscaratcs         = DB::table('atcs')->where('idatc', '=', $codmedicamento)->get();
        return view('AtcMeddra.atc',compact('buscaratcs'));
    }

    public function meddra()
    {     
        $buscarmeddras = DB::table('medical_diccionaries')->take(5)->get();
        return view('AtcMeddra.meddra',compact('buscarmeddras'));
    }

    public function buscarmeddra()
    {     
        $codreaccionadversa     = Input::get('codreaccionadversa'); 
        $buscarmeddras          = DB::table('medical_diccionaries')->where('iddescripcion', '=', $codreaccionadversa)->get();
        return view('AtcMeddra.meddra',compact('buscarmeddras'));
    }
}
