<?php namespace SINRAIM\Http\Controllers;

use SINRAIM\Http\Requests;
use SINRAIM\Http\Requests\PatientCreateRequest;
use SINRAIM\Http\Requests\PatientUpdateRequest;
use SINRAIM\Http\Controllers\Controller;
use Illuminate\Http\Request;
use SINRAIM\Patient;
use SINRAIM\User;
use SINRAIM\History;
use SINRAIM\MedicalDiccionary;
use Illuminate\Routing\Route;
use Illuminate\Database\Eloquent\Collection;
use Redirect;
use DB;
use Auth;
use Input;

class PatientController extends Controller
{
     
    /**
     * Busqueda desde modelo para Metodos de Actualizar, editar y eliminar.
     *
     * 
     */
    public function __construct(){
        $this->middleware('auth');
        $this->beforeFilter('@find',['only' => ['edit','update','destroy']]);
    }

    public function find(Route $route){
        $this->patients = Patient::find($route->getParameter('paciente'));
    }

    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //Recibir del modelo paciente a todos los que existen y retornarlo a la vista principal.
       
       $idnotificador = Auth::user()->id; 
       $patients = Patient::verpacientenoti($idnotificador);


        return view('Patient.index',compact('patients','idnotificador'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //retornar a la vista donde esta el formulario de crear, en este caso es un modal ubicado en el index.
        return view('Patient.index');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $v = \Validator::make($request->all(), [
            
            'name'              => 'required',
            'last_name'         => 'required',
            'fechanacimiento'   => 'required',
            'genero'            => 'required'
        ]);
 
        if ($v->fails())
        {
            return redirect('/paciente')->with('message-error','Error! El paciente no se ha guardado por que falta informacion, revisa el formulario de nuevo paciente!!!.')->withInput()->withErrors($v->errors());
           //return redirect()->back()->withInput()->withErrors($v->errors());
        }
        //Guardar los valores recibidos de la vista en los objetos que representan al modelo, despues de un proceso de validacion
        //mediante el Request        
         $patients=Patient::create([
                'name' =>  $request['name'],
                'last_name' =>  $request['last_name'],
                'fechanacimiento' =>  $request['fechanacimiento'],
                'edad' =>  $request['edad'],             
                'genero' =>  $request['genero'],
            ]);

         $idnotificador = Input::get('idnotificador');

         $patients->save();
         $patients->users()->attach($idnotificador);

         //retornar a la vista index con el mensaje.
        return redirect('/paciente')->with('message','Felicidades!, Tu nuevo paciente ha sido guardado con exito!!!.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Busqueda del paciente segun el ID
        $patients       = Patient::find($id);
        //Guardar los objetos ID y nombre en variables que seran utilizadas en la vista de forma independiente.
        $idpaciente     = $patients->id; 
        $nombrepaciente = $patients->name;
        //Traer del modelo la consulta que indica los antecedentes por usuario seleccionado
        $histories      = History::Antecedentes($id);
        
        //$descripcion_id = MedicalDiccionary::lists('descripcion','iddescripcion');
        //retornar a la vista el arreglo recibido mas las variables independientes.
       return view('Patient.show',compact('histories'))->with('nombrepaciente',$nombrepaciente)->with('idpaciente',$idpaciente);
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //Recibe el ID del paciente a editar y luego envia el objeto al ajax.
       return response()->json([
            'name' => $this->patients->name,
            'last_name' => $this->patients->last_name,
            'fechanacimiento' => $this->patients->fechanacimiento,
            'edad' => $this->patients->edad,
            'genero' => $this->patients->genero,
            'id' => $this->patients->id
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //Recibir el ID y la validacion para luego actualizar en la BD 
       $this->patients->fill($request->all());
        $this->patients->save();

        return response()->json([
                    "mensaje"=> "Listo"
                ]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //recibir del modelo con el ID del paciente que debe ser eliminado y borrarlo
        //si este proceso se ejecuta mediante ajax enviar el mensaje.
        $this->patients->delete();

       /* $patients = Patient::find($id);
        $patients->users()->detach($patient_id);
        $patients->histories()->detach($patient_id);
        $this->patients->delete();*/

        //$patients->users()->delete();

        return response()->json([
                "mensaje"=> "Borrado"
            ]);
      
    }

    public function autocomplete(Request $request)
    {
        $data = MedicalDiccionary::select("descripcion as name","iddescripcion as id")->where("descripcion","LIKE", "%{$request->input('query')}%")->take(10)->get();

        return response()->json($data);
        
        
    }
}
