<?php

namespace SINRAIM\Http\Requests;

use SINRAIM\Http\Requests\Request;

class UserUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'last_name'=> 'required',
            'username'=> 'required|unique:users',
            'email'=> 'required|unique:users',
        ];
    }
}
