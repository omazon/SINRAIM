<?php

namespace SINRAIM\Http\Requests;

use SINRAIM\Http\Requests\Request;

class UserCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'last_name'=> 'required',
            'username'=> 'required|unique:users',
            'password'=> 'required|min:8',
            'email'=> 'required|unique:users',
        ];
    }
}
