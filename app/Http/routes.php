<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);



Route::get('/', 'SinraimController@getenviarnotificacion');
Route::get('vernotificacion', 'SinraimController@getvernotificacion');
Route::get('evaluarnotificacion', 'SinraimController@getevaluarnotificacion');
Route::get('perfilusuario', 'SinraimController@getperfilusuario');
Route::get('login', 'SinraimController@getlogin');
Route::get('/', 'SinraimController@getenviarnotificacion');
Route::filter('is_admin', function()
{
    if(Auth::user()->roles_id != '1' ) return Redirect::to('/');
});

*/

//Route::get('notificacion', 'SinraimController@getenviarnotificacion');



//RUTA PARA ADMINISTRAR USUARIOS
Route::resource('usuario', 'UserController');
//RUTA PARA ADMINISTRAR LOGIN
Route::resource('/', 'LogController');
Route::get('logout','LogController@logout');
Route::any('resetpassword', ['as' => 'resetpassword', 'uses' => 'LogController@resetpassword']);

//RUTA PARA ACCEDER AL DASHBOARD
Route::any('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
Route::any('dashboard2', ['as' => 'dashboard2', 'uses' => 'DashboardController@dashboard2']);

//RUTA PARA ADMINISTRAR DATOS DEL PACIENTE
Route::resource('paciente', 'PatientController');
//RUTA PARA ADMINISTRAR ANTECEDENTES FAMILIARES
Route::resource('historia', 'HistoryController');


//RUTA PARA ENVIAR NOTIFICACION DE REACCIONES ADVERSAS A INSUMOS MEDICOS
Route::resource('notificacion', 'NotificationController');
//RUTA PARA AUTOCOMPLETADO TYPEAHEAD, DESDE EL CONTROLADOR SE REALIZA LA CONSULTA Y SE DEVUELVE
Route::get('autocomplete', ['as' => 'autocomplete', 'uses' => 'PatientController@autocomplete']);
//RUTA PARA AUTOCOMPLETADO TYPEAHEAD, DESDE EL CONTROLADOR SE REALIZA LA CONSULTA Y SE DEVUELVE
Route::get('autocompletemedicamento', ['as' => 'autocompletemedicamento', 'uses' => 'NotificationController@autocompletemedicamento']);
//RUTA PARA AUTOCOMPLETADO CON SELECT2 PARA FACTORES DE RIESGOS
Route::get('factoriesgos', ['as' => 'factoriesgos', 'uses' => 'NotificationController@factoriesgos']);
//RUTA PARA CREAR NOTIFICACION A PARTIR DEL PACIENTE ELEGIDO
Route::any('crearnotificacion/{id?}', ['as' => 'crearnotificacion', 'uses' => 'NotificationController@crearnotificacion']);


//RUTA PARA EVALUAR NOTIFICACION ELIGIENDO INSUMO Y REACCION DESDE VER NOTIFICACION
Route::any('evaluarnotificacionind/{id?}', ['as' => 'evaluarnotificacionind', 'uses' => 'NotificationController@evaluarnotificacionind']);
//RUTA PARA GUARDAR ESTADO EN EVALUACION INDIVIDUAL
Route::any('guardarestadonotificacionind/{id?}', ['as' => 'guardarestadonotificacionind', 'uses' => 'NotificationController@guardarestadonotificacionind']);


//RUTA PARA EVALUAR ALGORITMO PRR
Route::any('evaluaralgoritmoprr', ['as' => 'evaluaralgoritmoprr', 'uses' => 'NotificationController@evaluaralgoritmoprr']);



//RUTA PARA ACCEDER AL ATC - MEDDRA
Route::resource('atcmeddra', 'AtcmeddraController');
Route::any('atc', ['as' => 'atc', 'uses' => 'AtcmeddraController@atc']);
Route::any('buscaratc', ['as' => 'buscaratc', 'uses' => 'AtcmeddraController@buscaratc']);
Route::any('meddra', ['as' => 'meddra', 'uses' => 'AtcmeddraController@meddra']);
Route::any('buscarmeddra', ['as' => 'buscarmeddra', 'uses' => 'AtcmeddraController@buscarmeddra']);


//RUTA PARA ACCEDER A PERFIL DE USUARIO
Route::resource('perfilusuario', 'PerfilusuarioController');
//RUTA PARA EDITAR USUARIO AUTENTICADO COMO NOTIFICADOR
Route::any('cambiarpassword', ['as' => 'cambiarpassword', 'uses' => 'PerfilusuarioController@cambiarpassword']);
Route::any('cambiarcontactos', ['as' => 'cambiarcontactos', 'uses' => 'PerfilusuarioController@cambiarcontactos']);
Route::any('cambiarhospital', ['as' => 'cambiarhospital', 'uses' => 'PerfilusuarioController@cambiarhospital']);
//RUTA PARA AGREGAR FOTOS
Route::any('subirarchivo', ['as' => 'subirarchivo', 'uses' => 'PerfilusuarioController@subirarchivo']);
Route::any('portada', ['as' => 'portada', 'uses' => 'PerfilusuarioController@portada']);
//RUTA PARA AGREGAR PROFESIONES Y ESPECIALIDADES A NOTIFICADOR
Route::any('datosprofesionales', ['as' => 'datosprofesionales', 'uses' => 'PerfilusuarioController@datosprofesionales']);
Route::any('datosespecialidades', ['as' => 'datosespecialidades', 'uses' => 'PerfilusuarioController@datosespecialidades']);
Route::get('especialidades', ['as' => 'especialidades', 'uses' => 'PerfilusuarioController@especialidades']);
Route::get('profesiones', ['as' => 'profesiones', 'uses' => 'PerfilusuarioController@profesiones']);

//RUTA PARA ACCDER A LA AYUDA
Route::any('ayuda', ['as' => 'ayuda', 'uses' => 'AyudaController@ayuda']);

//RUTA PARA ACCDER A LA CONFIGURACION IMPORTAR Y EXPORTAR
Route::any('configuracion', ['as' => 'configuracion', 'uses' => 'ConfiguracionController@configuracion']);
Route::any('importarexcelusuario', ['as' => 'importarexcelusuario', 'uses' => 'ExcelController@importarexcelusuario']);
Route::any('importarexcelATC', ['as' => 'importarexcelATC', 'uses' => 'ExcelController@importarexcelATC']);
Route::any('exportarexcelusuario/{type}', ['as' => 'exportarexcelusuario', 'uses' => 'ExcelController@exportarexcelusuario']);
Route::any('exportarPDFusuario', ['as' => 'exportarPDFusuario', 'uses' => 'ExcelController@exportarPDFusuario']);

Route::any('exportarexcelnotificacion/{id?}', ['as' => 'exportarexcelnotificacion', 'uses' => 'ExcelController@exportarexcelnotificacion']);
Route::any('exportarpdfnotificacion/{id?}', ['as' => 'exportarpdfnotificacion', 'uses' => 'ExcelController@exportarpdfnotificacion']);


//RUTA PARA ENVIAR EMAIL
Route::resource('mailreset', 'MailController');
Route::get('password/email','Auth\PasswordController@getEmail');
Route::post('password/email','Auth\PasswordController@postEmail');

Route::get('password/reset/{token}','Auth\PasswordController@getReset');
Route::post('password/reset','Auth\PasswordController@postReset');


//RUTA PARA GRAFICOS DE SINRAIM
Route::resource('informes', 'InformesController');
Route::any('informespacientes', ['as' => 'informespacientes', 'uses' => 'InformesController@informespacientes']);
Route::any('informesdepartamentos', ['as' => 'informesdepartamentos', 'uses' => 'InformesController@informesdepartamentos']);
Route::any('informesanhos', ['as' => 'informesanhos', 'uses' => 'InformesController@informesanhos']);
Route::any('informescuatro', ['as' => 'informescuatro', 'uses' => 'InformesController@informescuatro']);
Route::any('informescinco', ['as' => 'informescinco', 'uses' => 'InformesController@informescinco']);

//RUTA PARA EL MAPA
Route::get('/mapa', 'MapaController@index');




