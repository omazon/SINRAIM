<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;

class Risk extends Model
{
     protected $table = "risks";

	protected $fillable = ['id','name'];

	public function notifications(){
        return $this->belongsToMany('SINRAIM\Notification','notification_risk','risk_id','notificacion_id');
    }
}
