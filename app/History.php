<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;
use SINRAIM\Patient;

class History extends Model
{
    //Permite la asignacion masiva de los valores como columnas.
    protected $fillable = ['familiar', 'edad','patient_id','vida','descripcion_id'];
    protected $table = "histories";

     //funcion que permite la relacion muchos a muchos entre antecedentes familiares y paciente
     public function patients(){
        return $this->belongsTo('SINRAIM\Patient');
    }

    //Funcion estatica que permite realizar la consulta con la BD para saber los antecedentes familiares por paciente seleccionado
    //Esta recibe el ID y mediante eloquent se realiza el query.
   public static function Antecedentes($id)
    {
         return \DB::table('histories')
                        ->join('patients', 'histories.patient_id', '=', 'patients.id')
                        ->join('medical_diccionaries', 'histories.descripcion_id', '=', 'medical_diccionaries.iddescripcion')
                        ->select('histories.*','medical_diccionaries.descripcion')
                        ->where('patients.id', $id)->get(); 
    }

}
