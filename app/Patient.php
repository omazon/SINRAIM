<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use SINRAIM\History;

class Patient extends Model
{
    //Permite la asignacion masiva de los valores como columnas.
    protected $fillable = ['name', 'last_name', 'fechanacimiento', 'edad', 'genero'];

    protected $table = "patients";
    public $timestamps = false;
    //protected $dates = ['fechanacimiento'];

    //funcion que permite la relacion muchos a muchos entre paciente y antecedentes familiares
     public function histories(){
        return $this->hasMany('SINRAIM\History','patient_id','id');
    }

    //Funcion que permite convertir la fecha de nacimiento en formato string
	public function SetFechaNacimientoAttribute($dates) {
         $this->attributes['fechanacimiento'] = Carbon::createFromFormat('Y-m-d', $dates);
    }

    //Funcion que permite calcular la edad a partir de la fecha de nacimiento y agregarlo en formato string
	public function setEdadAttribute($dates) {         
        $this->attributes['edad'] = Carbon::parse($this->fechanacimiento)->diff(Carbon::now())->format('%y years %m months %d days');       
     }

      public function users(){
        return $this->belongsToMany('SINRAIM\User','user_patient','patient_id','user_id');
    }

    public static function verpacientenoti($idnotificador)
    {
         return \DB::table('patients')
                        ->join('user_patient', 'patients.id', '=', 'user_patient.patient_id')    
                        ->join('users', 'user_patient.user_id', '=', 'users.id')                    
                        ->select('patients.*')
                        ->where('users.id', '=', $idnotificador)
                        ->get(); 
    }


}
