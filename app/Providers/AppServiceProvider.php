<?php namespace SINRAIM\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Guard;
use SINRAIM\Notification;
use SINRAIM\User;
use Auth;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot(Guard $auth)
	{
		 view()->composer('*', function($view) use ($auth) {
		        $usuariomenus   = $auth->user();
		        
		        if(empty($usuariomenus)){
                	$idusuariomenus = "Sin Usuario Logueado";
	            }
	            else{
	                 $idusuariomenus = $usuariomenus->id;   
	            } 
		        //dd($idusuariomenus);
		        //$view->with('idusuariomenus', $idusuariomenus);	    	 

	         //$notificacionesmenus     = Notification::get()->take(5);
	         $notificacionesmenus     = Notification::where('user_id', '=', $idusuariomenus)->orderBy('updated_at','desc')->take(5)->get();

	         view()->share('notificacionesmenus', $notificacionesmenus);

         });
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'SINRAIM\Services\Registrar'
		);
	}

}
