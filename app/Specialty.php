<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;

class Specialty extends Model
{
	protected $table = "specialties";

	protected $fillable = ['id','name'];

    public function users(){
        return $this->belongsToMany('SINRAIM\User','user_specialty','specialty_id','user_id');
    }
}
