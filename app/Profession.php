<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $table = "professions";

	protected $fillable = ['id','name'];

    public function users(){
        return $this->belongsToMany('SINRAIM\User','user_profession','profession_id','user_id');
    }
}
