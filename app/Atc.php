<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;

class Atc extends Model
{
    protected $table = "atcs";

	protected $fillable = ['codatc', 'descripcion', 'viadministracion', 'principioactivo', 'unidadmedida', 'unidadreferencia', 'formafarmaceutica'];

	public $timestamps = false;
}
