<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;

class MedicaLassagna extends Model
{
    protected $table = "medica_lassagnas";

	protected $fillable = ['Pregunta1', 'Pregunta2', 'Pregunta3', 'Pregunta4', 'Pregunta5', 'Pregunta6', 'Pregunta7', 'Pregunta8', 'Pregunta9', 'Pregunta10', 'Resultado_valor', 'estadolassagna', 'medicalsupplie_id'];

	public $timestamps = false;

	public function medical_supplies(){
        return $this->belongsTo('SINRAIM\MedicalSupply');
    }
}
