<?php namespace SINRAIM;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
	use Authenticatable, Authorizable, CanResetPassword;

	protected $table = "users";

	protected $fillable = ['name', 'last_name', 'username', 'password', 'genero', 'path', 'portada','telefono', 'email', 'hospital_id', 'roles_id'];

	protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($valor){
        if(!empty($valor)){
            $this->attributes['password'] = \Hash::make($valor);
        }
    }

    public function patients(){
        return $this->belongsToMany('SINRAIM\Patient','user_patient','user_id','patient_id');
    }

    public function specialties(){
        return $this->belongsToMany('SINRAIM\Specialty','user_specialty','user_id','specialty_id');
    }

    public function professions(){
        return $this->belongsToMany('SINRAIM\Profession','user_profession','user_id','profession_id');
    }

}
