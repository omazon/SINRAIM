<?php

namespace SINRAIM;

use Illuminate\Database\Eloquent\Model;
use DB;

class MedicalDiccionary extends Model
{
    //Permite la asignacion masiva de los valores como columnas.
    protected $table = "medical_diccionaries";

	protected $fillable = ['iddescripcion','descripcion', 'idcategoria', 'categoria'];
}
