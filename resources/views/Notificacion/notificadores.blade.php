@extends('layouts.layout')

@section('title')
  SINRAIM | Notificadores
@stop
@section('css')


@section('body')  
<!-- Page -->
 <div class="page animsition">
    <div class="page-content container-fluid">
         <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
              <!-- Panel Wizard Form Container principal -->
              <div class="panel" id="exampleWizardForm">
                  <div class="panel-heading">
                      <h3 class="panel-title text-center">PANEL DE CONFIGURACION MEDICOS</h3>
                  </div></br>
                  <div class="panel-body">
                      <div id="tabla-medico" class="col-sm-12">
                          <div class="panel col-sm-8">
                              <header class="panel-heading">
                                <div class="panel-actions"></div>
                                <h1 class="panel-title"></h1>
                              </header>
                              <div class="panel-body">
                                  <table id="exampleAddRow" class="table table-hover dataTable table-striped width-full text-center" data-plugin="dataTable">
                                    <thead>
                                      <tr>
                                        <th>Nombre</th>
                                        <th>Posición</th>
                                        <th>Acceso</th>
                                        <th>Acción</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>Damon</td>
                                        <td>5516 Adolfo Green</td>
                                        <td><input type="checkbox" data-plugin="switchery" checked="true" /></td>
                                        <td class="actions">
                                          <a href="#" class="remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon fa-trash-o" aria-hidden="true"></i></a><a href="#"><i class="icon fa-pencil-square-o"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>Torrey</td>
                                        <td>1995 Richie Neck</td>
                                        <td><input type="checkbox" data-plugin="switchery" checked="true" /></td>
                                        <td class="actions">
                                          <a href="#" class="remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon fa-trash-o" aria-hidden="true"></i></a>
                                          <a data-toggle="modal" data-target="#medicosidebar"><i class="icon fa-pencil-square-o"></i></a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>Miracle</td>
                                        <td>176 Hirthe Squares</td>
                                        <td><input type="checkbox" data-plugin="switchery" checked="true" /></td>
                                        <td class="actions">
                                          <a href="#" class="remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon fa-trash-o" aria-hidden="true"></i></a><a href="#"><i class="icon fa-pencil-square-o"></i></a>
                                        </td>
                                      </tr>
                                      <tr class="gradeA">
                                        <td>Wilhelmine</td>
                                        <td>44727 O&#x27;Hara Union</td>
                                        <td><input type="checkbox" data-plugin="switchery" checked="true" /></td>
                                        <td class="actions">
                                          <a href="#" class="remove-row" data-toggle="tooltip" data-original-title="Remove"><i class="icon fa-trash-o" aria-hidden="true"></i></a><a href="#"><i class="icon fa-pencil-square-o"></i></a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                          </div><!--final DIV-->  
                        </div> <!--final TABLA MEDICO-->  
                  </div><!--final del BODY-->
               </div>
            </div>
          </div><!--final del row-->        
     </div><!--final del page-content-->     
  </div><!--final del animsition-->
  <!-- modales -->
  <div class="modal fade in" id="medicosidebar" aria-hidden="true" aria-labelledby="medicosidebar" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sidebar modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
        <img src="http://placehold.it/100x100" class="img-responsive center-block">
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">guardar</button>
      </div>
    </div>
  </div>
  </div>
  @section('javascript')
@stop