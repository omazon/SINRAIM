@extends('layouts.layout')

@section('title')
  Notificacion | Evaluar Notificacion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL PARA VER DETALLES DE NOTIFICACIONES ENVIADAS</h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
            <div class="form-group">
                 <label class="control-label" for="inputmedicamento">Nombre Insumo Médico</label>
                 <input type="text" class="form-control" id="inputmedicamento" name="medicamento" placeholder="Ingresar Nombre">
             </div>
             <div class="form-group">
                 <label class="control-label" for="inputfabricante">Nombre Reacción Aversa</label>
                 <input type="text" class="form-control" id="inputfabricante" name="fabricante" placeholder="Ingresar Nombre">
             </div></br>
             <div class="form-group text-right">
                <button type="button" class="btn btn-outline btn-primary">Buscar</button>
             </div>
                 <div class="row">
                 <div class="col-sm-offset-0">
                     <div class="col-sm-3 "><!-- Panel Wizard Form Container widgets -->
                          <div class="panel" id="exampleWizardForm">              
                              <div class="widget-content">
                                    <div class="bg-blue-600 white padding-30">
                                        <div class="font-size-20 clearfix">
                                            Power
                                            <span class="pull-right">
                                                <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                            </span>
                                        </div>
                                        <div class="font-size-14 red-200 clearfix">
                                            2015.1.20
                                            <span class="pull-right">+12.5(2.8%)</span>
                                        </div>            
                                    </div>
                                    <div class="padding-30">
                                      <div class="row no-space">
                                        <div class="col-xs-7">
                                          <p>
                                            <span class="icon wb-medium-point cyan-600 margin-right-5"></span>35% Dietary intake</p>
                                          <p class="margin-bottom-20">
                                            <span class="icon wb-medium-point red-600 margin-right-5"></span>65% Motion capture</p>
                                          <p>TOTAL POWER</p>
                                          <p class="font-size-20 margin-bottom-0" style="line-height:1;">4200</p>
                                        </div>
                                      </div>
                                    </div>
                              </div>          
                          </div><!--final del panel-->
                      </div>    
                      <div class="col-sm-3 "><!-- Panel Wizard Form Container widgets -->
                          <div class="panel" id="exampleWizardForm">              
                              <div class="widget-content">
                                  <div class="bg-orange-600 white padding-30">
                                      <div class="font-size-20 clearfix">
                                          Power
                                          <span class="pull-right">
                                              <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                          </span>
                                      </div>
                                      <div class="font-size-14 red-200 clearfix">
                                          2015.1.20
                                          <span class="pull-right">+12.5(2.8%)</span>
                                      </div>            
                                  </div>
                                  <div class="padding-30">
                                    <div class="row no-space">
                                      <div class="col-xs-7">
                                        <p>
                                          <span class="icon wb-medium-point cyan-600 margin-right-5"></span>35% Dietary intake</p>
                                        <p class="margin-bottom-20">
                                          <span class="icon wb-medium-point red-600 margin-right-5"></span>65% Motion capture</p>
                                        <p>TOTAL POWER</p>
                                        <p class="font-size-20 margin-bottom-0" style="line-height:1;">4200</p>
                                      </div>
                                    </div>
                                  </div>
                            </div>          
                          </div><!--final del panel-->
                       </div>
                       <div class="col-sm-3 "><!-- Panel Wizard Form Container widgets -->
                          <div class="panel" id="exampleWizardForm">              
                              <div class="widget-content">
                                    <div class="bg-red-600 white padding-30">
                                        <div class="font-size-20 clearfix">
                                            Power
                                            <span class="pull-right">
                                                <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                            </span>
                                        </div>
                                        <div class="font-size-14 red-200 clearfix">
                                            2015.1.20
                                            <span class="pull-right">+12.5(2.8%)</span>
                                        </div>            
                                    </div>
                                    <div class="padding-30">
                                      <div class="row no-space">
                                        <div class="col-xs-7">
                                          <p>
                                            <span class="icon wb-medium-point cyan-600 margin-right-5"></span>35% Dietary intake</p>
                                          <p class="margin-bottom-20">
                                            <span class="icon wb-medium-point red-600 margin-right-5"></span>65% Motion capture</p>
                                          <p>TOTAL POWER</p>
                                          <p class="font-size-20 margin-bottom-0" style="line-height:1;">4200</p>
                                        </div>
                                      </div>
                                    </div>
                              </div>          
                          </div><!--final del panel-->
                      </div>   
                            <div class="col-sm-3 "><!-- Panel Wizard Form Container widgets -->
                          <div class="panel" id="exampleWizardForm">              
                              <div class="widget-content">
                                    <div class="bg-green-600 white padding-30">
                                        <div class="font-size-20 clearfix">
                                            Power
                                            <span class="pull-right">
                                                <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                            </span>
                                        </div>
                                        <div class="font-size-14 red-200 clearfix">
                                            2015.1.20
                                            <span class="pull-right">+12.5(2.8%)</span>
                                        </div>            
                                    </div>
                                    <div class="padding-30">
                                      <div class="row no-space">
                                        <div class="col-xs-7">
                                          <p>
                                            <span class="icon wb-medium-point cyan-600 margin-right-5"></span>35% Dietary intake</p>
                                          <p class="margin-bottom-20">
                                            <span class="icon wb-medium-point red-600 margin-right-5"></span>65% Motion capture</p>
                                          <p>TOTAL POWER</p>
                                          <p class="font-size-20 margin-bottom-0" style="line-height:1;">4200</p>
                                        </div>
                                      </div>
                                    </div>
                              </div>          
                          </div><!--final del panel-->
                      </div>   
                    </div>
                </div>  
                
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  


 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
      
    @endpush
@endsection
    
@stop

