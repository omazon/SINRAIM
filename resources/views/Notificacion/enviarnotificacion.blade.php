@extends('layouts.layout')

@section('title')
  Notificacion | Enviar Notificacion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div class="page animsition">
    <div class="page-content container-fluid">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
          <!-- Panel Wizard Form Container -->
          <div class="panel" id="exampleWizardForm">
            <div class="panel-heading">
              <h3 class="panel-title">Enviar Notificación Online</h3>
            </div>
            <div class="panel-body">
              <!-- Steps -->
              <div class="pearls row" data-plugin="matchHeight" data-by-row="true" role="tablist">
                <div class="pearl col-xs-6 col-sm-3 current" data-target="#exampleAccount" role="tab">
                    <div class="pearl-icon"><i class="icon wb-user" aria-hidden="true"></i></div>
                  <span class="pearl-title">Datos del Paciente</span>
                </div>
                <div class="pearl col-xs-6 col-sm-3" data-target="#exampleBilling" role="tab">
                  <div class="pearl-icon"><i class="icon wb-rubber" aria-hidden="true"></i></div>
                  <span class="pearl-title">Insumos Médicos</span>
                </div>

                <div class="pearl col-xs-6 col-sm-3" data-target="#exampleBilling2" role="tab">
                  <div class="pearl-icon"><i class="icon wb-link-broken" aria-hidden="true"></i></div>
                  <span class="pearl-title">Reacción Adversa</span>
                </div>
               
                <div class="pearl col-xs-6 col-sm-3" data-target="#exampleGetting" role="tab">
                  <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
                  <span class="pearl-title">Confirmación</span>
                </div>
              </div>
              <div class="wizard-content">
              <form id="exampleAccountForm">
                <div class="wizard-pane active" id="exampleAccount" role="tabpanel">
                  <div class="panel-group panel-validacion" id="DatosPaciente" aria-multiselectable="true"
                role="tablist">
                  <div class="panel bg-blue-grey-200">
                    <div class="panel-heading" id="EncabezadoDatosPersonales" role="tab">
                      <div class="popover-primary" data-title="Pasos para enviar Notificación" data-content="En cada paso de la notificación, puede presionar en el signo de + para llenar el formulario, completarlo y darle siguiente para continuar" data-trigger="hover" data-placement="top" data-toggle="popover">
                        <a class="panel-title"  data-toggle="collapse" href="#DatosPersonales"
                      data-parent="#DatosPaciente" aria-expanded="true"
                      aria-controls="DatosPersonales">
                      Datos Personales
                    </a>
                      </div>
                    </div>
                    <div class="panel-collapse collapse in" id="DatosPersonales" aria-labelledby="EncabezadoDatosPersonales"
                    role="tabpanel">
                      <div class="panel-body">
                                 <div class="form-group">
                                  <label class="control-label" for="inputUserName">Nombre</label>
                                  <input type="text" class="form-control" id="inputnombre" name="nombre" required="required">
                                </div>
                                <div class="form-group">
                                  <label class="control-label" for="inputUserName">Apellido</label>
                                  <input type="text" class="form-control" id="inputapellido" name="apellido"
                                  required="required">
                                </div>
                                
                              <div class="form-group">
                                <p>Escoja un género</p>
                                <select data-plugin="selectpicker" title="Género" name="requiredSelect" id="departamento" data-style="btn-outline btn-primary" class="form-control" required="required">
                                    <option value="" data-icon="fa-female">Femenino</option>
                                    <option value="" data-divider="true"></option>
                                    <option value="" data-icon="fa-male">Masculino</option>
                                </select>
                              </div>
                                              



                           
                              <div class="form-group">
                                  <div class="row">
                                      <div class="col-sm-6 col-xs-12">
                                          <label class="control-label">Edad</label><br>
                                          <input type="text" class="control-form" data-plugin="asSpinner" data-min="0" data-max="150">
                                      </div>
                                      <div class="col-sm-6 col-xs-12 text-right">
                                          <label class="control-label">Peso (kg)</label><br>
                                          <input type="text" class="control-form" data-plugin="asSpinner" data-max="250" data-min="1">
                                      </div>
                                  </div>
                              </div>
                              
                        </div>
                      </div>
                    </div>
                   <div class="panel bg-blue-grey-200">
                    <div class="panel-heading" id="EncabezadoUbicacion" role="tab">
                      <a class="panel-title collapsed" data-toggle="collapse" href="#DatosUbicacion"
                      data-parent="#DatosPaciente" aria-expanded="false"
                      aria-controls="DatosUbicacion">
                      Ubicación
                    </a>
                    </div>
                    <div class="panel-collapse collapse" id="DatosUbicacion" aria-labelledby="EncabezadoUbicacion"
                    role="tabpanel">                    
                      <div class="panel-body">
                           <div class="row">
                              <form class="form-group">
                                
                                  <div class="col-sm-12">
                                      <!--panel con boton de cierre-->
                                        <div class="panel bg-green-500">
                                          <div class="panel-heading">
                                            
                                            <div class="panel-actions">
                                              <a data-toggle="panel-close" aria-hidden="true" class="panel-action icon wb-close"></a>
                                            </div>
                                          </div>                                          
                                          <div class="panel-body">
                                              <p class="white">El departamento y el Municipio se agregan automáticamente en dependencia de la procedencia del médico notificador. Agregue solamente el Centro de Salud y de Click en siguiente.</p>
                                          </div>
                                        </div>

                                      <!--fin del panel del boton de cierre--><br>
                                     <label class="control-label" for="">Escoja un Departamento</label><br>
                                     <select class="col-sm-12 col-xs-12 form-control" disabled data-plugin="selectpicker" data-live-search="true" title="Departamento" data-style="btn-outline btn-primary">
                                    <optgroup label="Región Pacífico">
                                        <option value="">Chinandega</option>
                                        <option value="">León</option>
                                        <option value="">Managua</option>
                                        <option value="">Masaya</option>
                                        <option value="">Granada</option>
                                        <option value="">Carazo</option>
                                        <option value="">Rivas</option>
                                    </optgroup>
                                    <optgroup label="Región Central">
                                        <option value="">Nueva Segovia</option>
                                        <option value="">Madriz</option>
                                        <option value="">Estelí</option>
                                        <option value="">Jinotega</option>
                                        <option value="">Matagalpa</option>
                                        <option value="">Boaco</option>
                                        <option value="">Chontales</option>
                                        <option value="">Río San Juan</option>
                                    </optgroup>
                                    <optgroup label="Región Atlático">
                                        <option value="">Región Autónoma Atlático Norte</option>
                                        <option value="">Región Antónoma Atlático Sur</option>
                                    </optgroup>

                                     </select>  
                                  </div>
                                  <div class="col-sm-12">
                                      <label class="control-label">Escoja un Municipio</label><br>
                                      <select disabled class="col-sm-12 col-xs-12 form-control" data-plugin="selectpicker" data-live-search="true" title="Municipio" data-style="btn-outline btn-primary">
                                        <optgroup label="Región Pacífico">
                                            <option value="">Chinandega</option>
                                            <option value="">Chinandega</option>
                                            <option value="">Chinandega</option>                       
                                        </optgroup>
                                      </select>
                                  </div>
                                  <div class="col-sm-12">
                                      <label class="control-label" for="">Escoja un Centro de Salud</label><br>
                                      <select class="col-sm-12 col-xs-12 form-control" data-plugin="selectpicker" data-live-search="true" title="Municipio" data-style="btn-outline btn-primary">
                                        <optgroup label="Región Pacífico">
                                            <option value="">Chinandega</option>
                                            <option value="">Chinandega</option>
                                            <option value="">Chinandega</option>                       
                                        </optgroup>
                                      </select>
                                  </div>                                        
                              </form>

                           </div> 
                        </div>
                    </div>
                  </div><br><br>  
                  </div>
                 
                </div>
              </form>
                              
                </div>
            <!--INSUMOS MEDICOS-->
                <div class="wizard-pane active" id="exampleBilling" role="tabpanel">
                    <form id="exampleBillingForm">
                     <div class="panel-group panel-validacion" id="InsumoMedico" aria-multiselectable="true">       
                             <br>
                        <div class="row">                          
                               <div class="col-xs-12 col-lg-12 text-center">
                                      <h4>Agregar otros Insumos Médicos</h4>
                                      <div class="popover-primary" data-title="Ingresar Insumos Médicos" data-content="Aca debe agregar los insumos médicos sospechos como los que consume normalmente. Click aqui para agregar" data-trigger="hover" data-placement="top" data-toggle="popover">
                                         <button data-toggle="modal" data-target="#OtrosInsumos" class="btn btn-primary">                          
                                           Click aqui para agregar
                                       </button>
                                      </div>

                               </div>
                            </div><br><br>
                        <div class="row">
                               <h5>Medicamentos Agregados</h5>
                                <div class="col-xs-12 col-lg-12">
                                      <!--inicion de la table editable inferior-->                           
                                      <table class="table table  table-bordered table-hover table-striped" id="exampleAddRow">
                                        <thead>
                                          <tr>
                                            <th>Nombre Genérico</th>
                                            <th>Dosis</th>
                                            <th>Fecha Vencimiento</th>
                                            <th>Acciones</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr class="gradeA">
                                            <td>1</td>
                                            <td>2</td>
                                            <td>3</td>
                                            <td class="actions">
                                              <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                                              data-toggle="tooltip" data-original-title="Gurdar"><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                              <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                                              data-toggle="tooltip" data-original-title="Borrar"><i class="icon wb-close" aria-hidden="true"></i></a>
                                              <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                              data-toggle="tooltip" data-original-title="Editar"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                              <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                              data-toggle="tooltip" data-original-title="Remover"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                            </td>
                                          </tr>
                                          </tbody>
                                        </table>                                           <!--final de la table inferior-->
                                </div>
                            </div>           
                     </div> 
                    </form>
                </div>
                  <br>
                  
                  <!--Modal de Agregar Insumo Medico-->
                  <div class="modal fade modal-primary modal-super-scaled" id="OtrosInsumos" aria-hidden="true" aria-labelledby="OtrosInsumos" role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-center">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true"></span>
                                  </button>
                                  <h4 class="modal-title">Agregue la Información del Insumo Médico</h4>
                              </div>
                              <div class="modal-body">                                
                                 <div class="panel-group panel-group-continuous margin-2" id="AcordionInternoInsumoMedico" aria-multiselectable="true" role="tablist">
                                   <!--cuerpo del modal--> 
                                   <h5>Agregar Insumo Médico</h5>
                                    <!--Codigo Cabecera UNO-->
                                      <div class="panel bg-blue-grey-200">
                                         <div class="panel-heading" id="CabeceraSegunda" role="tab">
                                             <a href="#AcordionDos" data-parent="#AcordionInternoInsumoMedico" data-toggle="collapse"  aria-controls="AcordionDos" aria-expanded="false" class="panel-title collapsed">
                                                 <i class="md-place"></i>Datos Indispensable del Insumo Médico
                                             </a>
                                         </div>
                                         <div class="panel-collapse collapse" id="AcordionDos" aria-labelledby="CabeceraSegunda" role="tabpanel">
                                             <div class="panel-body">
                                                 <!--cuerpo Acordion Dos-->
                                                 <div class="row">
                                                    <div class="form-group">
                                                          <label class="control-label" for="inputmedicamento2">Nombre Genérico Insumo Médico</label>
                                                          <input type="text" class="form-control" id="inputmedicamento2" name="medicamento" placeholder="Nombre">
                                                      </div>
                                                     <div class="col-sm-6">
                                                        <div class="form-group">
                                                         <label for="">Dósis Tomada</label>
                                                         <input type="text" class="form-control" data-plugin="asSpinner" value="0" /> 
                                                        </div>                                                         
                                                     </div>
                                                     <div class="col-sm-6">
                                                        <div class="form-group">
                                                          <label class="control-label" for="">Unidad de Medida</label>
                                                          <select class="form-control" data-plugin="selectpicker" title="Unidad" data-style="btn-outline btn-primary">
                                                                <optgroup label="Región Pacífico">
                                                                    <option value="">Chinandega</option>
                                                                    <option value="">Chinandega</option>
                                                                    <option value="">Chinandega</option>                       
                                                                </optgroup>
                                                          </select>
                                                        </div>                                                         
                                                     </div>
                                                     <div class="col-sm-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="">Frecuencia de Consumo</label><br>  
                                                            <select class="form-control" data-plugin="selectpicker" title="Frecuencia" data-style="btn-outline btn-primary">                            
                                                                    <option value="">Cada 24 Horas (1 vez al dia)</option>
                                                                    <option value="">cada 12 Horas (2 veces al dia)</option>
                                                                    <option value="">Cada 8 Horas (3 veces al dia)</option>
                                                                    <option value="">Cada 6 Horas (4 veces al dia)</option>  
                                                            </select> 
                                                         </div>
                                                     </div>
                                                     <div class="col-sm-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="">Tipo de Insumo Médico</label><br>  
                                                            <select class="form-control" data-plugin="selectpicker" title="Frecuencia" data-style="btn-outline btn-primary">                            
                                                                    <option value="">Insomo Médico Sospechoso</option>
                                                                    <option value="">Insumo Médico Concumitante</option>                              
                                                            </select> 
                                                         </div>
                                                     </div>  
                                                     <div class="col-sm-12">
                                                         <div class="form-group">
                                                            <label class="control-label" for="">Vía de Administración</label><br>  
                                                            <select class="form-control" data-plugin="selectpicker" title="Medida" data-style="btn-outline btn-primary">
                                                                <optgroup label="Región Pacífico">
                                                                    <option value="">Chinandega</option>
                                                                    <option value="">Chinandega</option>
                                                                    <option value="">Chinandega</option>                       
                                                                </optgroup>
                                                            </select> 
                                                         </div>
                                                     </div>                      
                                                 </div>                                                       
                                                     <div class="input-daterange" data-plugin="datepicker">
                                                       <label for="">Inicio y Final de Toma de Medicamento </label><br>
                                                        <div class="input-group">                                
                                                            <span class="input-group-addon">
                                                            <i class="icon wb-calendar" aria-hidden="true"></i>
                                                            </span>
                                                            <input type="text" class="form-control" name="start">
                                                        </div>
                                                        <div class="input-group">                                   
                                                            <span class="input-group-addon">a</span>
                                                            <input type="text" class="form-control" name="end">
                                                        </div>
                                                     </div>                                             
                                                <!--final cuarpo acordion dos-->                                        
                                             </div>
                                         </div>
                                     </div><br>

                                     
                                     <!--final cabecera uno-->
                                     
                                     <!--codigo cabecera dos-->
                                     <div class="panel bg-blue-grey-200">                                        
                                         <div class="panel-heading" id="CabeceraPrimero" role="tab">
                                             <a href="#AcordionUno" data-parent="#AcordionInternoInsumoMedico" data-toggle="collapse" aria-controls="AcordionUno" aria-expanded="false" class="panel-title collapsed">
                                                 <i class="md-palette"></i>Otros Datos
                                             </a>
                                         </div>
                                         <div class="panel-collapse collapse" id="AcordionUno" aria-labelledby="CabeceraPrimero" role="tabpanel">
                                             <div class="panel-body">
                                                 <!--Cuerpo Acordion Uno-->
                                                 <form>                                                      
                                                      <div class="row">
                                                          <div class="col-sm-12">
                                                             <label for="">Fecha de Vencimiento</label>
                                                              <div class="input-group">
                                                                  <span class="input-group-addon">
                                                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                                                  </span>
                                                                  <input type="text" class="form-control" data-plugin="datepicker">
                                                              </div> 
                                                          </div>
                                                          <div class="col-sm-12">
                                                               <div class="form-group">
                                                                  <label class="control-label" for="inputfabricante2">Fabricante</label>
                                                                  <input type="text" class="form-control" id="inputfabricante2" name="fabricante" placeholder="Fabricante">
                                                               </div>
                                                          </div>
                                                          <div class="col-sm-12">
                                                              <div class="form-group">
                                                                  <label class="control-label" for="inputfabricante2">Número de Lote</label>
                                                                  <input type="text" class="form-control" id="inputfabricante2" name="fabricante" placeholder="Número de Lote">
                                                              </div>
                                                          </div>
                                                          <div class="row">
                                                           <div class="col-sm-12">
                                                             <label for="">Motivo de la Prescripción</label>
                                                              <textarea rows="3" class="maxlength-textarea form-control" data-plugin="maxlength" maxlength="100" placeholder="Este campo de texto tiene un límite de 100 caracteres">                                            </textarea> 
                                                           </div>
                                                 </div> 
                                                      </div>     
                                                 </form>                              
                                             </div>                                                          
                                         </div>
                                     </div><br>
                                     <!--final cabecera dos-->
                                 </div>
                              </div>
                              <!--fin cuerpo modal-->
                              <div class="modal-footer">
                                  <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                  <button class="btn btn-primary">Aceptar</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!--Fin del Modal de Insumo medico-->                  
                </div>
                <!--final del paso 2-->
                
                <!--inicio de la pestaña de reaccion adversa-->
                <div class="wizard-pane active" id="exampleBilling2" role="tabpanel">
                 <div class="panel-group" id="ReaccionAdversa" aria-multiselectable="true">
                  <form id="exampleBillingForm2">    
                      <div class="panel-validacion">
                          <div class="row">  
                              <div class="col-xs-12 col-lg-10 col-lg-offset-1 text-center">
                                 <h2>Estas a un paso de terminar, te falta muy poco</h2> 
                            <p>Para finalizar le presentamos la lista de los medicamentos sospechosos, de los cuales requerimos de su apoyo completando una última información, para ello seleccione por cada medicamento el botón de agregar y llene el pequeño formulario adjunto. Muchas Gracias</p>    
                              </div>                                                    
                              </div>
                          <div class="row">                
                               <div class="col-xs-12 col-lg-10 col-lg-offset-1 text-center">
                                     <h5 class="margin-left-20 text-left">Reacciones Adversas que se presentaron</h5><br>
                                      <h4>Agregar Reacciones Adversas</h4>
                                      <div class="popover-primary" data-title="Ingresar Reacciones Adversas" data-content="Es hora de registrar las reacciones adversas sufridas y los antecedentes familiares para poder evaluar mejor el caso" data-trigger="hover" data-placement="top" data-toggle="popover">
                                        <button data-toggle="modal" data-target="#OtrasReacciones" class="btn btn-primary">                                          
                                           Click aqui para agregar
                                       </button>
                                      </div><br><br>
                                       
                               </div> 
                                    </div>
                          <div class="row"> 
                            <div class="col-xs-11 col-lg-10 col-lg-offset-1"> 
                                  <!--inicion de la table editable inferior-->                           
                                  <table class="table table  table-bordered table-hover table-striped" id="exampleAddRow2">
                                    <thead>
                                      <tr>
                                        <th>Nombre Reacción</th>
                                        <th>Inicio Reacción</th>
                                        <th>Fin Reacción</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="gradeA">
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td class="actions">
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                                          data-toggle="tooltip" data-original-title="Gurdar"><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                                          data-toggle="tooltip" data-original-title="Borrar"><i class="icon wb-close" aria-hidden="true"></i></a>
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                          data-toggle="tooltip" data-original-title="Editar"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                          data-toggle="tooltip" data-original-title="Remover"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                        </td>
                                      </tr>
                                      </tbody>
                                    </table>                                           <!--final de la table inferior-->
                            </div>
                        </div>
                       
                            <br><br>  
                            
                               <!--Modal de Agregar Reaccion Adversa-->
                  <div class="modal fade modal-primary modal-super-scaled" id="OtrasReacciones" aria-hidden="true" aria-labelledby="OtrasReacciones" role="dialog" tabindex="-1">
                      <div class="modal-dialog modal-center">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true"></span>
                                  </button>
                                  <h4 class="modal-title">Agregue la Información de la Reacción Adversa</h4>
                              </div>
                              <div class="modal-body">                                
                                 <div class="panel-group panel-group-continuous margin-2" id="AcordionInternoReaccionAdversa" aria-multiselectable="true" role="tablist">
                                   <!--cuerpo del modal--> 
                                   <h5>Reacciones Adversas presentadas con el medicamento</h5>
                                    <!--Codigo Cabecera UNO-->
                                     <div class="panel bg-blue-grey-200">                                        
                                         <div class="panel-heading" id="CabeceraPrimeroReaccionAdversa" role="tab">
                                             <a href="#AcordionUnoReaccionAdversa" data-parent="#AcordionInternoReaccionAdversa" data-toggle="collapse" aria-controls="AcordionUnoReaccionAdversa" aria-expanded="false" class="panel-title collapsed">
                                                 <i class="md-palette"></i>Datos de la Reacción Adversa
                                             </a>
                                         </div>
                                         <div class="panel-collapse collapse" id="AcordionUnoReaccionAdversa" aria-labelledby="CabeceraPrimeroReaccionAdversa" role="tabpanel">
                                             <div class="panel-body">
                                                 <!--Cuerpo Acordion Uno-->
                                                 <form>
                                                      <div class="form-group">
                                                          <label class="control-label" for="inputmedicamento2">Nombre Reacción Adversa</label>
                                                          <input type="text" class="form-control" id="inputreaccion" name="reaccion" placeholder="Nombre">
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="control-label" for="">Intensidad</label>
                                                          <select class="form-control" data-plugin="selectpicker" title="Nivel" data-style="btn-outline btn-primary">                                   
                                                                    <option value="">LEVE</option>
                                                                    <option value="">MODERADA</option>
                                                                    <option value="">SEVERA</option>                          
                                                          </select>
                                                        </div> 
                                                         <div class="input-daterange" data-plugin="datepicker">
                                                           <label for="">Inicio y Final de la Reacción Adversa </label><br>
                                                            <div class="input-group">                                
                                                                <span class="input-group-addon">
                                                                <i class="icon wb-calendar" aria-hidden="true"></i>
                                                                </span>
                                                                <input type="text" class="form-control" name="start">
                                                            </div>
                                                            <div class="input-group">                                   
                                                                <span class="input-group-addon">a</span>
                                                                <input type="text" class="form-control" name="end">
                                                            </div>
                                                         </div>  
                                                         <div class="form-group">
                                                          <label class="control-label" for="">Desenlace</label>
                                                          <select class="form-control" data-plugin="selectpicker" title="Estado" data-style="btn-outline btn-primary">                                   
                                                                    <option value="">RECUPERADO</option>
                                                                    <option value="">SECUELAS</option>
                                                                    <option value="">MORTAL</option>                          
                                                          </select>
                                                        </div> 
                                                        <form>
                                                           <label class="control-label" for="">¿Requirió Reingreso?</label>
                                                           <div class="row">
                                                               <div class="col-xs-4 text-center">
                                                                   <input class="icheckbox-primary" type="radio" name="reaccion" value="si" id="r_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue">SI  
                                                               </div>
                                                               <div class="col-xs-4">
                                                                   <input type="radio" class="icheck-primary" name ="reaccion" value="no" id="r_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue">NO         
                                                               </div>
                                                           </div>                                                           
                                                        </form>      
                                                                              
                                             </div>                                                          
                                         </div>
                                     </div><br>
                                     <!--final cabecera uno-->
                                     
                                     <!--codigo cabecera dos-->
                                     <div class="panel bg-blue-grey-200">
                                         <div class="panel-heading" id="CabeceraSegundaReaccionAdversa" role="tab">
                                             <a href="#AcordionDosReaccionAdversa" data-parent="#AcordionInternoReaccionAdversa" data-toggle="collapse"  aria-controls="AcordionDosReaccionAdversa" aria-expanded="false" class="panel-title collapsed">
                                                 <i class="md-place"></i>Información Adicional
                                             </a>
                                         </div>
                                         <div class="panel-collapse collapse" id="AcordionDosReaccionAdversa" aria-labelledby="CabeceraSegundaReaccionAdversa" role="tabpanel">
                                             <div class="panel-body">
                                                 <!--cuerpo Acordion Dos-->                                               
                                                      <div class="col-xs-12 col-lg-10 col-lg-offset-1 text-center">
                                                         <h5 class="margin-left-20 text-left">Reacciones Adversas que se presentaron</h5><br>
                                                         <h4>Agregar Reacciones Adversas</h4>
                                                         <button data-toggle="modal" data-target="#ReaccionAdversaModalLateral" class="btn btn-primary">
                                                           <i class="icon wb-plus"></i>
                                                               Agregar
                                                         </button><br><br>
                                                     </div> 
                                                     <div class="col-xs-12 col-lg-12"> 
                                  <!--inicion de la table editable inferior-->                           
                                  <table class="table table  table-bordered table-hover table-striped" id="exampleAddRow3">
                                    <thead>
                                      <tr>
                                        <th>Familia</th>
                                        <th>Edad</th>
                                        <th>Enfermedad</th>
                                        <th>Acciones</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr class="gradeA">
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td class="actions">
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                                          data-toggle="tooltip" data-original-title="Gurdar"><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                                          data-toggle="tooltip" data-original-title="Borrar"><i class="icon wb-close" aria-hidden="true"></i></a>
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                          data-toggle="tooltip" data-original-title="Editar"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                          <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                          data-toggle="tooltip" data-original-title="Remover"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                        </td>
                                      </tr>
                                      </tbody>
                                  </table><!--final de la table inferior-->
                                      <div class="row">
                                         <div class="col-sm-12">
                                           <label for="">Factores de Riesgo que Asocia</label>
                                            <textarea rows="3" class="maxlength-textarea form-control" data-plugin="maxlength" maxlength="100" placeholder="Describa brevemente si padece de alguna alergia, si fuma, toma licor o cualquier otro factor que afecte su salud (100)">                                            </textarea> 
                                         </div>
                                     </div>                                     
                            </div>  
                                                 </div>                                               
                                                <!--final cuarpo acordion dos-->                                        
                                             </div>
                                         </div>
                                     </div>
                                     <!--final cabecera dos-->
                                 </div>
                                 <div class="modal-footer">
                                  <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                  <button class="btn btn-primary">Aceptar</button>
                              </div>
                              </div>
                              <!--fin cuerpo modal-->                              
                          </div>
                      </div>
                  </div>
                  <!--Fin del Modal de Reaccion Adversa-->   
                      </div>          
                  </form>
                 </div>
                 <!--Modal lateral para agregar parentesco-->
                 <div tabindex="-1" class="modal fade" id="ReaccionAdversaModalLateral" role="dialog" aria-hidden="true" aria-labelledby="ReaccionAdversaModealLateral">
                     <div class="modal-dialog modal-sidebar modal-sm">
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true">X</span>
                                 </button>
                                 <h4 class="modal-title">ANTECEDENTE FAMILIAR</h4>
                             </div>
                             <div class="modal-body">
                                 <label class="control-label">Familiar</label>
                                 <form>
                                  <div class="margin-left-30">
                                     <div class="row">
                                       <div class="col-sm-12">
                                           <input class="icheckbox-primary" type="radio" name="familia" value="abuelo" id="f_abuelo" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">Abuelo 
                                       </div>
                                       <div class="col-sm-12">
                                           <input type="radio" class="icheck-primary" name ="familia" value="abuela" id="f_abuela" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">Abuela         
                                       </div>
                                       <div class="col-sm-12">
                                           <input type="radio" class="icheck-primary" name ="familia" value="papa" id="f_papa" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">Papá         
                                       </div>
                                       <div class="col-sm-12">
                                           <input type="radio" class="icheck-primary" name ="familia" value="mama" id="f_mama" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">Mamá         
                                       </div>
                                       <div class="col-sm-12">
                                           <input type="radio" class="icheck-primary" name ="familia" value="hermano" id="f_hermano" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">Hermanos(as)       
                                       </div>
                                       <div class="col-sm-12">
                                           <input type="radio" class="icheck-primary" name ="familia" value="tio" id="f_tio" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">Tíos(as)   
                                       </div>
                                   </div>  
                                  </div> <br>                                  
                                   <div class="row">
                                       <div class="col-sm-12">
                                           <label for="" class="control-label">
                                               Edad
                                           </label><br>
                                           <input type="text" data-plugin="asSpinner" data-min="1" data-max="150" >                                          
                                       </div>
                                       <div class="col-sm-12">
                                           <br><label for="" class="control-label">¿Esta con vida?</label>
                                       </div>                                       
                                       <div class="col-sm-6">                                            
                                            <input type="radio" class="icheck-primary" name ="f_vida" value="si" id="f_tio" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI 
                                       </div>
                                       <div class="col-sm-6">
                                            <input type="radio" class="icheck-primary" name ="f_vida" value="no" id="f_tio" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO 
                                       </div>
                                       <div class="col-sm-12">
                                           <br><label for="" class="control-label">
                                               Enfermedad
                                           </label>
                                           <input type="text" class="form-control" placeholder="Ponga aquí la enfermedad">
                                           
                                       </div>
                                   </div>                                                                            
                                </form>
                                 
                             </div>
                             <div class="modal-footer">
                                 <button class="btn btn-primary btn-block" type="button">Guardar</button>
                                 <button class="btn btn-default btn-block" type="button" data-dismiss="modal">Cerrar</button>
                             </div>
                         </div>
                     </div>
                 </div>
                 <!--fin modal de parentesco-->
                
                  
                  
                <div class="wizard-pane active" id="exampleGetting" role="tabpanel">
                    <div class="panel-group panel-validacion">
                        
                   <div class="col-sm-12 text-center">
                     <h5>Para finalizar solo necesitamos una última intervención. 5 preguntas de control que nos ayudaran a determinar si la reacción fue causada por el medicamento.
                      </h5> 
                       <button data-toggle="modal" data-target="#final_sidebar" class="btn btn-primary">
                              <i class="icon wb-plus"></i> Agregar
                        </button>
                        <!--inicion de la table editable inferior-->  
                        <br><br><br>
                           <table class="table table  table-bordered table-hover table-striped" id="exampleAddRow4">
                              <thead>
                                <tr>
                                    <th>Familia</th>
                                    <th>Edad</th>
                                    <th>Enfermedad</th>
                                    <th>Acciones</th>                                        
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="gradeA">
                                  <td>1</td>
                                  <td>2</td>
                                  <td>3</td>
                                  <td class="actions">
                                    <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing save-row"
                                    data-toggle="tooltip" data-original-title="Gurdar"><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                    <a href="#" class="btn btn-sm btn-icon btn-pure btn-default hidden on-editing cancel-row"
                                    data-toggle="tooltip" data-original-title="Borrar"><i class="icon wb-close" aria-hidden="true"></i></a>
                                    <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                    data-toggle="tooltip" data-original-title="Editar"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                    <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                    data-toggle="tooltip" data-original-title="Remover"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                  </td>                                       
                                </tr>
                                </tbody>
                            </table>                                     <!--final de la table inferior-->
                    </div>

                  
                                    <!--modal de final-->
                                    <div class="modal fade" id="final_sidebar" aria-hidden="true" aria-labelledby="final_sidebar"
                        role="dialog" tabindex="-1">
                                      <div class="modal-dialog modal-sidebar modal-lg">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                            </button>
                                            <h3 class="modal-title">Información para Evaluación (Use el scroll del ratón para bajar)</h3>
                                          </div>
                                          <div class="modal-body"> 
                                              <div style="overflow-y: scroll; height: 500px; overflow-x: hidden ">
                                                 <ol><br><br>
                                                          <li>¿Existen evidencias previas sobre que este medicamento causa esta reaccion?<br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="1" value="si" id="1_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="1" value="no" id="1_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                      <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="1" value="no" id="1_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                         
                                                              </form>
                                                          </li><br><br>
                                                          <li> ¿La Reacción Adversa apareció después de administrar el medicamento sospechoso?<br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="2" value="si" id="2_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="2" value="no" id="2_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                      <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="2" value="no" id="2_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿La Reacción Adversa Mejoro al suspender el medicamento?<br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="3" value="si" id="3_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-5">
                                                                          <input type="radio" class="icheck-primary" name ="3" value="no" id="3_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="3" value="no" id="3_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿La Reacción Adversa reapareció al re administrar el medicamento?<br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="4" value="si" id="4_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="4" value="no" id="4_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="4" value="no" id="4_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿Existen causas alternativas para explicar la reacción adversa? <br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="5" value="si" id="5_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4 ">
                                                                          <input type="radio" class="icheck-primary" name ="5" value="no" id="5_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="5" value="no" id="5_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿Se presento la reacción adversa después de administrar un placebo? <br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="6" value="si" id="6_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4 ">
                                                                          <input type="radio" class="icheck-primary" name ="6" value="no" id="6_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="6" value="no" id="6_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿Después de realizar exámenes, se determino la presencia del fármaco en la sangre u otros líquidos biológicos? <br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="7" value="si" id="7_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4 ">
                                                                          <input type="radio" class="icheck-primary" name ="7" value="no" id="7_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="7" value="no" id="7_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿La reacción adversa fue más intensa al aumentar la dosis? <br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="8" value="si" id="8_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4 ">
                                                                          <input type="radio" class="icheck-primary" name ="5" value="no" id="8_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="8" value="no" id="8_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿El paciente ha tenido reacciones similares al medicamento sospechoso o a medicamentos similares? <br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="9" value="si" id="9_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4 ">
                                                                          <input type="radio" class="icheck-primary" name ="9" value="no" id="9_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="9" value="no" id="9_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                          <li>¿La Reacción Adversa mejoro al quitar el medicamento o al suministrar otro? <br><br>
                                                              <form>
                                                                  <div class="row">
                                                                      <div class="col-xs-4 text-center">
                                                                          <input class="icheckbox-primary" type="radio" name="10" value="si" id="10_si" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SI  
                                                                      </div>
                                                                      <div class="col-xs-4 ">
                                                                          <input type="radio" class="icheck-primary" name ="10" value="no" id="10_no" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">NO         
                                                                      </div>
                                                                       <div class="col-xs-4">
                                                                          <input type="radio" class="icheck-primary" name ="10" value="no" id="10_desconocido" data-plugin="iCheck" data-radio-class="iradio_flat-blue form-control">SE DESCONOCE        
                                                                      </div>
                                                                  </div>                                                                             
                                                              </form>
                                                          </li><br><br>
                                                      </ol>
                                              </div>          
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-primary btn-block">Guardar</button>
                                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cerrar</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <!--fin del modal final--> 
                    </div>
                </div>
              </div><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@section('javascript')
@stop