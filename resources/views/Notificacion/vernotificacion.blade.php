@extends('layouts.layout')

@section('title')
  Notificacion | Ver Notificacion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL PARA VER NOTIFICACIONES ENVIADAS</h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">                 
            <!--INSUMOS MEDICOS-->               
            <table class="table table-bordered table-hover table-striped width-full" data-plugin="dataTable" data-selectable="selectable" data-row-selectable="true">
                <thead>
                  <tr>
                        <th class="text-center">
                            <span class="checkbox-custom checkbox-primary">
                                <input class="selectable-all" type="checkbox">
                                <label></label>
                            </span>
                        </th>
                        <th>Número de Notificación</th>
                        <th>Reacción Adversa</th>
                        <th>Insumo Médico</th>
                        <th>Médico Notificador</th>
                        <th>Estado</th>
                        <th class="text-center">Edición</th>
                  </tr>
                </thead>
                <tbody>
                      <tr>
                            <td class="text-center">
                                <span class="checkbox-custom checkbox-primary">
                                    <input class="selectable-item" type="checkbox" id="fila_1" value="1">
                                    <label for="fila_1"></label>
                                </span>
                            </td>
                            <td>Trident</td>
                            <td>Internet Explorer 5.5</td>
                            <td>Win 95+</td>
                            <td>Win 95+</td>
                            <td>
                                <span class="label label-primary" style="font-size: 100%">Dudoso</span>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn-pure"  data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title="Foto" title="">
                                        <a class="blue-grey-500 icon fa-camera" href="#foto" data-toggle="modal">
                                        </a>
                                    </button>
                                    <button type="button" class="btn-pure" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title="Vista" title="">
                                        <a class="blue-grey-500 icon fa-eye" href="#verNoti" data-toggle="modal">
                                        </a>
                                    </button>
                                    <button type="button" class="btn-pure" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title="Descargar" title="">
                                        <a class="blue-grey-500 icon fa-download" href="#descarga" data-toggle="modal">
                                        </a>
                                    </button>
                                    <button type="button" class="btn-pure" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title="Evaluar" title="">
                                        <a class="blue-grey-500 icon fa-check-square-o" href="#notiEvaluacion" data-toggle="modal">
                                        </a>
                                    </button>
                                </div>                 
                            </td>
                      </tr>
                </tbody>
          </table><!-- TABLA DE BASE DE DATOS PARAVER NOTIFICACION ENVIADA --> 
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  


<!--MODALES-->
<!-- MODAL PARA AGREGAR FOTOS A LA NOTIFICACION -->
<div class="modal fade modal-success" id="foto" aria-hidden="true" aria-labelledby="foto" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">X</span>
                </button>
                <h4 class="modal-title">Nueva foto</h4>
            </div>
            <div class="modal-body">
                <p>Solo Archivos Jpeg y no mayores de 1 MegaByte</p>
               <input type="file" id="foto-archivo" data-max-file-size="1M" accept="image/jpeg" data-plugin="dropify" data-default-file="" data-height="300">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>        
</div>
<!--FIN MODAL PARA AGREGAR FOTOS A LA NOTIFICACION-->


<!--MODAL PARA DESCARGAR-->
<div class="modal fade modal-success" id="descarga" aria-hidden="true" aria-labelledby="descarga" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">X</span>
                </button>
                <h4 class="modal-title">Descarga tu archivo</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-4">
                        <a id="tostar_word" data-plugin="toastr" data-message="El archivo se guardado con éxito" data-title="¡GUARDADO!" data-container-id="toast-top-right" data-close-button="true" data-tap-to-dismiss="false" data-icon-class="toast-just-text toast-info" href="javascript:void(0)" role="button">
                            <img data-dismiss="modal"  src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Microsoft_Word_2013_logo.svg/110px-Microsoft_Word_2013_logo.svg.png">
                        </a>
                        
                    </div>
                    <div class="col-xs-4">
                        <a id="tostar_excel" data-plugin="toastr" data-message="El archivo se guardado con éxito" data-title="¡GUARDADO!" data-container-id="toast-top-right" data-close-button="true" data-tap-to-dismiss="false" data-icon-class="toast-just-text toast-info" href="javascript:void(0)" role="button">
                            <img data-dismiss="modal" src="https://upload.wikimedia.org/wikipedia/commons/8/86/Microsoft_Excel_2013_logo.svg">
                        </a>                            
                    </div>
                    <div class="col-xs-4">
                        <a id="tostar_pdf" data-plugin="toastr" data-message="El archivo se guardado con éxito" data-title="¡GUARDADO!" data-container-id="toast-top-right" data-close-button="true" data-tap-to-dismiss="false" data-icon-class="toast-just-text toast-info" href="javascript:void(0)" role="button">
                            <img data-dismiss="modal" width="100" src="https://upload.wikimedia.org/wikipedia/commons/7/7d/Adobe_PDF.svg">
                        </a>                            
                    </div>
                </div>           
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>        
</div>
<!--FIN MODAL PARA DESCARGAR-->


<!--MODAL VER NOTIFICACION-->
<div class="modal fade" aria-hidden="true" aria-labelledby="verNoti" role="dialog" tabindex="-1" id="verNoti">
    <div class="modal-dialog modal-sidebar">
        <div class="modal-content exceso-hidden">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">X</span>
                </button>
                <div class="row">
                    <div class="col-xs-2">
                        <button type="button" class="btn-pure icon fa-arrow-left flechas" data-toggle="modal" data-dismiss="modal" href="#notiComentario">
                        </button>                            
                    </div>
                    <div class="col-xs-10">
                        <h1 class="modal-title blue-600">Detalle de Notificación</h1>
                    </div>
                </div>                    
            </div>
            <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist">
                <li class="active" role="presentation">
                    <a data-toggle="tab" href="#verNoti_1" aria-controls="verNoti_1" role="tab">Todo
                    </a>
                </li>
                <li role="presentation">
                    <a data-toggle="tab" href="#verNoti_2" aria-controls="verNoti_2" role="tab">Paciente
                    </a>
                </li>
                <li role="presentation">
                    <a data-toggle="tab" href="#verNoti_3" aria-controls="verNoti_3" role="tab">Reacción
                    </a>
                </li>
                <li role="presentation">
                    <a data-toggle="tab" href="#verNoti_4" aria-controls="verNoti_4" role="tab">Medicamento
                    </a>
                </li>
                <li role="presentation">
                    <a data-toggle="tab" href="#verNoti_5" aria-controls="verNoti_5" role="tab">Médico
                    </a>
                </li>
            </ul>
        </div>
        <div class="modal-body">
            <div class="exceso">
               <div class="tab-content">
                <div class="tab-pane active" id="verNoti_1">
                    <div class="row">
                        <div class="col-sm-4">
                            <a class="bg-blue-500 text-center block" href="#">
                                <i class="fa fa-user gran-blanco"></i>
                                <h5 class="blanco">Datos Personales</h5>                      
                            </a>
                        </div>
                        <div class="col-sm-4">
                             <a class="bg-blue-500 text-center block" href="#">
                                <i class="fa fa-map-o gran-blanco"></i>
                                <h5 class="blanco">Ubicación</h5>                      
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a class="bg-blue-500 text-center block" href="#">
                                <i class="pe-plus gran-blanco"></i>
                                <h5 class="blanco">Información Adicional</h5>                      
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <a class="bg-blue-500 text-center block" href="#" >
                                <i class="fa-frown-o gran-blanco"></i>
                                <h5 class="blanco">Datos Reacción Adversa</h5>                      
                            </a>
                        </div>
                        <div class="col-sm-4">
                             <a class="bg-blue-500 text-center block" href="#">
                                <img class="gran-blanco" src="../libreria/pildora.svg">
                                <h5 class="blanco">Datos Insumo Médico</h5>                     
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a class="bg-blue-500 text-center block" href="#">
                                <img class="gran-blanco-dos-lineas" src="../libreria/jarra.png">
                                <h5 class="blanco">Datos Consumo del Insumo Médico</h5>            
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <a class="bg-blue-500 text-center block" href="#" >
                                <img class="gran-blanco" src="../libreria/pildoras.png">
                                <h5 class="blanco">Datos del Insumo Médico Concumitante</h5>         
                            </a>
                        </div>
                        <div class="col-sm-4">
                             <a class="bg-blue-500 text-center block" href="#datosInConcumitante" data-toggle="modal">
                                <img class="gran-blanco-dos-lineas" src="../libreria/jarra_pildora.png">
                                <h5 class="blanco">Datos Consumo del Insumo Médico Concumitante</h5> 
                            </a>
                        </div><br>                           
                    </div>
                </div>
                <div class="tab-pane" id="verNoti_2">
                    <div class="row">
                        <div class="col-sm-6">
                            <a class="bg-blue-500 text-center block" href="#">
                                <i class="fa fa-user gran-blanco"></i>
                                <h5 class="blanco">Datos Personales</h5>                      
                            </a>
                        </div>
                        <div class="col-sm-6">
                             <a class="bg-blue-500 text-center block" href="#">
                                <i class="fa fa-map-o gran-blanco"></i>
                                <h5 class="blanco">Ubicación</h5>                      
                            </a>
                        </div>
                        
                    </div>
                </div>
                <div class="tab-pane" id="verNoti_3">
                    <div class="row">
                        <div class="col-sm-6">
                            <a class="bg-blue-500 text-center block" href="#" >
                                <i class="fa-frown-o gran-blanco"></i>
                                <h5 class="blanco">Datos Reacción Adversa</h5>                      
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="bg-blue-500 text-center block" href="#">
                                <i class="pe-plus gran-blanco"></i>
                                <h5 class="blanco">Información Adicional</h5>                      
                            </a>
                        </div>
                    </div>
                </div>
                 <div class="tab-pane" id="verNoti_4">
                   <div class="row">
                        <div class="col-sm-6">
                             <a class="bg-blue-500 text-center block" href="#">
                                <img class="gran-blanco" src="../libreria/pildora.svg">
                                <h5 class="blanco">Datos Insumo Médico</h5>                     
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="bg-blue-500 text-center block" href="#">
                                <img class="gran-blanco-dos-lineas" src="../libreria/jarra.png">
                                <h5 class="blanco">Datos Consumo del Insumo Médico</h5>            
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <a class="bg-blue-500 text-center block" href="#" >
                                <img class="gran-blanco" src="../libreria/pildoras.png">
                                <h5 class="blanco">Datos del Insumo Médico Concumitante</h5>         
                            </a>
                        </div>
                        <div class="col-sm-6">
                             <a class="bg-blue-500 text-center block" href="#">
                                <img class="gran-blanco-dos-lineas" src="../libreria/jarra_pildora.png">
                                <h5 class="blanco">Datos Consumo del Insumo Médico Concumitante</h5> 
                            </a>
                        </div>                           
                    </div>
                </div>
                 <div class="tab-pane" id="verNoti_5">
                    <h3 class="blue-600">Datos Del Médico Notificador</h3>
                     <div class="row">
                        <div class="col-sm-12 col-sm-offset-2">
                            <table class="espacio">
                                 <tr>
                                    <td>Nombre y Apellido del Médico</td>
                                    <td>Omar Antonio Boza Bonilla</td>
                                 </tr>
                                 <tr>
                                    <td>Profesión</td>
                                    <td>Médico Géneral</td>
                                 </tr>
                                 <tr>
                                    <td>Especialidad</td>
                                    <td>Cirujano Plástico</td>
                                 </tr>
                                 <tr>
                                    <td>Celular</td>
                                    <td>ACM1PT69</td>
                                 </tr>
                                 <tr>
                                    <td>Correo</td>
                                    <td>concha@loco.com</td>
                                 </tr>
                             </table>
                         </div>
                     </div>                         
                </div>
            </div> 
            </div>  
        </div>
    </div>
</div>
<!--FIN DE MODAL VER NOTIFICACION-->


<!--MODAL EVALUACION DE NOTIFICACION-->
<div class="modal fade" aria-hidden="true" aria-labelledby="notiEvaluacion" role="dialog" tabindex="-1" id="notiEvaluacion">
    <div class="modal-dialog modal-sidebar">
        <div class="modal-content exceso-hidden">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">X</span>
                </button>
                <h2 class="modal-title blue-600">Evaluación de Notificación</h2>
            </div>
            <div class="modal-body">
                <h4 class="text-center">Puede filtrar la evaluación eligiendo los medicamentos y las reacciones adversas guardadas en esta notificación, así podrá saber los posibles cruces y la posibilidad de causa de la reacción sobre el medicamento</h4>
            </div>
            <div class="container-fluid"> 
                <div style="overflow-y: scroll; height: 500px; overflow-x: hidden ">           
                <div class="row">
                     <div class="col-sm-offset-0">
                        <h4 class="text-center">Resultados Algoritmo PRR</h4>
                         <div class="col-sm-6 "><!-- Panel Wizard Form Container widgets -->
                              <div class="panel" id="exampleWizardForm">              
                                  <div class="widget-content">
                                        <div class="bg-blue-600 white padding-30">
                                            <div class="font-size-20 clearfix">
                                                Power
                                                <span class="pull-right">
                                                    <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                                </span>
                                            </div>
                                            <div class="font-size-14 red-200 clearfix">
                                                2015.1.20
                                                <span class="pull-right">+12.5(2.8%)</span>
                                            </div>            
                                        </div>
                                  </div>          
                              </div><!--final del panel-->
                          </div>    
                          <div class="col-sm-6 "><!-- Panel Wizard Form Container widgets -->
                              <div class="panel" id="exampleWizardForm">              
                                  <div class="widget-content">
                                      <div class="bg-orange-600 white padding-30">
                                          <div class="font-size-20 clearfix">
                                              Power
                                              <span class="pull-right">
                                                  <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                              </span>
                                          </div>
                                          <div class="font-size-14 red-200 clearfix">
                                              2015.1.20
                                              <span class="pull-right">+12.5(2.8%)</span>
                                          </div>            
                                      </div>
                                </div>          
                              </div><!--final del panel-->
                           </div>
                           <div class="col-sm-6 "><!-- Panel Wizard Form Container widgets -->
                              <div class="panel" id="exampleWizardForm">              
                                  <div class="widget-content">
                                        <div class="bg-red-600 white padding-30">
                                            <div class="font-size-20 clearfix">
                                                Power
                                                <span class="pull-right">
                                                    <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                                </span>
                                            </div>
                                            <div class="font-size-14 red-200 clearfix">
                                                2015.1.20
                                                <span class="pull-right">+12.5(2.8%)</span>
                                            </div>            
                                        </div>
                                  </div>          
                              </div><!--final del panel-->
                          </div>   
                          <div class="col-sm-6 "><!-- Panel Wizard Form Container widgets -->
                              <div class="panel" id="exampleWizardForm">              
                                  <div class="widget-content">
                                        <div class="bg-green-600 white padding-30">
                                            <div class="font-size-20 clearfix">
                                                Power
                                                <span class="pull-right">
                                                    <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                                </span>
                                            </div>
                                            <div class="font-size-14 red-200 clearfix">
                                                2015.1.20
                                                <span class="pull-right">+12.5(2.8%)</span>
                                            </div>            
                                        </div>
                                  </div>          
                              </div><!--final del panel-->
                          </div> 
                          <h4 class="text-center">Resultados Algoritmo Lassagna</h4>
                           <div class="col-sm-12 "><!-- Panel Wizard Form Container widgets -->
                              <div class="panel" id="exampleWizardForm">              
                                  <div class="widget-content">
                                        <div class="bg-blue-600 white padding-30">
                                            <div class="font-size-20 clearfix">
                                                Algoritmo Lassagna
                                                <span class="pull-right">
                                                    <i class="icon wb-triangle-up" aria-hidden="true"></i> 260
                                                </span>
                                            </div>
                                            <div class="font-size-14 red-200 clearfix">
                                                2015.1.20
                                                <span class="pull-right">+12.5(2.8%)</span>
                                            </div>            
                                        </div>
                                  </div>          
                              </div><!--final del panel-->
                          </div>     
                        </div>
                    </div>  
                    <h5>Estado de la Notificación</h5>
                    <div class="form-group">
                        <select data-plugin="selectpicker" title="Notificadas" name="requiredSelect" id="reNotificado" data-style="btn-primary" class="form-control" required="required">
                            <option value="">Dudosa</option>
                            <option value="">Posible</option>
                            <option value="">Probable</option>
                            <option value="">Probada</option>
                        </select>
                    </div>
                    <h5>Motivo por el cual afirma el estado anterior de la notificación</h5>
                    <textarea class="maxLength-textarea form-control" data-plugin="maxLength" maxlength="100" rows="3" style="resize: none"></textarea>

                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-outline btn-primary">
                            <a id="tostar_notificacion" data-plugin="toastr" data-message="Guardado con éxito" data-title="¡GUARDADO!" data-container-id="toast-top-left" data-close-button="true" data-tap-to-dismiss="false" data-icon-class="toast-just-text toast-info" data-position-class="toast-top-left" href="javascript:void(0)" role="button">Enviar</a>
                        </button>
                    </div>     
                    </div>                
            </div>                             
        </div>
    </div>
</div>
<!--FIN MODAL EVALUACION DE NOTIFICACION-->
<!--MODAL RESULTADO-->
<div class="modal fade modal-fade-in-scale-up modal-success" id="notiResultado" aria-hidden="true" aria-labelledby="notiResultado" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content exceso-hidden">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">X</span>
                </button>
                <h4 class="modal-title">Resultado de la Evaluación</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <h3 class="blue-600 text-center">Resultado</h3>
                        <div class="col-sm-5 col-sm-offset-1">
                            <a class="bg-blue-600 text-center block">
                                <h5 class="texto-blanco">Total de<br>Notificaciones<br><br><strong class="texto-blanco-strong">500</strong></h5>         
                            </a>
                        </div>
                       <div class="col-sm-5">
                            <a class="bg-orange-600 text-center block">
                                <h5 class="texto-blanco">Total Notificaciones por Insumo Médico<br><br><strong class="texto-blanco-strong">500</strong></h5>         
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 col-sm-offset-1">
                            <a class="bg-orange-600 text-center block">
                                <h5 class="texto-blanco">Total Notificaciones por Reacción Adversa<br><br><strong class="texto-blanco-strong">40</strong></h5> 
                            </a>
                        </div>
                        <div class="col-sm-5">
                            <a class="bg-orange-600 text-center block">
                                <h5 class="texto-blanco">Total Concidencias, Notificaciones, Insumo y Reacción<br><strong class="texto-blanco-strong">500</strong></h5>  
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <a class="bg-orange-600 text-center block">
                                <h5 class="texto-blanco">Algoritmo Lassagna<br>
                                <strong class="texto-blanco-strong">500</strong><br>
                                <div id="lassagna">
                                    <strong id="puntaje">Puntaje = 4</strong>
                                    <strong id="posibilidad">POSIBLE</strong>
                                </div>                                    
                                </h5>
                            </a>
                        </div>
                    </div> 
                </div>                                    
            </div>
        </div>
    </div>
</div>
<!--FIN MODAL RESULTADO-->
<!--MODAL DATOS INSUMO CONCUMITANTE-->
<div class="modal fade modal-primary modal-super-scaled" id="datosInConcumitante" aria-hidden="true" aria-labelledby="datosInConcumitante" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content exceso-hidden">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">X</span>
                </button>
                <h4 class="modal-title">Datos Consumo del Insumo Médico Concumitante</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-sm-12">
                   <div class="form-group">
                      <label class="control-label" for="inputmedicamento2">Nombre  Genérico Insumo Médico</label>
                      <input type="text" class="form-control" id="inputmedicamento2" name="medicamento" placeholder="Nombre" readonly>
                  </div> 
                </div>                                                    
                <div class="col-sm-6">
                    <div class="form-group">
                    <label for="">Dósis Tomada</label>
                    <input type="text" class="form-control" value="0" readonly /> 
                    </div>                                           
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label" for="">Unidad de Medida</label>
                        <select class="form-control" title="Unidad" data-style="btn-outline btn-primary">  
                            <option value="">Chinandega</option>
                        </select>
                    </div>                                     
                 </div>
                 <div class="col-sm-12">
                    <div class="form-group">
                        <label class="control-label" for="">Frecuencia de Consumo</label><br>  
                        <select class="form-control" title="Frecuencia" data-style="btn-outline btn-primary">                            
                            <option value="">Cada 24 Horas (1 vez al dia)</option>
                        </select> 
                     </div>
                 </div>
                 <div class="col-sm-12">
                     <div class="form-group">
                        <label class="control-label" for="">Tipo de Insumo Médico</label><br>  
                        <select class="form-control" title="Frecuencia" data-style="btn-outline btn-primary">                            
                                <option value="">Insomo Médico Sospechoso</option>              
                        </select> 
                     </div>
                 </div>  
                 <div class="col-sm-12">
                     <div class="form-group">
                        <label class="control-label" for="">Vía de Administración</label><br>  
                        <select class="form-control" title="Medida" data-style="btn-outline btn-primary">
                            <option value="">Chinandega</option>
                        </select> 
                     </div>
                 </div>
                 <div class="col-sm-6">
                    <div class="form-group">
                     <label for="">Inicio Toma Medicamento</label>
                     <input type="text" class="form-control" value="0" readonly /> 
                    </div>                                           
                 </div>
                 <div class="col-sm-6">
                    <div class="form-group">
                     <label for="">Fin Toma Medicamento</label>
                     <input type="text" class="form-control" value="0" readonly /> 
                    </div>                                           
                 </div>
                 <div class="col-sm-12">
                   <textarea class="maxLength-textarea form-control" data-plugin="maxLength" maxlength="100" rows="6" style="resize: none">
                   </textarea>    
                 </div>
             </div>                             
            </div>
        </div>
    </div>
</div>
 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
      <script type="text/javascript">  
       
      
      </script>

    @endpush
@endsection
    
@stop