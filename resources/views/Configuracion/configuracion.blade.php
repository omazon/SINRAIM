@extends('layouts.layout')

@section('title')
  Notificacion | Configuracion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL DE CONFIGURACION </h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
            <br>
            @include('alerts.success') 
            @include('alerts.errors')  
            <div class="col-sm-12">
                 <label class="lead text-muted" style="display: block; color:#279566; font-weight: bold;">Importar Usuarios Masivamente  </label>
                 {!!Form::open(['route'=>['importarexcelusuario'], 'method'=>'POST', 'enctype'=>'multipart/form-data'])!!}            
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="file" class="filestyle" name="importarusuario" id="importarusuario" data-plugin="dropify" data-default-file=""></input>
                              {!!Form::submit('Guardar',['id'=>'btnguardarcsvusuario','class'=>'btn btn-primary pull-right'])!!} 
                              <div class="dropify-preview">
                                  <span class="dropify-render"></span>
                                  <div class="dropify-infos">
                                      <div class="dropify-infos-inner">
                                        <p class="dropify-filename">
                                            <span class="file-icon"></span> 
                                            <span class="dropify-filename-inner"></span>
                                        </p>
                                      </div>
                                  </div>
                              </div>
                  {!!Form::close()!!}   
            </div>


            <br>
            <div class="col-sm-12">
                 <label class="lead text-muted" style="display: block; color:#279566; font-weight: bold;">Importar Diccionario ATC Masivamente  </label>
                 {!!Form::open(['route'=>['importarexcelATC'], 'method'=>'POST', 'enctype'=>'multipart/form-data'])!!}            
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              <input type="file" class="filestyle" name="importarATC" id="importarATC" data-plugin="dropify" data-default-file=""></input>
                              {!!Form::submit('Guardar',['id'=>'btnguardarcsvatc','class'=>'btn btn-primary pull-right'])!!} 
                              <div class="dropify-preview">
                                  <span class="dropify-render"></span>
                                  <div class="dropify-infos">
                                      <div class="dropify-infos-inner">
                                        <p class="dropify-filename">
                                            <span class="file-icon"></span> 
                                            <span class="dropify-filename-inner"></span>
                                        </p>
                                      </div>
                                  </div>
                              </div>
                  {!!Form::close()!!}   
            </div>
                      
            
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   

 @section('javascript')
  @section('scripts') 
          <!-- Plugins para subir archivos -->
  {!!Html::script('../../global/vendor/jquery-ui/jquery-ui.min.js')!!}
  {!!Html::script('../../global/vendor/blueimp-tmpl/tmpl.min.js')!!}
  {!!Html::script('../../global/vendor/blueimp-canvas-to-blob/canvas-to-blob.min.js')!!}
  {!!Html::script('../../global/vendor/blueimp-load-image/load-image.all.min.js')!!}
  {!!Html::script('../../global/vendor/blueimp-file-upload/jquery.fileupload.js')!!}
  {!!Html::script('../../global/vendor/blueimp-file-upload/jquery.fileupload-process.js')!!}
  {!!Html::script('../../global/vendor/blueimp-file-upload/jquery.fileupload-image.js')!!}
  {!!Html::script('../../global/vendor/blueimp-file-upload/jquery.fileupload-audio.js')!!}
  {!!Html::script('../../global/vendor/blueimp-file-upload/jquery.fileupload-video.js')!!}
  {!!Html::script('../../global/vendor/blueimp-file-upload/jquery.fileupload-validate.js')!!}
  {!!Html::script('../../global/vendor/blueimp-file-upload/jquery.fileupload-ui.js')!!}
  {!!Html::script('../../global/vendor/dropify/dropify.min.js')!!}

  {!!Html::script('../../global/js/components/dropify.min.js')!!}
  {!!Html::script('../assets/examples/js/forms/uploads.min.js')!!}  
  @endsection

  @section('footer')
    @push('scripts')
        {!!Html::script('js/ayuda/configuracion-tour.js')!!} 
      
    @endpush
@endsection
    
@stop

