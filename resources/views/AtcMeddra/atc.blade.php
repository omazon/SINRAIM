@extends('layouts.layout')

@section('title')
  Notificacion | Diccionario ATC
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-9">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL DICCIONARIO ATC</h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
             {!!link_to('/atcmeddra', $title='Regresar a Menu Diccionarios', $attributes = ['class'=>'btn btn-success pull-left'], $secure = null)!!} <br><br>           
            <div class="row"><br><br>
                <div class="col-md-12">
                       <span class="text-center blue-800 font-size-18"><strong>Para realizar la busqueda</strong> necesitas agregar el insumo medico en la caja de texto y luego Usando el botón <strong>Buscar</strong> para obtener la informacion del ATC. Para mas informacion presiona el boton de ayuda en la esquina inferior derecha</span>
                </div>                
                <div class="col-md-12"><br><br>                   
                   {!!Form::open(['route'=>['buscaratc'], 'method'=>'POST'])!!}
                      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                       <div class="form-group">
                            <label class="control-label" for="inputmedicamento2">Nombre Insumo Médico</label>
                            {!!Form::text('nombremedicamento',null,['name'=>'nombremedicamento[]', 'id'=>'nombremedicamento', 'class'=>'form-control nombremedicamento', 'autocomplete'=>'off', 'data-fv-notempty'=>'true', 'data-fv-notempty-message'=>'Este campo no puede estar vacio'])!!}
                            {!!Form::hidden('codmedicamento',null,['id'=>'codmedicamento', 'class'=>'form-control'])!!}
                        </div>
                          <div class="col-sm-12 text-right">
                              {!!Form::submit('BUSCAR',['id'=>'btnbuscaratc', 'class'=>'btn btn-primary'])!!} 
                          </div> 
                    {!!Form::close()!!} 
                </div> 
                <div class="col-md-12">
                      <br><br>
                      <table class="table table-hover dataTable table-striped width-full text-center" id="leeratc">
                          <thead>
                              <tr>
                                  <th>CodATC</th>
                                  <th>Descripcion</th>
                                  <th>Via Administración</th>
                                  <th>Principio Activo</th>
                                  <th>Unidad Medida</th>
                                  <th>Unidad Referencia</th>
                                  <th>Forma Farmaceutica</th>
                              </tr>
                          </thead>             
                          <tbody id="datos">
                                 @foreach($buscaratcs as $buscaratc)
                                    <tr> 
                                        <td>{{$buscaratc->codatc}}</td>
                                        <td>{{$buscaratc->descripcion}}</td>
                                        <td>{{$buscaratc->viadministracion}}</td>
                                        <td>{{$buscaratc->principioactivo}}</td>
                                        <td>{{$buscaratc->unidadmedida}}</td>
                                        <td>{{$buscaratc->unidadreferencia}}</td>
                                        <td>{{$buscaratc->formafarmaceutica}}</td>                                                      
                                      </tr> 
                                  @endforeach     
                            </tbody>
                        </table>
                  </div>  
              </div>         
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->

 <!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   
  


 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
         <script type="text/javascript">
////////////////////////////////////////////////////////////////////////////////////////////////////////
          //Autocompletado del Insumo Medico
          var path = "{{URL::route('autocompletemedicamento')}}";
          //Enviado del contenido del textbox al controlador para obtener el autocompletado de la BD
          $('.nombremedicamento').typeahead({
              source:  function (query, process) {
               $.get(path, { query: query }, function (data) {
                        return process(data);
                  }), 'json';
              },
              minLength:2,
              hint: true,
              highlight: true, 
               //Guardar en el mismo texbox el valor seleccionado
              updater: function(item) { 
                    $("#codmedicamento").val(item.id);
                    return item;
                }
             
          });
////////////////////////////////////////////////////////////////////////////////////////////////////////  

         </script>

         <!-- Scripts para panel atc --> 
        {!!Html::script('js/ayuda/atc-tour.js')!!} 
      
    @endpush
@endsection
    
@stop

