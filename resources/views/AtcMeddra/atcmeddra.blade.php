@extends('layouts.layout')

@section('title')
  Notificacion | ATC-Meddra
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL PARA VER DETALLES DE ATC Y MEDDRA</h1>
        </header><br> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-2">
                    <div class="widget widget-shadow" id="chartLinePie">
                        <div class="widget-content">
                            <div class="bg-blue-600 white padding-30">
                                <div class="font-size-20 clearfix text-center">
                                    DICCIONARIO ATC                             
                                </div><br>
                                <div class="font-size-80 clearfix text-center">
                                     <i class="icon fa fa-medkit"></i>
                                </div>
                        </div>
                        <div class="padding-30">
                            <div class="row no-space">
                               <div class="font-size-80 clearfix text-center">
                                   <a  href="{{ URL::route('atc') }}" class="btn btn-success" role="button" id="btnveratc">VER ATC</a>
                                   
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="widget widget-shadow" id="chartLinePie">
                        <div class="widget-content">
                            <div class="bg-blue-600 white padding-30">
                                <div class="font-size-20 clearfix text-center">
                                    DICCIONARIO MEDDRA                             
                                </div><br>
                                <div class="font-size-80 clearfix text-center">
                                     <i class="icon fa fa-heartbeat"></i>
                                </div>
                        </div>
                        <div class="padding-30">
                            <div class="row no-space">
                               <div class="font-size-80 clearfix text-center">
                                   <a  href="{{ URL::route('meddra') }}" class="btn btn-success" role="button" id="btnvermeddra">VER MEDDRA</a>
                                   
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div><!-- FIN ROW -->                
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   


 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
         <!-- Scripts para panel atcmeddra --> 
        {!!Html::script('js/ayuda/panelatcmeddra-tour.js')!!} 
    @endpush
@endsection
    
@stop

