@extends('layouts.layout')

@section('title')
  Notificacion | Diccionario Meddra
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-9">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL DICCIONARIO MEDDRA</h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
             {!!link_to('/atcmeddra', $title='Regresar a Menu Diccionarios', $attributes = ['class'=>'btn btn-success pull-left'], $secure = null)!!} <br><br> 
            <div class="row"><br><br>
                <div class="col-md-12">
                       <span class="text-center blue-800 font-size-18"><strong>Para realizar la busqueda</strong> necesitas agregar la reaccion adversa en la caja de texto y luego Usando el botón <strong>Buscar</strong> para obtener la informacion del Meddra. Para mas informacion presiona el boton de ayuda en la esquina inferior derecha</span>
                </div>                
                <div class="col-md-12"><br><br>  
                   {!!Form::open(['route'=>['buscarmeddra'], 'method'=>'POST'])!!}
                      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                          <div class="form-group">
                              <label class="control-label" for="inputreaccion">Nombre Reacción Adversa</label>
                              {!!Form::text('nombrereaccion',null,['name'=>'nombrereaccion[]', 'id'=>'nombrereaccion', 'class'=>'form-control nombrereaccion', 'autocomplete'=>'off', 'data-fv-notempty'=>'true', 'data-fv-notempty-message'=>'Este campo no puede estar vacio'])!!}
                              {!!Form::hidden('codreaccionadversa',null,['id'=>'codreaccionadversa', 'class'=>'form-control'])!!}
                          </div>
                          <div class="col-sm-12 text-right">
                              {!!Form::submit('BUSCAR',['id'=>'btnbuscarmeddra', 'class'=>'btn btn-primary'])!!} 
                          </div> 
                    {!!Form::close()!!} 
                </div> 
                <div class="col-md-12">
                      <br><br>
                      <table class="table table-hover dataTable table-striped width-full text-center" id="leermeddra">
                          <thead>
                              <tr>
                                  <th>Cod Descripcion</th>
                                  <th>Descripcion</th>
                                  <th>Cod Categoria</th>
                                  <th>Categoria</th>
                              </tr>
                          </thead>             
                          <tbody id="datos">
                                 @foreach($buscarmeddras as $buscarmeddra)
                                    <tr> 
                                        <td>{{$buscarmeddra->iddescripcion}}</td>
                                        <td>{{$buscarmeddra->descripcion}}</td>
                                        <td>{{$buscarmeddra->idcategoria}}</td>
                                        <td>{{$buscarmeddra->categoria}}</td>                                                      
                                      </tr> 
                                  @endforeach     
                            </tbody>
                        </table>
                  </div>  
              </div>         
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  
 <!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   



 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
         <script type="text/javascript">
////////////////////////////////////////////////////////////////////////////////////////////////////////
          //Autocompletado de las Reacciones Adversas
          var path1 = "{{URL::route('autocomplete')}}";
          //Enviado del contenido del textbox al controlador para obtener el autocompletado de la BD
          $('.nombrereaccion').typeahead({
              source:  function (query, process) {
               $.get(path1, { query: query }, function (data) {
                        return process(data);
                  }), 'json';
              },
              minLength:2,
              hint: true,
              highlight: true, 
              //Guardar en el mismo texbox el valor seleccionado 
              updater: function(item) { 
                    $("#codreaccionadversa").val(item.id);
                    return item;
                }
             
          });  
////////////////////////////////////////////////////////////////////////////////////////////////////////  

         </script>

         <!-- Scripts para panel meddra --> 
        {!!Html::script('js/ayuda/meddra-tour.js')!!} 
      
    @endpush
@endsection
    
@stop

