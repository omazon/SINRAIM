  <!-- modal edicion de notificador -->
  <div class="modal fade in" id="pacientesidebar" aria-hidden="true" aria-labelledby="pacientesidebar" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sidebar modal-lg">
        <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
        </div>
        <div class="modal-content">
                   
            <div class="modal-body">

                  <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                  <input type="hidden" id="id">
                  @include('Patient.form.paciente')

            </div>
            <div class='modal-footer'>
                {!!link_to('#', $title = 'Actualizar', $attributes = ['id'=>'actualizar', 'class'=>'btn btn-primary'], $secure = null)!!}   
              </div>
        </div>
      </div>
  </div>


