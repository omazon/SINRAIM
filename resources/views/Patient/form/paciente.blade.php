<h4 align="center">Agregar Datos Personales del Paciente</h4></br>
<div class="form-group">
 	{!!Form::hidden('idnotificador',$idnotificador,['id'=>'idnotificador', 'class'=>'form-control'])!!}
</div>
<div class="form-group">
	{!!Form::label('name','Nombre (Name)')!!}
	{!!Form::text('name',null,['id'=>'name','class'=>'form-control', 'placeholder'=>'Ingrese el Nombre del paciente'])!!}
</div>  
<div class="form-group">
	{!!Form::label('last_name','Apellido (Last_name)')!!}
	{!!Form::text('last_name',null,['id'=>'last_name','class'=>'form-control', 'placeholder'=>'Ingrese el Apellido del paciente'])!!}
</div> 
<div class="form-group">
	{!!Form::label('fechanacimiento','fechanacimiento')!!}
	{!!Form::date('fechanacimiento','', ['class' => 'form-control','placeholder'=>'DD/MM/YYYY', 'required'])!!}                       
</div> 
<div class="form-group">
	{!!Form::label('genero','Genero')!!}
	{!!Form::select('genero', ['M' => 'Masculino', 'F' => 'Femenino'], null, ['class' => 'form-control'], ['placeholder' => 'Ingresa el genero'])!!}
</div>


         