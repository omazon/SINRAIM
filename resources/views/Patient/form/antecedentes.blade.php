<h4 align="center">Editar Antecedentes Familiares del Paciente</h4></br>
<div class="form-group">
{!!Form::label('id','id')!!}
{!!Form::text('id',null,['id'=>'id', 'class'=>'form-control'])!!}
</div>  
<div class="form-group">
{!!Form::label('familiar','Familiar')!!}
{!!Form::select('familiar', ['Abuelo' => 'Abuelo', 'Abuela' => 'Abuela', 'Papa' => 'Papa', 'Mama' => 'Mama', 'Hermano' => 'Hermano', 'Hermana' => 'Hermana', 'Tio' => 'Tio', 'Tia' => 'Tia'], null, ['class' => 'form-control'], ['placeholder' => 'Ingresa el Familiar con el padecimiento'])!!}
</div>  
            
<div class="form-group">
{!!Form::label('edad','Edad del Familiar')!!}
{!!Form::number('edad', 'value',['class' => 'form-control'])!!}
</div>

<div class="form-group">
{!!Form::label('vida','Vida del Familiar')!!}
{!!Form::select('vida', ['SI' => 'SI', 'NO' => 'NO'], null, ['class' => 'form-control'], ['placeholder' => 'El familiar esta con vida?'])!!}
</div>

<div class="form-group">
    {!!Form::label('searchmedicaldiccionaries','Buscar Enfermedad')!!}
    {!!Form::text('searchmedicaldiccionaries',null,['name'=>'searchmedicaldiccionaries', 'class'=>'form-control searchmedicaldiccionaries', 'autocomplete'=>'off'])!!}
</div> 

