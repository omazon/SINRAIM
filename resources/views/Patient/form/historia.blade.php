 <div class="popover-primary" data-title="Agregar Antecedentes familiares del Paciente" data-content="Presionar + Agregar nuevo para  agregar tantos familiares como sean necesario." data-trigger="hover" data-placement="top" data-toggle="popover">                      
  <button  href="#" name="addRow" type="button" id="addRow" class="btn btn-primary" value="Add Row" onclick="addRow('leerhistoria')"><i class="icon wb-plus"></i>Agregar nuevo</button>
  <button  href="#" type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('leerhistoria')"><i class="icon wb-minus"></i>Eliminar</button><br><br>
</div> 


<table class="table table-hover dataTable table-striped width-full text-center" id="tablantecedentes">

    <thead>
        <th>Seleccionar</th>
        <th></th>
        <th>Familiar</th>
        <th>Edad</th>
        <th>Esta con Vida?</th>
        <th>Enfermedad</th>
        <th></th>
    </thead>
           
      <tbody id="leerhistoria">
            <tr> 
                <td><input type="checkbox" name="chk"></td>
                <td>
                     <input type="hidden" id="patient_id"  value={{$idpaciente}}>
                </td>
                <td>
                    {!!Form::select('familiar', ['Abuelo' => 'Abuelo', 'Abuela' => 'Abuela', 'Papa' => 'Papa', 'Mama' => 'Mama', 'Hermano' => 'Hermano', 'Hermana' => 'Hermana', 'Tio' => 'Tio', 'Tia' => 'Tia'], null, ['name' => 'familiar[]', 'id' => 'familiar',  'class' => 'form-control'], ['placeholder' => 'Ingresa el Familiar con el padecimiento'])!!}              
                </td>
                <td>
                    {!!Form::number('edad', 'value',['name' => 'edad[]', 'id' => 'edad', 'class' => 'form-control'])!!}
                </td>
                 <td>
                    {!!Form::select('vida', ['SI' => 'SI', 'NO' => 'NO'], null, ['name' => 'vida[]', 'id'=>'vida', 'class' => 'form-control'], ['placeholder' => 'El familiar esta con vida?'])!!}
                </td>
                <td>
                       
                    {!!Form::text('searchmedicaldiccionaries',null,['name'=>'searchmedicaldiccionaries[]', 'id'=>'searchmedicaldiccionaries', 'fila'=>'row-0-0', 'class'=>'form-control searchmedicaldiccionaries', 'autocomplete'=>'off'])!!}
                      
                </td>   
                <td>
                    {!!Form::hidden('descripcion_id',null,['name'=>'descripcion_id[]', 'id'=>'descripcion_id', 'buscador'=>'row-0-0', 'fila'=>'row-0-0', 'class'=>'form-control descripcion_id'])!!}
                </td>                                                           
            </tr>          
        </tbody>   
</table> 




