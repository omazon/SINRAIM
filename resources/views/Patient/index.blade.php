@extends('layouts.layout')

@section('title')
  SINRAIM | Pacientes
@stop
@section('css')

@section('body') 

<!-- Page -->
<div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h1 class="panel-title">PANEL ENVIO DE NOTIFICACIONES POR PACIENTE</h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">                   
              <div class="popover-primary" data-title="Enviar Notificación y Pacientes" data-content="En esta seccion puede agregar nuevos pacientes y luego seleccionarlos para enviar nuevos casos de notificacion." data-trigger="hover" data-placement="top" data-toggle="popover">                             
                  <button id="add-patient" href="#" data-toggle="modal" data-target="#nuevo-paciente" type="button" class="btn btn-success pull-right"><i class="icon wb-plus"></i>Agregar nuevo paciente</button><br><br>
              </div>            
              @include('Patient.modal') 
              @include('alerts.success') 
              @include('alerts.errors')
              <div id="msj-success-act" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Felicidades!</strong> Paciente acualizado con exito!!!.
              </div>
              <div id="msj-success-elim" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  Paciente Eliminado con exito!!!.
              </div>
              <table class="table table-hover dataTable table-striped width-full text-center" id="leerpacientes">
                  <thead>
                      <tr>
                          <th>Id</th>
                          <th>Nombres</th>
                          <th>Apellidos</th>
                          <th>Fecha Nacimiento</th>
                          <th>Edad</th>
                          <th id="accion">Acción</th>
                      </tr>
                  </thead>             
                  <tbody id="datos">
                         @foreach($patients as $patient)
                            <tr> 
                                    <td>{{$patient->id}}</td>
                                    <td>{{$patient->name}}</td>
                                    <td>{{$patient->last_name}}</td>
                                    <td>{{date('d/m/Y', strtotime($patient->fechanacimiento))}}</td>
                                    <td>{{$patient->edad}}</td>
                                      <td>
                                        <button id="editarpaciente" class='btn btn-primary' onclick="Mostrar({{$patient->id}})" data-toggle='modal' data-target='#pacientesidebar'>Editar</button> 

                                        <!-- -->
                                        <a  href="{{ URL::route('paciente.show', $patient->id) }}" id="antecedentes" class="btn btn-primary" role="button">Antecedentes</a>
                                     
                                        <button id="eliminarpaciente" class='btn btn-danger' onclick="Eliminar({{$patient->id}})" >Eliminar</button>

                                        <a  href="{{ URL::route('crearnotificacion', $patient->id) }}" id="enviarnotificacion" class="btn btn-info" role="button">Enviar</a>

                                      </td>                                                      
                                </tr> 
                          @endforeach     
                    </tbody>
                </table>          
        </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

 <!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   

 <!-- MODAL PARA AGREGAR NUEVO PACIENTE -->
<div class="modal fade modal-info in" id="nuevo-paciente" role="dialog" aria-hidden="true" aria-labelledby="nuevo-paciente" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" aria-label="Cerrar" type="button" data-dismiss="modal">X</button>
          <h4 class="modal-title">Agregar Nuevo Paciente</h4>
        </div>
        <div class="modal-body">
           {!!Form::open(['route'=>'paciente.store', 'method'=>'POST', 'novalidate'])!!}
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                @include('Patient.form.paciente')
                {!!Form::submit('Guardar',['id'=>'guardarpaciente','class'=>'btn btn-primary'])!!}  
                 
          {!!Form::close()!!}        
        </div>
      </div>
    </div>
</div>        
<!-- FIN MODAL PARA AGREGAR NUEVO PACIENTE -->


 @section('javascript')
  @section('scripts')   
    <!-- Scripts para ajax de ver usuarios -->
    {!!Html::script('js/scriptpaciente/verpaciente.js')!!}   
  @endsection


@push('scripts')
<script>
    $("#leerpacientes").DataTable({
      select:true,
      "paging":true,
      "bProcessing":true,

    });


    
</script>
<script src="js/ayuda/patient-tour.js"></script>
@endpush


         
@stop