@extends('layouts.layout')

@section('title')
  SINRAIM | Pacientes
@stop
@section('css')

@section('body') 


        
<!-- VER ANTECEDENTES FAMILIARES DEL PACIENTE -->
<div id="tabla-medico" aclass="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h1 class="panel-title">PANEL ANTECEDENTES FAMILIARES </h1>
        </header>
        <div class="modal-content">  
           {!!link_to('/paciente', $title='Regresar a Paciente', $attributes = ['class'=>'btn btn-success pull-left','id'=>'back-patient'], $secure = null)!!}
           <button id="addantecedentesfam" class='btn btn-primary pull-right' data-toggle="modal" data-target="#nuevo-historial">Agregar mas Antecedentes</button></br></br>
                                    
            <h4 class="panel-title">Nombre del Paciente:  <?php echo $nombrepaciente; ?></h4>
            <div class="modal-body">
                 <div id="msj-success" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <strong>Felicidades!</strong> Antecedentes guardados con exito!!!.
                  </div> 
                   <div id="msj-success-elim" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Ancedente Eliminado con exito!!!.
                  </div>  
                  <table class="table table-hover dataTable table-striped width-full text-center" id="verhistoria">
                      <thead>
                          <tr>
                              <th id="familiar">Familiar</th>
                              <th id="Padecimiento">Padecimiento</th>
                              <th id="vivo">Esta Con vida</th>
                              <th id="accion">Acción</th>
                          </tr>
                      </thead>   
                      <tbody>                           
                              @foreach ($histories as $historie)
                                    <tr>    
                                      <td>{{$historie->familiar}}</td>       
                                      <td>{{$historie->descripcion}}</td>    
                                      <td>{{$historie->vida}}</td>  
                                      <td> 
                                        <button type="button" id="eliminarantecedente" class="btn btn-danger" data-plugin="alertify" data-type="log" data-delay="5000" data-confirm-title="Hemos eliminado el Antecedente" data-log-message="Antecedente Eliminado con Exito" onclick="Eliminar({{$historie->id}})">Eliminar</button>   
                                      </td>                                            
                                    </tr> 
                              @endforeach 
                        </tbody>
                  </table>               
            </div>
        </div>
      </div>
  </div>     
<!-- FIN MODAL PARA VER ANTECEDENTES FAMILIARES DEL PACIENTE  -->


<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   


<!-- MODAL PARA AGREGAR ANTECEDENTES FAMILIARES DEL PACIENTE  -->
<div class="modal fade in" id="nuevo-historial" aria-hidden="true" aria-labelledby="nuevo-historial" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sidebar modal-lg">
        <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
              <h1 class="panel-title">Agregar Antecedentes familiares del Paciente</h1>  
              <span class="pull-left blue-800 font-size-18"><strong>Nuevos antecedentes</strong> Usando el botón <strong>+ Agregar nuevo</strong> puede agregar tantos familiares como sean necesario</span>
        </div>
        <div class="modal-content">
                   
            <div class="modal-body">
                 @include('alerts.errors')
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                  @include('Patient.form.historia')  
                  {!!link_to('#', $title='Registrar', $attributes = ['id'=>'registroantecedentes', 'class'=>'btn btn-primary'], $secure = null)!!}               
                      
            </div>
        </div>
      </div>
  </div>     
  <!-- FIN MODAL PARA AGREGAR ANTECEDENTES FAMILIARES DEL PACIENTE  -->

<!-- MODAL PARA EDITAR ANTECEDENTES FAMILIARES DEL PACIENTE  -->
  <div class="modal fade in" id="antecedentesidebar" aria-hidden="true" aria-labelledby="antecedentesidebar" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-sidebar modal-sm">
        <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>              
        </div>
        <div class="modal-content">
                   
            <div class="modal-body">

                  <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                  <input type="hidden" id="id">
                  @include('Patient.form.antecedentes')

            </div>
            <div class='modal-footer'>
                {!!link_to('#', $title = 'Actualizar', $attributes = ['id'=>'actualizar', 'class'=>'btn btn-primary'], $secure = null)!!}   
              </div>
        </div>
      </div>
  </div>
 
  <!-- FIN MODAL PARA EDITAR ANTECEDENTES FAMILIARES DEL PACIENTE  -->

 @section('javascript')
  @section('scripts')   
    <!-- Scripts para ajax de ver usuarios -->
    {!!Html::script('js/scriptpaciente/verantecedente.js')!!}   
    {!!Html::script('js/ayuda/antecedente-tour.js')!!}   
  @endsection

@section('footer')
    @push('scripts')
      <script type="text/javascript">
        $("#addantecedentesfam").click(borrarfila);
        function borrarfila(){
              $("#tablantecedentes tbody tr:nth-child(1)").css("display","none");
        } 

        function addrowclick(counter){   
          var path = "{{URL::route('autocomplete')}}";

          var newrowid = "row-" + counter + "-" + "0";
          $("#descripcion_id").attr('buscador', newrowid);
          $("#descripcion_id").attr("fila",newrowid);
          $("#searchmedicaldiccionaries").attr("fila",newrowid);

          
          $('.searchmedicaldiccionaries').typeahead({
              source:  function (query, process) {
               $.get(path, { query: query }, function (data) {

                        return process(data);

                  }), 'json';
              },
              minLength:2,
              hint: true,
              highlight: true,  
              updater: function(item) {   
                    var variable = counter-1;
                    $("[buscador='row-" + variable + "-" + "0'").val(item.id);
                    return item;
                }
             
          });

        };
      </script>
    @endpush
@endsection

         
@stop