@extends('layouts.layout')

@section('title')
  SINRAIM | Notificadores
@stop
@section('css')

@section('body') 


<!-- Page -->
<div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
          <div class="panel-actions"></div>
          <h1 class="panel-title">PANEL DE CONFIGURACION MEDICOS</h1>
         
        </header>
        <div class="panel-body">
         <a  href="#" class="btn btn-success" id="importarusuario" role="button" data-toggle="modal" data-target="#subirarchivoexcel">Importar Usuarios</a>
         <div class="btn-group" role="group">
              <button type="button" class="btn btn-success dropdown-toggle" id="exampleGroupDrop1" data-toggle="dropdown" aria-expanded="false">Descargar
                  <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu" id="export-menu">
                  <li id="export-to-excel" role="presentation"><a href="{{ URL::to('exportarexcelusuario/xlsx') }}" role="menuitem">Excel</a></li>
                  <li role="presentation"><a href="{{ URL::to('exportarPDFusuario') }}" role="menuitem">PDF</a></li>
              </ul>
          </div>
         <button id="addnewuser" href="#" data-toggle="modal" data-target="#nuevo-medico" type="button" class="btn btn-success pull-right"><i class="icon wb-plus"></i>Agregar nuevo médico</button><br><br>
         @include('User.modal') 
                  @include('alerts.success') 
                  @include('alerts.errors')               
                  
                  <div id="msj-success-act" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <strong>Felicidades!</strong> Usuario acualizado con exito!!!.
                  </div>
                  <div id="msj-success-elim" class="alert alert-success alert-dismissible" role="alert" style="display:none">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            Usuario Eliminado con exito!!!.
                  </div>
 
                   <table class="table table-hover dataTable table-striped width-full text-center" id="leerusuarios">
                      <thead>
                          <tr>
                              <th>Nombres</th>
                              <th>Apellidos</th>
                              <th>Email</th>
                              <th id="accion">Acción</th>
                          </tr>
                      </thead>             
                          <tbody id="datos">
                            @foreach($users as $user)
                                <tr> 
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->last_name}}</td>
                                        <td>{{$user->email}}</td>

                                          <td>
                                            <button id="editarusuario" class='btn btn-primary' onclick="Mostrar({{$user->id}})" data-toggle='modal' data-target='#medicosidebar'>Editar</button> 
                                         
                                             <button type="button" id="eliminarusuario" class="btn btn-danger" data-plugin="alertify" data-type="log" data-delay="5000" data-confirm-title="Hemos eliminado el Usuario" data-log-message="Usuario Eliminado con Exito" onclick="Eliminar({{$user->id}})">Eliminar</button>  
                                          </td>
                                        
                                    </tr> 
                              @endforeach                 
                          </tbody>
                  </table>  
        </div>       
  	</div>
 </div>  

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" id="ayudausuarios" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   

<!-- MODAL NUEVO NOTIFICADOR -->
<div class="modal fade modal-info in" id="nuevo-medico" role="dialog" aria-hidden="true" aria-labelledby="nuevo-medico" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" aria-label="Cerrar" type="button" data-dismiss="modal">X</button>
          <h4 class="modal-title">Agregar Nuevo Médico</h4>
        </div>
        <div class="modal-body">
          {!!Form::open(['route'=>'usuario.store', 'method'=>'POST', 'novalidate'])!!}
                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                @include('User.form.usuario') 
                <div class="form-group">
                    {!!Form::hidden('portada','portada1.jpg',['id'=>'portada','class'=>'form-control'])!!}
                </div>                     
                <div class="form-group">
                    {!!Form::hidden('path','defaultuser.jpg',['id'=>'path','class'=>'form-control'])!!}
                </div>  
                {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!}
             
          {!!Form::close()!!}        
        </div>
      </div>
    </div>
  </div>
<!--FIN MODAL NUEVO NOTIFICADOR -->

<!--MODAL DE EXCEL-->
<div class="modal fade modal-info in" id="subirarchivoexcel" role="dialog" aria-hidden="true" aria-labelledby="subirarchivoexcel" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
             <div class="modal-header">
                <button class="close" aria-label="Cerrar" type="button" data-dismiss="modal">X</button>
                <h4 class="modal-title">Agregar Usuarios Masivamente</h4>
            </div>
            <div class="modal-body">
                {!!Form::open(['route'=>['importarexcelusuario'], 'method'=>'POST', 'enctype'=>'multipart/form-data'])!!}            
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">                    
                       <div class="form-group">
                          {!!Form::label('excel','Subir Excel:')!!}
                          <input type="file" class="filestyle" name="importarusuario" id="importarusuario"></input>
                        </div>
                      <div class="col-sm-12 text-center">
                        {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} 
                      </div>
                {!!Form::close()!!}       
            </div>
            <div class="modal-footer text-center">
            </div>
        </div>
    </div>
</div>
<!--FIN MODAL EXCEL-->


  @section('javascript')
  @section('scripts')
    <!-- Scripts para ajax de crear usuario -->
    {!!Html::script('js/scriptusuario/crearusuario.js')!!}      
    <!-- Scripts para ajax de ver usuarios -->
    {!!Html::script('js/scriptusuario/verusuario.js')!!}  
    <!-- Scripts para ajax de ver usuarios -->
    {!!Html::script('js/scriptusuario/paginacion.js')!!} 

  @endsection


@push('scripts')
<script>
    $("#leerusuarios").DataTable({
      select:true,
      "paging":true,
      "bProcessing":true,

    });
</script>

    <!-- Scripts ayuda de usuario --> 
    {!!Html::script('js/ayuda/user-tour.js')!!} 
@endpush

@stop