          <div class="form-group">
              {!!Form::label('name','Nombre (Name)')!!}
              {!!Form::text('name',null,['id'=>'name','class'=>'form-control', 'placeholder'=>'Ingrese el Nombre del notificador'])!!}
          </div>  
          <div class="form-group">
               {!!Form::label('last_name','Apellido (Last_name)')!!}
               {!!Form::text('last_name',null,['id'=>'last_name','class'=>'form-control', 'placeholder'=>'Ingrese el Apellido del notificador'])!!}
          </div> 
          <div class="form-group">
               {!!Form::label('username','Username')!!}
               {!!Form::text('username',null,['id'=>'username','class'=>'form-control', 'placeholder'=>'Ingrese el Username'])!!}
          </div> 
          <div class="form-group">
               {!!Form::label('password','Password')!!}
               {!!Form::password('password',['id'=>'password','class'=>'form-control', 'placeholder'=>'Ingrese el password'])!!}
          </div> 
         <div class="form-group">
               {!!Form::label('genero','Genero')!!}
               {!!Form::select('genero', ['M' => 'Masculino', 'F' => 'Femenino'], null, ['class' => 'form-control'], ['placeholder' => 'Ingresa el genero'])!!}
          </div> 
          <div class="form-group">
               {!!Form::label('telefono','Teléfono ó Celular')!!}
               {!!Form::text('telefono',null,['id'=>'telefono','class'=>'form-control', 'placeholder'=>'Ingrese el Telefono ó celular'])!!}
          </div>
          <div class="form-group">
               {!!Form::label('email','Email')!!}
               {!!Form::email('email',null,['id'=>'email','class'=>'form-control', 'placeholder'=>'Ingrese el Email'])!!}
          </div> 
           <div class="form-group">
               {!!Form::label('hospital_id','Centro de Salud')!!}
               {!!Form::select('hospital_id', $hospital_id, null, ['class'=>'form-control'] )!!}
          </div> 
           <div class="form-group">
               {!!Form::label('roles_id','Roles')!!}
               {!!Form::select('roles_id', $roles_id, null, ['class'=>'form-control'])!!}
          </div> 