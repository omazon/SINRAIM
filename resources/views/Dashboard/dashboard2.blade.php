@extends('layouts.layout')

@section('title')
  Notificacion | Dashboard
@stop
@section('css')


@section('body')
  {{--<!-- Page -->--}}
  {{--<style>--}}
      {{--#mapster_wrap_0 img{--}}
        {{--width: 200px;--}}
      {{--}--}}
      {{--canvas{--}}
          {{--width:400px;--}}
      {{--}--}}
  {{--</style>--}}
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title text-center">PANEL DE VIGILANCIA - DASHBOARD -</h1><br>
        </header> 
        <!-- PANEL DATOS  -->     
        <div class="panel-body">
            <div id="vistaprobabilidad" class="col-sm-6 col-sm-offset-3">
                <div class="font-size-20 margin-bottom-20 text-uppercase blue-grey-700 text-center">CANTIDAD DE NOTIFICACIONES</div>
                <ul class="list-unstyled margin-bottom-0">
                    <li>
                       <label class="lead text-muted text-center" style="display: block; color:#3583CA; font: 300% sans-serif;">{{ $cantidadnoti }}</label>
                    </li>
                     <label class="lead text-muted text-center" style="display: block; color:#3583CA; font: 200% sans-serif;">{{ $nombredepartamento }}</label>   
                  <li>
                      <div class="counter counter-sm text-left">
                          <div class="counter-number-group margin-bottom-10">
                              <span class="counter-number-related">Cantidad Probadas -  {{$cantidadnotiprobadas}} </span>
                              <span class="counter-number pull-right">Porcentaje: {{ $porcentajeprobadas }}%</span>
                          </div>
                      </div>
                      <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-info bg-red-600" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajeprobadas }}%" role="progressbar">
                              <span class="sr-only">{{ $porcentajeprobadas }}%</span>
                          </div>
                      </div>
                  </li>
                  <li>
                      <div class="counter counter-sm text-left">
                          <div class="counter-number-group margin-bottom-10">
                              <span class="counter-number-related">Cantidad Probables -  {{$cantidadnotiprobables}}</span>
                              <span class="counter-number pull-right">Porcentaje:  {{$porcentajeprobables}}%</span>
                          </div>
                      </div>
                      <div class="progress progress-xs margin-bottom-0">
                          <div class="progress-bar progress-bar-info bg-yellow-600" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajeprobables}}%" role="progressbar">
                              <span class="sr-only">{{ $porcentajeprobables }}%</span>
                          </div>
                      </div>
                  </li><br>                  
                   <li>
                      <div class="counter counter-sm text-left">
                          <div class="counter-number-group margin-bottom-10">
                              <span class="counter-number-related">Cantidad Posibles -  {{$cantidadnotiposibles}}</span>
                              <span class="counter-number pull-right">Porcentaje:  {{ $porcentajeposibles }}%</span>
                          </div>
                      </div>
                      <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-info bg-cyan-600" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajeposibles }}%" role="progressbar">
                              <span class="sr-only">{{ $porcentajeposibles }}%</span>
                          </div>
                      </div>
                  </li>
                   <li>
                      <div class="counter counter-sm text-left">
                          <div class="counter-number-group margin-bottom-10">
                              <span class="counter-number-related">Cantidad Dudosas -  {{$cantidadnotidudosas}}</span>
                              <span class="counter-number pull-right">Porcentaje:  {{ $porcentajedudosas }}%</span>
                          </div>
                      </div>
                      <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-info bg-green-600" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajedudosas }}%" role="progressbar">
                              <span class="sr-only">{{ $porcentajedudosas }}%</span>
                          </div>
                      </div>
                  </li>                       
                </ul>
            </div>
            <div class="row">
                <div class="col-sm-12"><!-- MAPA DE NICARAGUA -->
                    {!!Form::open(['route'=>['dashboard2'], 'method'=>'POST'])!!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        {!!Form::hidden('departamentos','',['id'=>'departamentos','class'=>'form-control'])!!}
                    </div>
                    <div class="widget-content text-center bg-white padding-5">
                        <!--<img class="cover-image" src="../../../archivos/DASHBOARD/nicaragua.png" alt="">-->
                        {{--<iframe id="idiframe" src="/mapa" height="400px" width="400px" scrolling="no" style="border: none; overflow: hidden; padding-left: 0px; padding-bottom: 0px;"></iframe>--}}
                        <div id="principal" class="preloader hide"></div>
                    </div>
                    <div class="col-sm-12 text-center">
                        {!!Form::submit('Buscar',['id'=>'btndepartamento','class'=>'btn btn-primary btn-block'])!!}
                    </div>
                    {!!Form::close()!!}
                </div>
          </div><br>
            <label class="lead text-muted" style="display: block; color:#3583CA; ">NOTIFICACIONES POR DEPARTAMENTO</label>
            <div id="mapa" class="mapa">
                <img src="_mapa/img/mapa-nicaragua-gris.png" state="s1" class="area" alt="" usemap="#Map" />

                <map name="Map" id="Map">
                    <!--RAAN-->
                    <area state="raan" href="#" data-key="1" class="area" id="Raan" alt="Raan" title="raan"  shape="poly" coords="581,60,581,65,583,69,579,73,577,73,573,71,573,65,572,62,565,63,566,60,568,55,572,54,574,52,576,49,582,48,585,55,588,54,592,50,594,47,587,45,593,45,593,37,585,37,578,37,574,37,563,35,561,36,558,37,555,40,550,47,548,49,541,50,541,52,535,52,531,55,530,59,524,60,517,62,511,66,506,67,498,67,496,70,488,72,482,69,477,68,479,70,479,73,476,75,472,75,467,76,464,74,461,73,458,73,460,79,459,85,455,84,453,81,450,81,449,79,446,76,443,81,439,83,436,85,428,85,426,86,422,84,412,86,415,84,412,83,409,82,406,82,404,82,400,80,399,77,396,71,395,66,391,65,386,63,378,63,375,64,374,68,373,76,368,74,362,77,361,79,361,84,361,89,362,93,362,96,368,96,365,95,364,100,365,104,368,106,368,110,367,115,364,118,371,126,380,127,386,128,388,132,385,137,383,144,383,150,382,158,382,161,381,165,377,171,372,178,369,183,366,188,362,195,357,199,354,204,348,202,346,198,340,198,338,202,333,206,324,208,317,208,318,212,315,214,313,217,310,221,314,224,314,229,314,236,307,241,301,244,299,245,300,251,304,256,308,264,312,268,318,274,324,273,325,271,328,275,333,284,337,279,339,279,345,279,348,279,357,278,359,282,361,280,365,278,369,278,373,274,377,278,380,278,383,278,388,280,391,279,396,276,397,273,404,269,408,269,416,269,422,267,425,267,436,265,446,265,455,264,455,261,460,261,463,264,466,264,473,261,476,260,482,261,485,261,490,261,493,264,499,264,503,265,511,267,514,267,521,267,525,264,522,273,522,277,521,279,523,281,527,281,531,281,533,283,536,284,541,289,547,293,550,297,555,295,555,291,555,286,551,283,550,278,550,271,550,261,549,254,548,247,551,242,547,236,552,232,553,235,555,230,555,225,552,225,548,223,549,216,545,215,543,213,547,207,552,207,558,207,558,203,562,195,564,186,563,176,561,179,558,175,555,171,558,170,564,170,570,167,573,163,578,160,582,154,585,143,585,140,582,133,579,128,578,125,579,118,584,116,585,122,589,129,590,137,588,143,594,138,596,132,597,123,597,116,596,111,593,104,591,100,588,90,587,83,584,73" />
                    <!--RAAS-->
                    <area state="raas" data-key="2" class="area" id="Raas" alt="Raas" title="raas" href="#" shape="poly"  coords="544,381,546,375,546,372,543,367,540,361,541,357,543,356,548,355,552,354,552,350,552,344,552,339,551,336,552,328,553,325,549,322,544,322,541,321,540,318,536,313,534,313,533,320,532,322,525,321,522,321,525,323,527,326,530,328,535,328,540,327,544,335,546,341,543,349,542,352,537,356,531,356,527,357,525,357,524,360,525,365,528,365,529,370,529,376,530,382,531,385,534,385,537,385,540,394,538,396,538,401,537,406,537,411,537,416,536,420,536,425,538,431,535,428,534,424,532,420,533,416,533,413,532,409,530,407,524,407,520,407,526,412,527,416,528,419,525,421,523,421,523,424,525,427,525,430,525,434,524,437,523,439,518,445,520,447,520,451,529,455,531,450,531,446,529,441,531,436,534,432,534,451,536,454,538,462,539,467,542,476,542,483,539,483,536,481,534,482,532,482,529,484,526,486,525,490,525,493,525,495,524,500,521,501,517,501,513,503,514,511,512,514,511,517,513,520,512,528,513,533,517,540,519,545,520,551,518,552,513,552,511,554,510,549,510,547,510,543,508,543,504,543,500,546,496,547,494,548,491,548,489,545,488,543,483,543,472,539,470,536,467,534,465,532,461,528,456,526,453,523,450,523,450,520,450,517,449,511,444,511,440,510,434,507,430,502,426,501,423,495,423,489,423,484,423,481,428,475,424,474,423,470,423,465,423,458,420,455,418,451,417,446,421,447,426,446,429,446,428,441,423,441,418,440,416,437,412,433,404,433,401,431,401,426,401,420,399,416,397,412,394,406,392,402,393,398,397,391,395,390,391,390,389,387,387,383,384,380,383,378,383,374,386,370,387,365,382,364,379,360,377,354,375,352,371,348,369,345,367,341,367,338,367,335,364,330,358,328,354,327,350,327,345,325,343,323,341,317,341,312,341,308,341,304,343,301,347,301,348,299,350,297,353,295,353,291,353,289,353,286,355,283,359,280,361,278,362,275,368,274,371,278,374,279,377,277,383,277,386,278,391,278,393,278,394,275,395,273,397,271,402,270,406,270,412,270,416,268,420,268,423,267,426,266,431,266,436,266,440,266,444,264,446,265,448,265,452,266,456,266,453,262,457,261,462,262,462,264,467,264,470,262,476,259,480,261,485,261,488,263,492,263,497,263,501,264,507,264,512,265,517,266,521,269,523,268,521,273,523,281,528,281,531,283,535,285,538,286,539,290,539,293,544,293,549,295,552,296,555,295,553,301,556,308,551,311,555,315,558,324,557,354,558,358,559,366,561,371,562,375,562,379,562,383,559,381,553,380" />
                    <!--Rio San Juan-->
                    <area state="rio" data-key="3" class="area" id="Rio_San_Juan" alt="Rio_San_Juan" title="rio" href="#" shape="poly"  coords="353,476,355,472,358,470,361,468,363,467,367,467,368,465,371,463,369,460,372,458,374,458,378,457,381,454,388,452,388,448,394,447,398,447,405,444,408,448,412,450,418,449,418,446,420,455,421,458,423,460,423,465,422,467,423,469,427,470,427,474,425,479,422,481,422,484,422,494,425,499,425,501,431,506,434,506,440,506,442,511,447,514,449,515,450,521,450,524,454,526,458,526,462,528,465,532,470,540,474,541,478,544,483,544,487,546,493,547,498,547,504,546,508,544,509,549,506,552,509,553,516,553,520,554,525,557,528,561,530,564,531,570,533,567,535,566,536,574,539,577,539,579,539,581,541,586,538,589,534,587,530,587,525,588,520,591,517,593,513,593,511,597,507,597,503,597,501,593,496,589,493,588,491,585,490,589,484,591,477,588,472,587,468,585,467,581,467,577,467,574,463,572,459,574,456,572,451,570,450,567,451,563,449,560,446,561,443,564,440,567,435,565,433,559,429,559,424,558,423,555,418,555,416,554,406,550,378,568,357,559,345,554,344,550,344,547,346,545,349,547,353,551,356,549,360,551,365,553,367,553,370,554,374,554,376,554,383,556,388,552,392,550,393,547,395,544,393,541,393,536,392,533,391,527,390,521,386,521,383,521,384,517,384,515,382,514,380,514,380,510,379,507,379,504,376,502,374,506,372,499,367,496,364,489,362,489,359,486,358,483,354,479" />
                    <!--Chontales-->
                    <area state="chontales" data-key="4" class="area" id="Chontales" alt="Chontales" title="chontales" href="#" shape="poly"  coords="370,344,365,348,362,352,361,356,360,362,361,367,360,370,355,372,351,374,348,374,344,374,339,377,334,380,334,382,328,383,322,383,318,383,312,384,307,385,303,386,297,386,293,388,289,392,289,396,289,402,287,402,284,404,285,409,281,410,280,419,278,421,280,423,284,425,288,425,291,425,296,429,301,436,303,440,305,443,308,445,310,447,310,444,312,443,317,443,320,445,325,449,331,454,334,456,336,459,339,460,344,462,344,464,348,472,351,476,353,473,357,470,359,468,364,468,366,464,369,462,371,459,374,457,378,457,381,455,385,448,390,448,393,448,399,448,403,445,410,447,411,449,416,447,419,444,423,446,430,444,407,433,405,433,400,427,402,422,400,418,400,412,397,409,394,406,393,401,393,396,396,395,394,391,391,388,390,386,388,383,386,379,385,371,386,367,382,364,382,361,378,354,375,351,371,346" />
                    <!--Boaco-->
                    <area state="boaco" data-key="5" class="area" id="Boaco" alt="Boaco" title="boaco" href="#" shape="poly"  coords="240,360,240,364,242,369,243,373,245,376,248,377,246,383,249,388,258,395,257,399,257,405,259,413,263,411,266,416,271,416,273,416,278,422,280,422,280,416,281,412,282,408,283,405,285,403,287,399,288,394,289,391,289,388,292,386,295,386,299,386,302,387,304,387,307,385,312,387,314,385,321,384,324,384,330,384,334,383,336,380,339,377,342,377,347,374,350,374,354,373,357,369,360,367,361,363,361,358,363,357,364,353,368,347,369,345,367,342,367,339,367,336,367,331,362,330,360,328,353,324,353,329,351,332,347,337,347,342,348,345,345,345,343,350,340,350,339,347,332,345,329,341,330,338,324,340,315,335,313,334,310,337,308,340,306,337,304,337,299,339,298,337,296,338,294,342,286,343,281,345,278,343,274,344,271,345,265,347,262,347,258,347,255,346,253,348,253,354,248,355,246,357,243,359" />
                    <!--Matagalpa-->
                    <area state="matagalpa" data-key="6" class="area" id="Matagalpa" alt="Matagalpa" title="matagalpa" href="#" shape="poly"  coords="203,316,203,321,202,329,203,334,201,340,199,344,205,346,208,347,215,348,218,351,221,352,225,355,228,359,235,361,239,361,242,360,246,357,250,353,252,349,253,347,256,347,265,347,272,345,278,344,282,344,289,344,291,340,295,338,301,338,307,339,311,338,313,336,316,336,321,336,321,339,324,339,328,341,330,341,331,345,333,345,334,348,339,349,344,348,344,345,344,342,348,337,349,333,352,331,352,325,348,325,346,323,341,323,341,320,344,316,343,313,341,310,342,304,342,301,343,296,346,301,347,298,348,294,353,293,355,291,352,287,351,284,357,284,357,280,357,277,354,277,350,277,347,277,344,276,345,282,342,281,341,279,337,280,331,282,328,279,328,276,325,274,321,272,318,272,316,268,311,268,310,265,305,262,303,259,302,255,301,251,299,247,298,249,295,253,292,258,290,258,287,258,285,259,283,264,279,266,279,269,275,270,273,273,272,277,268,279,267,282,264,282,262,281,261,284,257,285,255,290,252,292,250,296,247,297,244,297,240,298,236,299,233,299,233,294,229,294,223,292,221,293,220,296,218,302,216,308,215,306,211,306,207,307,203,310,203,314" />
                    <!--Jinotega-->
                    <area state="jinotega" data-key="7" class="area" id="Jinotega" alt="Jinotega" title="jinotega" href="#" shape="poly"  coords="259,188,255,194,252,196,252,202,250,209,249,216,247,224,248,229,251,232,249,237,246,236,245,234,241,234,238,231,233,234,229,232,228,234,228,239,225,240,222,242,218,241,213,243,209,247,208,253,208,261,206,267,205,274,207,277,210,282,215,287,222,289,222,294,228,294,232,295,233,300,238,299,241,297,246,295,252,293,256,290,259,284,261,282,267,282,269,278,272,273,274,271,277,269,279,266,282,262,284,258,288,257,293,254,300,251,300,246,301,243,305,240,312,238,314,236,316,231,316,227,314,223,313,220,315,218,316,215,317,212,318,209,322,207,329,207,334,204,346,196,357,201,384,160,388,129,381,127,373,126,368,120,366,120,368,114,368,111,370,106,366,105,366,100,366,96,364,91,361,91,355,96,351,96,348,96,345,99,345,105,342,107,342,109,341,114,338,117,336,121,340,122,340,126,343,128,344,131,341,134,340,137,334,137,331,136,325,135,321,137,319,140,317,141,316,146,312,151,309,153,306,151,306,158,303,156,297,163,295,159,293,159,291,160,286,162,283,163,281,169,281,166,280,171,279,174,273,173,270,176,271,181,273,186,273,188,271,189,258,193,261,188,264,189" />
                    <!--Nueva Segovia-->
                    <area state="segovia" data-key="8" class="area" id="Nueva_Segovia" alt="Nueva_Segovia" title="segovia" href="#" shape="poly"  coords="260,185,257,178,252,180,242,173,233,166,233,157,224,167,203,197,175,200,164,195,142,197,143,211,152,212,160,220,173,222,189,219,202,220,215,216,225,226,232,231,244,233,251,232,247,215,250,200,254,192" />
                    <!--Madriz-->
                    <area state="madriz" data-key="9" class="area" id="Madriz" alt="Madriz" title="madriz" href="#" shape="poly"  coords="228,233,225,231,224,227,221,225,220,221,218,219,212,218,212,221,205,222,199,223,198,220,195,220,193,221,190,222,188,222,178,221,175,221,170,221,165,224,160,222,157,220,156,216,152,214,145,213,142,213,140,213,139,217,140,221,143,225,143,228,143,232,145,236,144,238,144,242,143,244,144,247,146,251,146,254,146,259,145,263,145,266,147,271,150,267,155,265,158,262,159,257,160,253,162,250,165,247,167,243,170,243,173,243,178,245,183,246,188,245,195,242,201,241,206,242,209,245,213,244,218,242,222,242,226,239" />
                    <!--Esteli-->
                    <area state="esteli" data-key="10" class="area" id="Esteli" alt="Esteli" title="esteli" href="#" shape="poly"  coords="147,270,153,266,156,262,159,258,162,252,165,247,168,241,172,241,176,244,182,244,189,245,196,243,202,241,207,243,210,248,208,252,205,256,208,259,208,264,208,267,205,269,204,272,204,275,207,278,209,282,212,285,217,287,222,290,222,295,219,297,217,304,213,307,210,307,206,307,204,312,202,316,201,319,198,316,193,316,190,315,187,313,184,313,183,309,182,304,185,298,184,295,180,293,179,289,177,284,173,284,170,282,166,283,164,284,160,286,157,287,153,287,149,286,145,286,144,284,142,280,140,277,144,273" />
                    <!--chinandega-->
                    <area state="chinandega" data-key="11" class="area" id="Chinandega" alt="Chinandega" title="chinandega" href="#" shape="poly"  coords="141,264,138,262,136,260,129,259,126,259,120,262,120,267,120,270,118,276,118,279,118,282,117,286,117,289,113,292,106,298,103,303,92,301,71,298,77,303,74,304,71,307,73,310,75,312,70,312,68,311,50,305,46,303,45,299,42,295,37,290,35,292,35,287,34,285,31,291,29,296,22,298,22,307,21,311,24,313,28,315,33,320,39,320,40,323,45,328,48,329,53,334,58,337,59,341,63,346,68,344,66,348,70,350,73,353,76,356,79,360,85,362,87,364,89,362,89,367,94,369,97,368,102,365,105,363,107,363,111,362,112,360,115,358,116,355,116,351,116,347,120,346,119,342,121,340,125,342,128,340,132,335,132,332,132,329,136,329,138,330,140,330,143,330,146,329,149,327,151,326,151,323,151,319,151,315,149,313,150,307,152,303,150,299,144,293,144,290,144,285,141,283,140,278,142,274,145,272,147,271,146,268,144,265,140,261" />
                    <!--León-->
                    <area state="leon" data-key="12" class="area" id="Leon" alt="Leon" title="Leon" href="#" shape="poly"  coords="85,368,88,370,90,372,93,374,96,374,99,376,101,378,104,381,109,383,111,385,116,388,119,391,121,391,125,394,126,397,129,397,135,401,132,399,137,401,140,401,140,404,138,404,138,409,140,411,141,414,143,415,143,418,144,422,147,426,149,428,152,428,152,426,150,424,152,422,153,420,155,419,158,419,160,419,162,419,165,418,167,416,167,414,170,413,170,410,170,408,168,406,168,404,168,401,167,398,167,394,168,392,167,390,162,388,161,385,158,384,158,381,157,378,157,374,161,373,163,375,166,376,170,379,172,379,175,377,178,372,182,372,184,370,192,368,191,365,190,363,189,360,189,357,190,354,190,352,191,350,194,348,196,347,199,348,200,344,201,338,201,333,202,331,202,328,202,325,203,322,201,321,201,318,198,318,195,318,192,317,192,315,189,314,187,313,184,311,181,309,181,306,182,303,182,301,183,296,183,293,181,292,180,290,178,288,177,286,173,284,167,284,164,284,161,286,159,287,155,288,151,286,149,286,146,286,142,286,144,289,145,292,145,294,147,298,149,300,149,302,151,303,150,310,151,315,151,318,152,321,151,323,150,325,147,327,147,329,145,330,140,329,136,329,133,329,132,333,132,335,129,337,127,339,123,340,122,342,121,345,120,348,116,349,116,351,116,355,116,360,114,360,109,361,107,363,101,366,96,366,94,368,94,370,97,372" />
                    <!--Managua-->
                    <area state="managua" data-key="13" class="area" id="Managua" alt="Managua" title="managua" href="#" shape="poly"  coords="151,429,155,427,154,424,153,422,152,418,155,418,157,418,161,418,164,418,167,415,170,412,171,406,169,404,167,401,167,397,168,395,168,392,167,389,164,389,162,387,160,383,158,383,159,380,160,376,160,374,163,374,165,379,170,380,174,379,175,376,176,373,179,371,182,371,187,370,187,368,190,368,193,366,192,364,191,362,189,360,189,358,191,355,193,351,195,348,201,346,210,347,215,347,218,349,221,351,224,352,225,355,227,357,230,358,234,359,238,360,239,363,240,368,243,372,245,376,248,377,247,380,247,383,249,387,253,389,254,392,257,393,259,396,258,399,258,402,254,403,249,406,246,409,240,410,233,409,228,409,224,412,221,415,216,417,214,419,210,423,213,426,217,429,213,432,211,434,207,434,205,432,204,430,201,431,200,436,197,439,195,443,191,451,189,454,185,457,179,461,180,463,180,467,177,465,172,463,169,460,170,457,169,453,166,447,164,444,160,444,158,443,159,439,154,433,150,432" />

                    <!--Masaya-->
                    <area state="masaya" data-key="14" class="area" id="Masaya" alt="Masaya" title="masaya" href="#" shape="poly"  coords="216,417,218,415,218,412,222,413,226,412,228,410,232,409,235,412,238,413,239,417,241,418,241,421,240,427,238,430,236,434,234,437,232,438,229,440,229,442,227,443,226,447,224,450,221,450,218,448,217,446,214,442,212,443,209,438,206,438,203,438,201,438,199,436,201,432,203,431,207,434,211,432,213,432,215,427,213,426,211,422" />
                    <!--Carazo-->
                    <area state="carazo" data-key="15" class="area" id="Carazo" alt="Carazo" title="carazo" href="#" shape="poly"  coords="217,493,217,489,219,486,220,484,223,481,222,478,220,476,220,472,221,467,224,467,227,465,225,463,224,461,222,459,222,455,224,452,226,452,225,449,222,450,219,448,217,446,216,443,214,442,212,440,208,439,204,439,204,437,202,435,200,434,198,440,195,443,192,447,190,450,188,454,187,457,183,457,182,461,180,465,183,468,186,472,191,476,195,480,200,483,203,483,209,488" />

                    <!--Granada-->
                    <area state="granada" data-key="16" class="area" id="Granada" alt="Granada" title="granada" href="#" shape="poly"  coords="265,415,265,413,263,411,260,409,257,410,257,408,255,403,252,402,249,403,248,409,236,411,238,413,241,416,241,419,241,425,240,429,235,431,233,437,230,438,229,441,227,448,226,450,223,454,223,459,223,463,225,467,223,467,221,467,220,470,219,473,221,480,224,481,228,481,234,479,235,475,239,474,242,475,248,475,250,470,248,469,248,466,250,463,252,458,252,451,252,448,249,446,248,444,251,443,251,440,246,440,245,437,246,434,246,432,247,427,250,424,249,422,252,420,253,417,258,418" />
                    <!--Rivas-->
                    <area state="rivas" data-key="17" class="area" id="Rivas" alt="Rivas" title="rivas" href="#" shape="poly"  coords="342,553,344,551,343,548,343,544,351,549,355,549,358,551,362,553,366,551,370,552,377,556,383,556,388,552,394,552,392,550,394,547,395,542,391,541,392,536,393,533,393,528,393,523,386,520,383,516,380,512,377,509,380,505,379,503,374,503,372,498,369,495,367,490,362,488,359,483,358,480,354,477,352,476,350,472,348,468,343,467,341,463,338,459,333,457,331,457,325,451,318,446,313,442,310,444,308,445,304,442,301,438,301,433,296,429,292,426,289,424,282,424,278,422,274,418,271,417,266,415,261,414,259,414,256,417,252,418,250,422,246,426,244,431,243,434,243,439,247,439,249,442,250,448,250,451,250,455,251,458,252,462,249,464,245,466,249,470,252,471,257,471,246,473,240,473,235,474,231,476,228,481,223,482,220,482,217,485,216,492,218,499,223,503,227,506,232,510,235,512,239,515,242,515,245,520,247,524,250,526,254,531,258,532,265,535,264,541,263,544,270,547,277,550,281,544,281,538,285,535,289,534,301,541,309,541,313,544,318,546,325,546,330,549,338,551,341,551" />
                </map>
            </div>
           <div id="topinsumosnoti" class="row">
             <label class="lead text-muted" style="display: block; color:#3583CA; ">TOP INSUMOS MEDICOS MAS NOTIFICADOS</label>
             <div class="col-sm-4">          
                  <!-- TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->     
                    <div class="widget">
                        <div class="widget-header white bg-red-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $topunoinsumonombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/1.png" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">1</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topunoinsumoconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div><!-- FIN TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->     
              </div>   
              <div class="col-sm-4">          
                  <div class="widget">
                        <div class="widget-header white bg-red-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $topdosinsumonombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/2.png" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">2</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topdosinsumoconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div><!-- FIN TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->    
              </div>        
              <div class="col-sm-4">          
                    <div class="widget">
                        <div class="widget-header white bg-red-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $toptresinsumonombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/3.png" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">3</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $toptresinsumoconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>   
              </div>    
          </div> <!-- FIN TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->  


           <div class="row">
             <label class="lead text-muted" style="display: block; color:#3583CA; ">TOP REACCIONES ADVERSAS MAS NOTIFICADAS</label>
             <div class="col-sm-4">          
                  <!-- TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->     
                    <div class="widget">
                        <div class="widget-header white bg-red-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $topunoreaccionnombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/1.png" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">1</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topunoreaccionconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>    
              </div>   
              <div class="col-sm-4">          
                  <div class="widget">
                        <div class="widget-header white bg-red-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $topdosreaccionnombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/2.png" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">2</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topdosreaccionconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div> 
              </div>        
              <div class="col-sm-4">          
                    <div class="widget">
                        <div class="widget-header white bg-red-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $toptresreaccionnombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/3.png" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">3</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $toptresreaccionconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>  
              </div>    
          </div><!-- FIN TOP REACCION ADVERSA MAS NOTIFICADAS  -->   



           <div class="row">
             <label class="lead text-muted" style="display: block; color:#3583CA; ">TOP MEDICOS QUE MAS NOTIFICAN</label>
             <div class="col-sm-4">          
                  <!-- TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->     
                    <div class="widget">
                        <div class="widget-header white bg-blue-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $topunousernombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/{{$topunouserpath}}" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">1</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number blue-600" style="display: block; font: 250% sans-serif;">{{ $topunouserconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>  
              </div>   
              <div class="col-sm-4">          
                  <div class="widget">
                        <div class="widget-header white bg-blue-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $topdosusernombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/{{$topdosuserpath}}" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">2</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number blue-600" style="display: block; font: 250% sans-serif;">{{ $topdosuserconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div> 
              </div>   

               <div class="col-sm-4">          
                  <div class="widget">
                        <div class="widget-header white bg-blue-600 padding-30 clearfix">
                             <div class="font-size-20 margin-bottom-15">{{ $toptresusernombres }}</div>
                            <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                <img src="../../../archivos/{{$toptresuserpath}}" alt="">
                            </a>
                            <div class="pull-right">
                                  <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">3</label>
                            </div>
                        </div>
                        <div class="widget-content">
                            <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                <div class="col-xs-12">
                                    <div class="counter">                    
                                        <span class="counter-number blue-600" style="display: block; font: 250% sans-serif;">{{ $toptresuserconteos }}</span>
                                        <div class="counter-label">Cantidad Notificados</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div> 
              </div>           
              
          </div><!-- FIN TOP MEDICOS QUE MAS NOTIFICAN  -->   

       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

 <!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating site-tour-trigger">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   


 @section('javascript')
  @section('scripts')  
      <!-- Scripts para TOUR DE AYUDA -->
    {!!Html::script('js/ayuda/dashboard-tour.js')!!}

      <script type="text/javascript" src="_mapa/js/jquery.rwdImageMaps.min.js"></script>
      <script type="text/javascript" src="_mapa/js/src/redist/when.js"></script>
      <script type="text/javascript" src="_mapa/js/src/core.js"></script>
      <script type="text/javascript" src="_mapa/js/src/graphics.js"></script>
      <script type="text/javascript" src="_mapa/js/src/mapimage.js"></script>
      <script type="text/javascript" src="_mapa/js/src/mapdata.js"></script>
      <script type="text/javascript" src="_mapa/js/src/areadata.js"></script>
      <script type="text/javascript" src="_mapa/js/src/areacorners.js"></script>
      <script type="text/javascript" src="_mapa/js/src/scale.js"></script>
      <script type="text/javascript" src="_mapa/js/main.php"></script>
  @endsection

  @section('footer')
    @push('scripts')
         <script type="text/javascript">

           $(document).ready(function(){
                  $("map area").click(elegirdepartamento);

              });
               
              function elegirdepartamento(){
                  if($(this).attr('alt')=='Raas'){   
                      $("#departamentos").val("ATLÁNTICO SUR");
                  }
                  if($(this).attr('alt')=='Raan'){                
                      $("#departamentos").val("ATLÁNTICO NORTE");
                  }
                  if($(this).attr('alt')=='Jinotega'){                
                      $("#departamentos").val("JINOTEGA");
                  }
                  if($(this).attr('alt')=='Matagalpa'){                       
                      $("#departamentos").val("MATAGALPA");
                  }
                  if($(this).attr('alt')=='Boaco'){                
                      $("#departamentos").val("BOACO");
                  }
                  if($(this).attr('alt')=='Chontales'){                
                      $("#departamentos").val("CHONTALES");
                  }

                   if($(this).attr('alt')=='Rio_San_Juan'){   
                      $("#departamentos").val("RIO SAN JUAN");
                  }
                  if($(this).attr('alt')=='Rivas'){                
                      $("#departamentos").val("RIVAS");
                  }
                  if($(this).attr('alt')=='Granada'){                
                      $("#departamentos").val("GRANADA");
                  }
                  if($(this).attr('alt')=='Carazo'){                        
                      $("#departamentos").val("CARAZO");
                  }
                  if($(this).attr('alt')=='Masaya'){                
                      $("#departamentos").val("MASAYA");
                  }
                  if($(this).attr('alt')=='Managua'){                
                      $("#departamentos").val("MANAGUA");
                  }

                   if($(this).attr('alt')=='Leon'){   
                      $("#departamentos").val("LEÓN");
                  }
                  if($(this).attr('alt')=='Chinandega'){                
                      $("#departamentos").val("CHINANDEGA");
                  }
                  if($(this).attr('alt')=='Esteli'){                
                      $("#departamentos").val("ESTELI");
                  }
                  if($(this).attr('alt')=='Madriz'){                       
                      $("#departamentos").val("MADRIZ");
                  }
                  if($(this).attr('alt')=='Nueva_Segovia'){                
                      $("#departamentos").val("NUEVA SEGOVIA");
                  }
              }
            

          </script>
      
    @endpush
@endsection
    
@stop

