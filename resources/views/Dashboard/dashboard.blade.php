@extends('layouts.layout')

@section('title')
    Notificacion | Dashboard
@stop
@section('css')


@section('body')
    <!-- Page -->
    <div id="tabla-medico" class="col-sm-12">
        <div class="panel col-sm-8">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h1 class="panel-title text-center">PANEL DE VIGILANCIA - DASHBOARD -</h1><br>
            </header>
            <!-- PANEL DATOS  -->
            <div class="panel-body">
                <div class="row">
                    <label class="lead text-muted" style="display: block; color:#3583CA; ">NOTIFICACIONES POR DEPARTAMENTO</label>
                    <div class="col-sm-6"><!-- MAPA DE NICARAGUA -->
                        {!!Form::open(['route'=>['dashboard'], 'method'=>'POST'])!!}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            {!!Form::hidden('departamentos','',['id'=>'departamentos','class'=>'form-control'])!!}
                        </div>
                        <div class="widget-content text-center bg-white padding-5">
                            <!--<img class="cover-image" src="../../../archivos/DASHBOARD/nicaragua.png" alt="">-->
                            <iframe id="idiframe" src="/mapa" height="400px" width="400px" scrolling="no" style="border: none; overflow: hidden; padding-left: 0px; padding-bottom: 0px;"></iframe>
                        </div>
                        <div class="col-sm-12 text-center">
                            {!!Form::submit('Buscar',['id'=>'btndepartamento','class'=>'btn btn-primary btn-block'])!!}
                        </div>
                        {!!Form::close()!!}
                    </div>
                    <div id="vistaprobabilidad" class="col-sm-6">
                        <div class="font-size-20 margin-bottom-20 text-uppercase blue-grey-700 text-center">CANTIDAD DE NOTIFICACIONES</div>
                        <ul class="list-unstyled margin-bottom-0">
                            <li>
                                <label class="lead text-muted text-center" style="display: block; color:#3583CA; font: 300% sans-serif;">{{ $cantidadnoti }}</label>
                            </li>
                            <label class="lead text-muted text-center" style="display: block; color:#3583CA; font: 200% sans-serif;">{{ $nombredepartamento }}</label>
                            <li>
                                <div class="counter counter-sm text-left">
                                    <div class="counter-number-group margin-bottom-10">
                                        <span class="counter-number-related">Cantidad Probadas -  {{$cantidadnotiprobadas}} </span>
                                        <span class="counter-number pull-right">Porcentaje: {{ $porcentajeprobadas }}%</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-info bg-red-600" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajeprobadas }}%" role="progressbar">
                                        <span class="sr-only">{{ $porcentajeprobadas }}%</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="counter counter-sm text-left">
                                    <div class="counter-number-group margin-bottom-10">
                                        <span class="counter-number-related">Cantidad Probables -  {{$cantidadnotiprobables}}</span>
                                        <span class="counter-number pull-right">Porcentaje:  {{$porcentajeprobables}}%</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs margin-bottom-0">
                                    <div class="progress-bar progress-bar-info bg-yellow-600" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajeprobables}}%" role="progressbar">
                                        <span class="sr-only">{{ $porcentajeprobables }}%</span>
                                    </div>
                                </div>
                            </li><br>
                            <li>
                                <div class="counter counter-sm text-left">
                                    <div class="counter-number-group margin-bottom-10">
                                        <span class="counter-number-related">Cantidad Posibles -  {{$cantidadnotiposibles}}</span>
                                        <span class="counter-number pull-right">Porcentaje:  {{ $porcentajeposibles }}%</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-info bg-cyan-600" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajeposibles }}%" role="progressbar">
                                        <span class="sr-only">{{ $porcentajeposibles }}%</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="counter counter-sm text-left">
                                    <div class="counter-number-group margin-bottom-10">
                                        <span class="counter-number-related">Cantidad Dudosas -  {{$cantidadnotidudosas}}</span>
                                        <span class="counter-number pull-right">Porcentaje:  {{ $porcentajedudosas }}%</span>
                                    </div>
                                </div>
                                <div class="progress progress-xs">
                                    <div class="progress-bar progress-bar-info bg-green-600" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentajedudosas }}%" role="progressbar">
                                        <span class="sr-only">{{ $porcentajedudosas }}%</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div><br>
                <div id="topinsumosnoti" class="row">
                    <label class="lead text-muted" style="display: block; color:#3583CA; ">TOP INSUMOS MEDICOS MAS NOTIFICADOS</label>
                    <div class="col-sm-4">
                        <!-- TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->
                        <div class="widget">
                            <div class="widget-header white bg-red-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $topunoinsumonombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/1.png" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">1</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topunoinsumoconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- FIN TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->
                    </div>
                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="widget-header white bg-red-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $topdosinsumonombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/2.png" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">2</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topdosinsumoconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- FIN TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->
                    </div>
                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="widget-header white bg-red-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $toptresinsumonombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/3.png" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">3</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $toptresinsumoconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- FIN TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->


                <div class="row">
                    <label class="lead text-muted" style="display: block; color:#3583CA; ">TOP REACCIONES ADVERSAS MAS NOTIFICADAS</label>
                    <div class="col-sm-4">
                        <!-- TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->
                        <div class="widget">
                            <div class="widget-header white bg-red-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $topunoreaccionnombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/1.png" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">1</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topunoreaccionconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="widget-header white bg-red-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $topdosreaccionnombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/2.png" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">2</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $topdosreaccionconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="widget-header white bg-red-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $toptresreaccionnombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/DASHBOARD/TOPINSUMOSRA/3.png" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">3</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number red-600" style="display: block; font: 250% sans-serif;">{{ $toptresreaccionconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- FIN TOP REACCION ADVERSA MAS NOTIFICADAS  -->



                <div class="row">
                    <label class="lead text-muted" style="display: block; color:#3583CA; ">TOP MEDICOS QUE MAS NOTIFICAN</label>
                    <div class="col-sm-4">
                        <!-- TOP INSUMOS MEDICOS MAS NOTIFICADOS  -->
                        <div class="widget">
                            <div class="widget-header white bg-blue-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $topunousernombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/{{$topunouserpath}}" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">1</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number blue-600" style="display: block; font: 250% sans-serif;">{{ $topunouserconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="widget-header white bg-blue-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $topdosusernombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/{{$topdosuserpath}}" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">2</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number blue-600" style="display: block; font: 250% sans-serif;">{{ $topdosuserconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="widget">
                            <div class="widget-header white bg-blue-600 padding-30 clearfix">
                                <div class="font-size-20 margin-bottom-15">{{ $toptresusernombres }}</div>
                                <a class="avatar avatar-100 pull-left margin-right-20" href="javascript:void(0)">
                                    <img src="../../../archivos/{{$toptresuserpath}}" alt="">
                                </a>
                                <div class="pull-right">
                                    <label class="lead text-muted" style="display: block; color:#F3F7F9; font: 400% sans-serif;">3</label>
                                </div>
                            </div>
                            <div class="widget-content">
                                <div class="row no-space padding-vertical-20 padding-horizontal-30 text-center">
                                    <div class="col-xs-12">
                                        <div class="counter">
                                            <span class="counter-number blue-600" style="display: block; font: 250% sans-serif;">{{ $toptresuserconteos }}</span>
                                            <div class="counter-label">Cantidad Notificados</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- FIN TOP MEDICOS QUE MAS NOTIFICAN  -->

            </div><!-- FIN PANEL BODY -->
        </div><!-- FIN PANEL BOOTSTRAP -->
    </div><!-- FIN PAGE PRINCIPAL -->

    <!--AYUDA POR TOUR-->
    <div class="site-action">
        <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating site-tour-trigger">
            <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
        </button>
    </div>
    <!--FIN AYUDA POR TOUR-->


@section('javascript')
@section('scripts')
    <!-- Scripts para TOUR DE AYUDA -->
    {!!Html::script('js/ayuda/dashboard-tour.js')!!}
@endsection

@section('footer')
    @push('scripts')
    <script type="text/javascript">

        $(document).ready(function(){
            $("iframe").contents().find("body").find("#mapa").find("map").find("#Raas,#Raan,#Jinotega,#Matagalpa,#Boaco,#Chontales,#Rio_San_Juan,#Rivas,#Granada,#Carazo,#Masaya,#Managua,#Leon,#Chinandega,#Esteli,#Madriz,#Nueva_Segovia").click(elegirdepartamento);

        });

        function elegirdepartamento(){
            if($(this).attr('alt')=='Raas'){
                $("#departamentos").val("ATLÁNTICO SUR");
            }
            if($(this).attr('alt')=='Raan'){
                $("#departamentos").val("ATLÁNTICO NORTE");
            }
            if($(this).attr('alt')=='Jinotega'){
                $("#departamentos").val("JINOTEGA");
            }
            if($(this).attr('alt')=='Matagalpa'){
                $("#departamentos").val("MATAGALPA");
            }
            if($(this).attr('alt')=='Boaco'){
                $("#departamentos").val("BOACO");
            }
            if($(this).attr('alt')=='Chontales'){
                $("#departamentos").val("CHONTALES");
            }

            if($(this).attr('alt')=='Rio_San_Juan'){
                $("#departamentos").val("RIO SAN JUAN");
            }
            if($(this).attr('alt')=='Rivas'){
                $("#departamentos").val("RIVAS");
            }
            if($(this).attr('alt')=='Granada'){
                $("#departamentos").val("GRANADA");
            }
            if($(this).attr('alt')=='Carazo'){
                $("#departamentos").val("CARAZO");
            }
            if($(this).attr('alt')=='Masaya'){
                $("#departamentos").val("MASAYA");
            }
            if($(this).attr('alt')=='Managua'){
                $("#departamentos").val("MANAGUA");
            }

            if($(this).attr('alt')=='Leon'){
                $("#departamentos").val("LEÓN");
            }
            if($(this).attr('alt')=='Chinandega'){
                $("#departamentos").val("CHINANDEGA");
            }
            if($(this).attr('alt')=='Esteli'){
                $("#departamentos").val("ESTELI");
            }
            if($(this).attr('alt')=='Madriz'){
                $("#departamentos").val("MADRIZ");
            }
            if($(this).attr('alt')=='Nueva_Segovia'){
                $("#departamentos").val("NUEVA SEGOVIA");
            }
        }


    </script>

    @endpush
@endsection

@stop

