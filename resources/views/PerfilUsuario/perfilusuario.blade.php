@extends('layouts.layout')

@section('title')
  Notificacion | Evaluar Notificacion
@stop
@section('css')
      <style type="text/css">
        #cabecera-perfil{
            background-image: url("../../../archivos/PORTADAS/{{Auth::user()->portada}}");
            padding-top: 10%;
            padding-bottom: 7%;
            margin-bottom: 2%;
        }
      </style>


@section('body')
<!-- PANEL DATOS DEL PACIENTE -->     
<section id="cabecera-perfil" data-toggle="modal" data-target="#portada">
    <div style="padding-top:20px;padding-bottom:20px;" data-toggle="tooltip" data-placement="bottom" title="Puedes cambiar la foto que tienes de portada, solo haz click en la foto y elige la que más te guste"></div>
</section>
<section id="logrosymetas">
    <div class="container">
        <div class="row">
          
            <!-- Panel Web Designer -->
              <div class="col-sm-4 masonry-item">
                <div class="widget widget-shadow">
                  <div class="widget-header bg-blue-600 text-center padding-30 padding-bottom-15">
                    <a class="avatar avatar-100 img-bordered margin-bottom-10 bg-white" href="javascript:void(0)">
                      <img src="../../../archivos/{{Auth::user()->path}}" alt="">
                    </a>
                  </div>
                  <div id="informacion-general" class="widget-footer padding-horizontal-30 padding-vertical-20 text-center">
                    <div class="row no-space">
                        <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->name; ?></label>
                        <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->last_name; ?></label>
                        <div style="background-color:#e4eaec; border-radius: 10px 10px 10px 10px;-moz-border-radius: 10px 10px 10px 10px;-webkit-border-radius: 10px 10px 10px 10px;border: 0px solid #000000;">
                            <p class="blue-800 margin-bottom-20">
                                 <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Profesiones: </label>
                                 @foreach ($nombreprofesionperfils as $nombreprofesionperfil)
                                      * <?php echo $nombreprofesionperfil->nombreprofesion; ?><br>     
                                 @endforeach 
                            </p> 
                        </div> 
                         <div style="background-color:#e4eaec; border-radius: 10px 10px 10px 10px;-moz-border-radius: 10px 10px 10px 10px;-webkit-border-radius: 10px 10px 10px 10px;border: 0px solid #000000;">
                            <p class="blue-800 margin-bottom-20">
                                 <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Especialidades: </label>
                                 @foreach ($nombrespecialidadperfils as $nombrespecialidadperfil)  
                                      * <?php echo $nombrespecialidadperfil->nombrespecialidad; ?><br>   
                                 @endforeach 
                            </p> 
                        </div>  
                        <div id="btn-config">                        
                           <button type="button" class="btn btn-success padding-horizontal-40" data-toggle="modal" data-target="#subirarchivo">Cambiar Foto Perfil</button><br>               
                          <button type="button" class="btn btn-primary padding-horizontal-40" data-toggle="modal" data-target="#portada">Cambiar Portada</button> 
                        </div>                     
                    </div>
                  </div>
                </div>
              </div>           
          <!-- End Panel Web Designer -->
            <div class="col-sm-8">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#datobasico">DATOS BASICOS</a></li>
                    <li><a data-toggle="tab" href="#contacto">DATOS DE CONTACTO</a></li>
                    <li><a data-toggle="tab" href="#profesional">DATOS PROFESIONALES</a></li>
                </ul>
                <div class="tab-content text-center">
                    <div id="datobasico" class="tab-pane fade in active">
                        <div class="col-sm-12">
                            <div class="col-sm-12 active">
                                <br>      
                                <table class="espacio">
                                     <tr>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Nombres:  </label>
                                          </td>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->name; ?></label>
                                          </td>
                                     </tr>  
                                     <tr>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Apellidos:  </label>
                                          </td>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->last_name; ?></label>
                                          </td>
                                     </tr>  
                                      <tr>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Username:  </label>
                                          </td>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->username; ?></label>
                                          </td>
                                     </tr>  
                                      <tr>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Genero:  </label>
                                          </td>
                                          <td>
                                                <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->genero; ?></label>
                                          </td>
                                     </tr>  
                                </table>
                                {!!Form::open(['route'=>['cambiarpassword'], 'method'=>'POST'])!!}
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                                    <div class="row">
                                        <div class="col-sm-12">
                                             <div class="form-group">
                                                    <label class="lead text-muted pull-left" style="display: block; color:#3583CA; font-weight: bold;">Password:  </label>
                                                   {!!Form::password('password',['id'=>'password','class'=>'form-control', 'placeholder'=>'Ingrese el password'])!!}
                                              </div>                                                  
                                        </div>  
                                        <div class="col-sm-12 text-center">
                                            {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} 
                                        </div>  
                                    </div><br>                  
                                {!!Form::close()!!}  
                               
                            </div>
                        </div>  
                    </div>
                    <div id="contacto" class="tab-pane fade">
                            <div class="col-sm-12"><br> 
                                  <div class="form-group">
                                       <label class="lead text-muted pull-left black-600" style="display: block;">Email:  </label>    
                                      <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->email; ?></label>                                    
                                  </div> 
                                 
                                    <div class="row">
                                      <div class="form-group">
                                            <label class="lead text-muted pull-left black-600" style="display: block;">Teléfono ó Celular:  </label>
                                            <label class="lead text-muted" style="display: block; color:#263238;"><?php echo Auth::user()->telefono; ?></label>    
                                           
                                      </div> 
                                      <div class="form-group">
                                           <label class="lead text-muted pull-left black-600" style="display: block;">Hospital ó Centro de Salud:  </label>    
                                            <label class="lead text-muted" style="display: block; color:#263238;"> {{$nombrehospital}} </label>                                      
                                      </div> 
                                       <div>
                                          <div class="example-buttons">
                                              <a class="btn btn-primary collapsed pull-left" data-toggle="collapse" href="#exampleCollapseExample" aria-expanded="false" aria-controls="exampleCollapseExample">
                                                    Editar
                                              </a>                                           
                                          </div>
                                          <div class="collapse" id="exampleCollapseExample" aria-expanded="false" style="height: 0px;">
                                            <div class="well">
                                                {!!Form::open(['route'=>['cambiarcontactos'], 'method'=>'POST'])!!}
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                                                      {!!Form::text('telefono',Auth::user()->telefono,['id'=>'telefono','class'=>'form-control', 'placeholder'=>'Ingrese el Telefono ó celular'])!!}
                                                      <div class="col-sm-12 pull-left">
                                                          {!!Form::submit('Actualizar telefono',['class'=>'btn btn-primary pull-left'])!!} 
                                                      </div>
                                                       <br>
                                                {!!Form::close()!!}  
                                                {!!Form::open(['route'=>['cambiarhospital'], 'method'=>'POST'])!!}
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                                                      {!!Form::select('hospital_id', $hospital_id, null, ['id'=>'hospital_id', 'class'=>'form-control'] )!!}
                                                      {!!Form::hidden('idhospitalperfil',null,['id'=>'idhospitalperfil', 'class'=>'form-control'])!!}
                                                      <div class="col-sm-12 pull-left">
                                                          {!!Form::submit('Actualizar Hospital',['class'=>'btn btn-primary pull-left'])!!} 
                                                      </div>
                                                       <br>
                                                {!!Form::close()!!}  
                                            </div>
                                          </div>
                                        </div>  
                                    </div><br>                  
                                
                                 
                            </div>                
                    </div><br><br>
                    <div id="profesional" class="tab-pane fade">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <div class="row">
                            <div class="col-sm-12">
                                  <div class="form-group">
                                      <label class="lead text-muted pull-left black-600" style="display: block;">Agregue Profesiones:  </label>
                                      <select name="profesiones[]" class="form-control" multiple="multiple" id="profesiones" style="width: 100%;"></select>                                      
                                  </div>
                             </div><br>
                              <div class="col-sm-12 text-left">
                                {!!Form::submit('Guardar Profesiones',['id'=>'guardarprofesiones', 'class'=>'btn btn-primary'])!!} 
                            </div> 
                           <div class="col-sm-12">
                              <div class="form-group"><br><br> 
                                    <label class="lead text-muted pull-left black-600" style="display: block;">Agregue las Especialidades:  </label>
                                    <select name="especialidades[]" class="form-control" multiple="multiple" id="especialidades" style="width: 100%;"></select>                                      
                                </div>
                           </div>       
                            <div class="col-sm-12 text-left">
                                {!!Form::submit('Guardar Especialidades',['id'=>'guardarespecialidades', 'class'=>'btn btn-primary'])!!} 
                            </div>  
                        </div><br>   
                                               
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->                                 

<!-- modales-->
<!--MODAL DE LA CABECERA-->
<div class="modal fade modal-info in" id="portada" aria-hidden="true" aria-labelledby="logros" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-label="Cerrar" type="button" data-dismiss="modal">X</button>
                <h4 class="modal-title">Cambiar Foto de Portada</h4>
            </div>
            <div class="modal-body text-center">
                 {!!Form::open(['route'=>['portada'], 'method'=>'POST'])!!}            
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        <div class="row">
                             <div class="col-sm-4">
                                  <img id="portada1" class="cover-image" src="../../../archivos/PORTADAS/portada1.jpg" alt="portada1">
                            </div>
                             <div class="col-sm-4">                                
                                  <img id="portada2" class="cover-image" src="../../../archivos/PORTADAS/portada2.jpg" alt="portada2">
                            </div>
                             <div class="col-sm-4">
                                <img id="portada3" class="cover-image" src="../../../archivos/PORTADAS/portada3.png" alt="portada3">
                            </div>
                        </div><br>   
                         <div class="row">
                             <div class="col-sm-4">
                                  <img id="portada4" class="cover-image" src="../../../archivos/PORTADAS/portada4.jpg" alt="portada4">
                            </div>
                             <div class="col-sm-4">                                
                                  <img id="portada5" class="cover-image" src="../../../archivos/PORTADAS/portada5.jpg" alt="portada5">
                            </div>
                             <div class="col-sm-4">
                                <img id="portada6" class="cover-image" src="../../../archivos/PORTADAS/portada6.png" alt="portada6">
                            </div>
                        </div><br> 
                        <div class="form-group">
                             {!!Form::hidden('portadas','',['id'=>'portadas','class'=>'form-control'])!!}
                        </div>                           
                      <div class="col-sm-12 text-center">
                        {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} 
                      </div>
                {!!Form::close()!!}      
                            
            </div>
            <div class="modal-footer text-center">
            </div>
        </div>
    </div>
</div>

<!--MODAL DE SUBIR ARCHIVOS-->
<div class="modal fade modal-info in" id="subirarchivo" role="dialog" aria-hidden="true" aria-labelledby="subirarchivo" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
             <div class="modal-header">
                <button class="close" aria-label="Cerrar" type="button" data-dismiss="modal">X</button>
                <h4 class="modal-title">Subir Foto de Perfil</h4>
            </div>
            <div class="modal-body">
                {!!Form::open(['route'=>['subirarchivo'], 'method'=>'POST', 'enctype'=>'multipart/form-data'])!!}            
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">                    
                       <div class="form-group">
                          {!!Form::label('portada','Subir Imagen:')!!}
                          <input type="file" class="filestyle" name="path" id="path" accept="image/*" ></input>
                        </div>
                      <div class="col-sm-12 text-center">
                        {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!} 
                      </div>
                {!!Form::close()!!}       
            </div>
            <div class="modal-footer text-center">
            </div>
        </div>
    </div>
</div>


 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
    {!!Html::script('js/ayuda/usuario-tour.js')!!} 
        <script type="text/javascript">

////////////////////////////////////////////////////////////////////////////////////////////////////////
          //Autocompletado select2 para factor riesgos
          var pathespecialidades = "{{URL::route('especialidades')}}";
          // inicializamos el plugin
          $('#especialidades').select2({
              // Activamos la opcion "Tags" del plugin
              tags: true,
              tokenSeparators: [','],
              ajax: {
                  dataType: 'json',
                  url: pathespecialidades,
                  delay: 250,
                  data: function(params) {
                      return {
                          term: params.term
                      }
                  },
                  processResults: function (data, page) {
                    return {
                      results: data
                    };
                  },
              }
          });
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
          //Autocompletado select2 para factor riesgos
          var pathprofesiones = "{{URL::route('profesiones')}}";
          // inicializamos el plugin
          $('#profesiones').select2({
              // Activamos la opcion "Tags" del plugin
              tags: true,
              tokenSeparators: [','],
              ajax: {
                  dataType: 'json',
                  url: pathprofesiones,
                  delay: 250,
                  data: function(params) {
                      return {
                          term: params.term
                      }
                  },
                  processResults: function (data, page) {
                    return {
                      results: data
                    };
                  },
              }
          });
////////////////////////////////////////////////////////////////////////////////////////////////////////
        $(document).ready(function(){
            $('#portada1,#portada2,#portada3,#portada4,#portada5,#portada6').click(cambiaportada);
        })
         
        function cambiaportada(){
            if($(this).attr('alt')=='portada1'){   
                //$('#cabecera-perfil').css('background-image',url("../../../archivos/PORTADAS/portada1.jpg"))
                $('#cabecera-perfil').css('background-color','#3583CA');
                $(this).addClass("sombraportada");
                $('#portada2,#portada3,#portada4,#portada5,#portada6').removeClass("sombraportada");
                $("#portadas").val("portada1.jpg");
            }
            if($(this).attr('alt')=='portada2'){
                //$('#cabecera-perfil').css('background-image',url("../../../archivos/PORTADAS/portada2.jpg"))
                $('#cabecera-perfil').css('background-color','#3583CA');
                $(this).addClass("sombraportada");
                $('#portada1,#portada3,#portada4,#portada5,#portada6').removeClass("sombraportada");
                $("#portadas").val("portada2.jpg");
            }
            if($(this).attr('alt')=='portada3'){
                $('#cabecera-perfil').css('background-color','#3583CA');
                $(this).addClass("sombraportada");
                $('#portada1,#portada2,#portada4,#portada5,#portada6').removeClass("sombraportada");
                $("#portadas").val("portada3.png");
            }
            if($(this).attr('alt')=='portada4'){   
                $('#cabecera-perfil').css('background-color','#3583CA');
                $(this).addClass("sombraportada");
                $('#portada1,#portada2,#portada3,#portada5,#portada6').removeClass("sombraportada");
                $("#portadas").val("portada4.jpg");
            }
            if($(this).attr('alt')=='portada5'){
                $('#cabecera-perfil').css('background-color','#3583CA');
                $(this).addClass("sombraportada");
                $('#portada1,#portada2,#portada3,#portada4,#portada6').removeClass("sombraportada");
                $("#portadas").val("portada5.jpg");
            }
            if($(this).attr('alt')=='portada6'){
                $('#cabecera-perfil').css('background-color','#3583CA');
                $(this).addClass("sombraportada");
                $('#portada1,#portada2,#portada3,#portada4,#portada5').removeClass("sombraportada");
                $("#portadas").val("portada6.png");
            }
        }
////////////////////////////////////////////////////////////////////////////////////////////////////////


        $("#hospital_id").on('change', function () {
             var idhospitalperfil = $(this).val();
             $("#idhospitalperfil").val(idhospitalperfil);
        });

////////////////////////////////////////////////////////////////////////////////////////////////////////
         $("#guardarprofesiones").click( function () {

              var token = $("#token").val();
              var TableDataprofesiones = new Array();      
              var profesiones = $("#profesiones").select2('val');
              TableDataprofesiones.push({profesiones}); 

               var arrayprofesiones = {                   
                    TableDataprofesiones: TableDataprofesiones,
                };

                var route = "http://sinraim.omarboza.com/datosprofesionales";
                $.ajax({
                  url: route,
                  headers: {'X-CSRF-TOKEN': token},
                  type: 'POST',
                  data: JSON.stringify(arrayprofesiones),
                  contentType: "json; charset=utf-8",
                  processData: false,
                  

                  success:function(){
                    console.log(arrayprofesiones);
                    location.reload();
                    //$("#central").load('inc/gallery.php');
                    //$("#msj-success").fadeIn();
                  },
                  error:function(msj){
                    $("#msj-error").fadeIn();
                    console.log(arrayprofesiones);
                  }
                });
        });

////////////////////////////////////////////////////////////////////////////////////////////////////////
         $("#guardarespecialidades").click( function () {

              var token = $("#token").val();              
              var TableDataespecialidades = new Array();
              var especialidades = $("#especialidades").select2('val');
              TableDataespecialidades.push({especialidades}); 

               var arrayespecialidades = {                   
                    TableDataespecialidades: TableDataespecialidades,
                };

                var route = "http://sinraim.omarboza.com/datosespecialidades";
                $.ajax({
                  url: route,
                  headers: {'X-CSRF-TOKEN': token},
                  type: 'POST',
                  data: JSON.stringify(arrayespecialidades),
                  contentType: "json; charset=utf-8",
                  processData: false,
                  

                  success:function(){
                    console.log(arrayespecialidades);
                    location.reload();
                    //$("#central").load('inc/gallery.php');
                    //$("#msj-success").fadeIn();
                  },
                  error:function(msj){
                    $("#msj-error").fadeIn();
                    console.log(arrayespecialidades);
                  }
                });
        });
         

        </script>
      
    @endpush
@endsection
    
@stop