@extends('layouts.layout')

@section('title')
  Sinraim | Informes
@stop
@section('css')
<style>
    .content_wrapper{
        float: right;
    }
    .col-sm-8{
        float: right !important; 
    }   
</style>


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL DE INFORMES</h1>
        </header> <br>
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
             <div>
                  <div class="row">
                      <div class="col-sm-8">
                          <div id="graf1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                      </div><br>
                      <div class="col-sm-8">
                          <div id="graf2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                      </div>
                  </div><br>
                  <div class="row">
                      <div class="col-sm-8">
                          <div id="graf3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                      </div>
                      <div class="col-sm-8">
                          <div id="graf4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                      </div>
                  </div><br>
              </div>                                    
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

 <!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   


 @section('javascript')
    @section('scripts')   
    <!-- Scripts para ajax de ver usuarios -->
      {!!Html::script('js/graficos/highcharts.js')!!}
      {!!Html::script('https://code.highcharts.com/modules/data.js')!!}  
      {!!Html::script('https://code.highcharts.com/modules/drilldown.js')!!}  
      {!!Html::script('https://code.highcharts.com/highcharts-more.js')!!}     
      {!!Html::script('https://code.highcharts.com/modules/exporting.js')!!}  
      {!!Html::script('js/graficos/grafico.js')!!} 

    @endsection


  @section('footer')
    @push('scripts')
                
        <!-- Scripts para panel informes --> 
        {!!Html::script('js/ayuda/informes-tour.js')!!} 
     
    @endpush
@endsection
    
@stop