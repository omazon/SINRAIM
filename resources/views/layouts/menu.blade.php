<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title> SINRAIM | Notificación</title>

 

  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">
  
  <!-- Stylesheets -->
  {!!Html::style('global/css/bootstrap.min.css')!!}
  {!!Html::style('global/css/bootstrap-extend.min.css')!!}
  {!!Html::style('assets/css/site.min.css')!!}
  {!!Html::style('global/vendor/bootstrap-select/bootstrap-select.css')!!}

  <!-- Plugins -->
  {!!Html::style('global/css/skintools.min.css')!!}
  {!!Html::script('assets/js/sections/skintools.min.js')!!}
  {!!Html::style('global/vendor/animsition/animsition.css')!!}
  {!!Html::style('global/vendor/asscrollable/asScrollable.css')!!}
  {!!Html::style('global/vendor/switchery/switchery.css')!!}
  {!!Html::style('global/vendor/intro-js/introjs.css')!!}
  {!!Html::style('global/vendor/slidepanel/slidePanel.css')!!}
  {!!Html::style('global/vendor/flag-icon-css/flag-icon.css')!!}
  {!!Html::style('global/vendor/editable-table/editable-table.min.css?v2.2.0')!!}
  {!!Html::style('global/vendor/formvalidation/formValidation.css')!!}
  {!!Html::style('global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')!!}
  {!!Html::style('global/vendor/asspinner/asSpinner.css')!!}
  {!!Html::style('assets/examples/css/tables/datatable.css')!!}
  {!!Html::style('global/vendor/filament-tablesaw/tablesaw.css')!!}
  {!!Html::style('global/vendor/icheck/icheck.css')!!}
  {!!Html::style('global/vendor/webui-popover/webui-popover.css')!!}
  {!!Html::style('global/vendor/asscrollable/asScrollable.css')!!}
  {!!Html::style('assets/examples/css/advanced/scrollable.css')!!}

    <!-- Plugins For table -->
  {!!Html::style('global/vendor/datatables-bootstrap/dataTables.bootstrap.min.css?v2.2.0')!!}
  {!!Html::style('global/vendor/datatables-fixedheader/dataTables.fixedHeader.min.css?v2.2.0')!!}
  {!!Html::style('global/vendor/datatables-responsive/dataTables.responsive.min.css?v2.2.0')!!}
  {!!Html::style('assets/examples/css/tables/datatable.min.css?v2.2.0')!!}

  {!!Html::style('css/estilo.css')!!}

  <!-- login -->
  {!!Html::style('assets/examples/css/pages/login-v2.css')!!}

    <!-- Plugins For This Page -->
    {!!Html::style('../../global/vendor/blueimp-file-upload/jquery.fileupload.min.css?v2.2.0')!!}
    {!!Html::style('../../global/vendor/dropify/dropify.min.css?v2.2.0')!!}

  <!-- Fonts -->
  {!!Html::style('global/fonts/web-icons/web-icons.min.css')!!}
  {!!Html::style('global/fonts/brand-icons/brand-icons.min.css')!!}
  {!!Html::style('global/fonts/font-awesome/font-awesome.min.css')!!}
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  {!!Html::script('global/vendor/modernizr/modernizr.js')!!}
  {!!Html::script('global/vendor/breakpoints/breakpoints.js')!!}

  <!-- CSS para select2 factores de riesgos -->
  {!!Html::style('css/select2.min.css')!!}

  <!-- CSS para select2 factores de riesgos -->
  {!!Html::style('global/vendor/alertify-js/alertify.min.css?v2.2.0')!!}
  {!!Html::style('assets/examples/css/advanced/alertify.min.css?v2.2.0')!!}



  <script>
  Breakpoints();
  </script>
</head>
<body>
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon wb-more-horizontal" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
        <img class="navbar-brand-logo" src="images/login-images/logosinraimblanco.png" title="Remark">
        <span class="navbar-brand-text" src="images/login-images/logosinraimblanco.png"> SINRAIM</span>
      </div>
      <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
      data-toggle="collapse">
        <span class="sr-only">Toggle Search</span>
        <i class="icon wb-search" aria-hidden="true"></i>
      </button>
    </div>
    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
          <ul class="nav navbar-toolbar">
              <li class="hidden-float" id="toggleMenubar">
                  <a data-toggle="menubar" href="#" role="button">
                      <i class="icon hamburger hamburger-arrow-left">
                          <span class="sr-only">Toggle menubar</span>
                          <span class="hamburger-bar"></span>
                      </i>
                  </a>
              </li>
              <li class="hidden-xs" id="toggleFullscreen">
                  <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                      <span class="sr-only">Toggle fullscreen</span>
                  </a>
              </li>
              <li class="dropdown dropdown-fw dropdown-mega">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                     data-animation="fade" role="button">Créditos <i class="icon wb-chevron-down-mini" aria-hidden="true"></i></a>
                  <ul class="dropdown-menu" role="menu">
                      <li role="presentation">
                          <div class="mega-content">
                              <div class="row">                     
                                  <div class="col-sm-8">                      
                                      <!-- Accordion -->
                                      <div class="panel-group" id="siteMegaAccordion" aria-multiselectable="true"
                                           role="tablist">
                                             <div class="panel">
                                              <div class="panel-heading" id="siteMegaAccordionHeadingTwo" role="tab">
                                                  <a class="panel-title collapsed" data-toggle="collapse" href="#siteMegaCollapseTwo"
                                                     data-parent="#siteMegaAccordion" aria-expanded="false"
                                                     aria-controls="siteMegaCollapseTwo">
                                                      Santiago Nicolás Aguilar Silva
                                                  </a>
                                              </div>
                                              <div class="panel-collapse collapse" id="siteMegaCollapseTwo" aria-labelledby="siteMegaAccordionHeadingTwo"
                                                   role="tabpanel">
                                                  <div class="panel-body">
                                                      <div class="col-sm-6">
                                                          <img width="150" height="150" class="img-rounded" alt="..." src="../global/portraits/santiagofondoblanco.png">
                                                      </div>
                                                      <div class="col-sm-6">
                                                          <ul>
                                                              <li>SCRUM Master</li>
                                                              <li>Encargado de Prototipado</li>
                                                              <li>Encargado de Desarrollo</li>
                                                              <li>User Xperience</li>
                                                              <li>UI Design</li>
                                                              <li>Analista Programador</li>                                            <li>Administrador de Base de Datos</li>
                                                          </ul>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>       
                                          <div class="panel">
                                              <div class="panel-heading" id="siteMegaAccordionHeadingOne" role="tab">
                                                  <a class="panel-title" data-toggle="collapse" href="#siteMegaCollapseOne" data-parent="#siteMegaAccordion"
                                                     aria-expanded="false" aria-controls="siteMegaCollapseOne">
                                                      Omar Antonio Boza Bonilla
                                                  </a>
                                              </div>
                                              <div class="panel-collapse collapse" id="siteMegaCollapseOne" aria-labelledby="siteMegaAccordionHeadingOne"
                                                   role="tabpanel">
                                                  <div class="panel-body">
                                                      <div class="col-sm-6">
                                                          <img width="150" height="150" class="img-rounded" alt="..." src="../img/omarboza@gmail.com/boza.jpg">
                                                      </div>
                                                      <div class="col-sm-6">
                                                          <ul>
                                                              <li>Encargado de Desarrollo</li>
                                                              <li>Beta Tester</li>
                                                              <li>Analista de Requirimientos</li>
                                                              <li>Analista Programador</li>
                                                          </ul>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                                         
                                      </div>
                                      <!-- End Accordion -->

                                  </div>
                                  <div class="col-sm-4">
                                      <img class="img-responsive" alt="..." src="../global/portraits/UNIlogo.jpg">
                                  </div>
                              </div>
                          </div>
                      </li>
                  </ul>
              </li>
          </ul>
        <!-- End Navbar Toolbar -->
        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
          <li class="dropdown">
            <a class="navbar-avatar dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="scale-up" role="button">{!!Auth::user()->name!!}
              <span class="avatar avatar-online">
                <img src="../../../archivos/{{Auth::user()->path}}" alt="...">
                <i></i>
              </span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li role="presentation">
                    <a href="/perfilusuario" role="menuitem"><i class="icon wb-user" aria-hidden="true"></i> Perfil</a>
                </li>
                <li role="presentation">
                    <a href="/configuracion" role="menuitem"><i class="icon wb-settings" aria-hidden="true"></i> Configuraciones</a>
                </li>
                <li class="divider" role="presentation"></li>
                <li role="presentation">
                    <a href="/logout" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Cerrar Sesión</a>
                </li>
            </ul>
          </li>
          <li class="dropdown">
            <a data-toggle="dropdown" href="javascript:void(0)" title="Notifications" aria-expanded="false"
            data-animation="scale-up" role="button">
              <i class="icon wb-bell" aria-hidden="true"></i>
              <span class="badge badge-success up">Nuevo</span>                 
            </a>
            <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                <li class="dropdown-menu-header" role="presentation">
                  <h5>NOTIFICACIONES</h5>
                </li>

                 @foreach ($notificacionesmenus as $notificacionesmenu)
                    @if ($notificacionesmenu->created_at == $notificacionesmenu->updated_at)
                      <li class="list-group" role="presentation">
                        <div data-role="container">
                          <div data-role="content">
                              <a class="list-group-item" href="javascript:void(0)" role="menuitem">
                                <div class="media">                                 
                                    <div class="media-body">
                                      
                                    </div>
                                </div>
                              </a>  
                          </div>
                        </div>
                      </li>                    
                     @else
                        <li class="list-group" role="presentation">
                          <div data-role="container">
                            <div data-role="content">
                                <a class="list-group-item" href="{{ URL::route('notificacion.show', $notificacionesmenu->id) }}" role="menuitem">
                                  <div class="media">
                                      <div class="media-left padding-right-10">
                                          @if ($notificacionesmenu->estado === "PROBADA")
                                              <span class="label label-danger" style="font-size: 100%">{{$notificacionesmenu->estado}}</span>
                                          @elseif ($notificacionesmenu->estado === "PROBABLE")
                                              <span class="label label-warning" style="font-size: 100%">{{$notificacionesmenu->estado}}</span>
                                          @elseif ($notificacionesmenu->estado === "POSIBLE")
                                              <span class="label label-info" style="font-size: 100%">{{$notificacionesmenu->estado}}</span>
                                          @else
                                              <span class="label label-success" style="font-size: 100%">{{$notificacionesmenu->estado}}</span>
                                          @endif                                          
                                      </div>
                                      <div class="media-body">
                                        <h6 class="media-heading">{{$notificacionesmenu->observaciones}}</h6>
                                        <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">{{date('F j, Y, g:i a', strtotime($notificacionesmenu->updated_at))}}</time>
                                      </div>
                                  </div>
                                </a>  
                            </div>
                          </div>
                        </li>                      
                  @endif                                               
                @endforeach 
            </ul>
          </li>
        </ul>
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->
      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon wb-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Search...">
              <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu">
            <li class="site-menu-category">General</li>
            <li class="site-menu-item has-sub">
                <a href="/dashboard">
                    <i class="site-menu-icon fa-dashboard" style="font-size: 25px;" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>        
              </a>              
            </li>  
            @if(Auth::user()->roles_id == 1 || Auth::user()->roles_id == 2) 
                <li class="site-menu-item has-sub active open">
                  <a href="javascript:void(0)">
                      <i class="site-menu-icon wb-layout" style="font-size: 25px;" aria-hidden="true"></i>
                    <span class="site-menu-title">Notificación</span>
                    <span class="site-menu-arrow"></span>
                  </a>
                  <ul class="site-menu-sub">
                    <li class="site-menu-item">
                      <a class="animsition-link" href="/paciente">
                        <span class="site-menu-title">Enviar</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="/notificacion">
                        <span class="site-menu-title">Ver</span>
                      </a>
                    </li>
                    <li class="site-menu-item">
                      <a class="animsition-link" href="/evaluaralgoritmoprr">
                        <span class="site-menu-title">Evaluar</span>
                      </a>
                      </li>            
                  </ul>
                </li>
            @endif
            <li class="site-menu-item has-sub">
                <a href="/perfilusuario">
                    <i class="site-menu-icon wb-user" style="font-size: 25px;" aria-hidden="true"></i>
                <span class="site-menu-title">Mi Perfil</span>        
              </a>              
            </li> 
            @if(Auth::user()->roles_id == 1 || Auth::user()->roles_id == 3)  
            <li class="site-menu-item has-sub">
              <a href="/informes">
                  <i class="site-menu-icon fa-pie-chart" style="font-size: 25px;" aria-hidden="true"></i>
                <span class="site-menu-title">Informes</span>                
              </a>              
            </li>
            @endif
            <li class="site-menu-item has-sub">
              <a href="/atcmeddra">
                  <i class="site-menu-icon wb-search" style="font-size: 25px;" aria-hidden="true"></i>
                <span class="site-menu-title">ATC /MEDDRA</span>                
              </a>              
            </li>
             @if(Auth::user()->roles_id == 1) 
            <li class="site-menu-item has-sub">
              <a href="/usuario/create">
                <i class="site-menu-icon fa-user-md" style="font-size: 25px;" aria-hidden="true"></i>
                <span class="site-menu-title">Admin. Médicos</span>                
              </a>              
            </li>
            <li class="site-menu-item has-sub">
              <a href="/configuracion">
                  <i class="site-menu-icon fa-gear" style="font-size: 25px;" aria-hidden="true"></i>
                <span class="site-menu-title">Configuración</span>                
              </a>              
            </li>
            @endif           
          </ul>          
        </div>
      </div>
    </div>
    <div class="site-menubar-footer">
        <a href="/dashboard" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Dashboard">
          <span class="icon fa-dashboard" aria-hidden="true"></span>
        </a>      
        <a href="/paciente" data-placement="top" data-toggle="tooltip" data-original-title="Enviar Notificacion">
            <span class="icon wb-layout" aria-hidden="true"></span>
        </a>
        <a href="../" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Cerrar Sesión">
            <span class="icon wb-power" aria-hidden="true"></span>
        </a>
    </div>
  </div>
  <div class="site-gridmenu">
    <div>
      <div>
        <ul>
          <li>
            <a href="../apps/mailbox/mailbox.html">
              <i class="icon wb-envelope"></i>
              <span>Mailbox</span>
            </a>
          </li>
          <li>
            <a href="../apps/calendar/calendar.html">
              <i class="icon wb-calendar"></i>
              <span>Calendar</span>
            </a>
          </li>
          <li>
            <a href="../apps/contacts/contacts.html">
              <i class="icon wb-user"></i>
              <span>Contacts</span>
            </a>
          </li>
          <li>
            <a href="../apps/media/overview.html">
              <i class="icon wb-camera"></i>
              <span>Media</span>
            </a>
          </li>
          <li>
            <a href="../apps/documents/categories.html">
              <i class="icon wb-order"></i>
              <span>Documents</span>
            </a>
          </li>
          <li>
            <a href="../apps/projects/projects.html">
              <i class="icon wb-image"></i>
              <span>Project</span>
            </a>
          </li>
          <li>
            <a href="../apps/forum/forum.html">
              <i class="icon wb-chat-group"></i>
              <span>Forum</span>
            </a>
          </li>
          <li>
            <a href="../index.html">
              <i class="icon wb-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>   

      <section class="contenedor">
           @yield('content')
        </section>         
  <!-- End Page -->
  <!-- Footer-->

  <!-- Core  -->
  {!!Html::script('global/vendor/jquery/jquery.js')!!}
  {!!Html::script('global/vendor/bootstrap/bootstrap.js')!!}
  {!!Html::script('global/vendor/animsition/animsition.js')!!}
  {!!Html::script('global/vendor/asscroll/jquery-asScroll.js')!!}
  {!!Html::script('global/vendor/mousewheel/jquery.mousewheel.js')!!}
  {!!Html::script('global/vendor/asscrollable/jquery.asScrollable.all.js')!!}
  {!!Html::script('global/vendor/ashoverscroll/jquery-asHoverScroll.js')!!}


  <!-- Plugins -->
  {!!Html::script('global/vendor/switchery/switchery.min.js')!!}
  {!!Html::script('global/vendor/intro-js/intro.js')!!}
  {!!Html::script('global/vendor/screenfull/screenfull.js')!!}
  {!!Html::script('global/vendor/slidepanel/jquery-slidePanel.js')!!}
  {!!Html::script('global/vendor/formvalidation/formValidation.js')!!}
  {!!Html::script('global/vendor/formvalidation/framework/bootstrap.js')!!}
  {!!Html::script('global/vendor/matchheight/jquery.matchHeight-min.js')!!}
  {!!Html::script('global/vendor/jquery-wizard/jquery-wizard.js')!!}




  <!-- Scripts -->
  {!!Html::script('global/js/core.js')!!}
  {!!Html::script('assets/js/site.js')!!}
  {!!Html::script('assets/js/sections/menu.js')!!}
  {!!Html::script('assets/js/sections/menubar.js')!!}
  {!!Html::script('assets/js/sections/gridmenu.js')!!}
  {!!Html::script('assets/js/sections/sidebar.js')!!}
  {!!Html::script('global/js/configs/config-colors.js')!!}
  {!!Html::script('global/js/components/asscrollable.js')!!}
  {!!Html::script('global/js/components/animsition.js')!!}
  {!!Html::script('global/js/components/slidepanel.js')!!}
  {!!Html::script('global/js/components/switchery.js')!!}
  {!!Html::script('global/js/components/jquery.bootstrap.wizard.min.js')!!}
  {!!Html::script('global/js/components/matchheight.js')!!}
  {!!Html::script('global/vendor/bootstrap-select/bootstrap-select.js')!!}
  {!!Html::script('global/js/components/bootstrap-select.js')!!}
  {!!Html::script('global/js/components/bootstrap-datepicker.js')!!}
  {!!Html::script('global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')!!}  
  {!!Html::script('global/js/components/asspinner.js')!!}
  {!!Html::script('global/vendor/asspinner/jquery-asSpinner.min.js')!!}
  {!!Html::script('global/vendor/icheck/icheck.min.js')!!}


    <!-- Plugins -->
  {!!Html::script('global/vendor/datatables/jquery.dataTables.js')!!}
  {!!Html::script('global/vendor/datatables-fixedheader/dataTables.fixedHeader.js')!!}
  {!!Html::script('global/vendor/datatables-bootstrap/dataTables.bootstrap.js')!!}
  {!!Html::script('global/vendor/datatables-responsive/dataTables.responsive.js')!!}
  {!!Html::script('global/vendor/datatables-tabletools/dataTables.tableTools.js')!!}
  {!!Html::script('global/js/components/datatables.js')!!}
  {!!Html::script('assets/examples/js/tables/datatable.js')!!}
  {!!Html::script('global/vendor/asrange/jquery-asRange.min.js')!!}
  {!!Html::script('global/vendor/bootbox/bootbox.js')!!}
  {!!Html::script('global/vendor/filament-tablesaw/tablesaw.js')!!}
  {!!Html::script('global/vendor/filament-tablesaw/tablesaw-init.js')!!}
  {!!Html::script('global/js/components/icheck.js')!!}
  {!!Html::script('global/vendor/webui-popover/jquery.webui-popover.min.js')!!}
  {!!Html::script('global/js/components/webui-popover.js')!!}

  <!-- agregar filas dinamicamente ajax -->
  {!!Html::script('js/table_script.js')!!}

  <!-- Scripts para autocomplete -->
  {!!Html::script('js/bootstrap3-typeahead.min.js')!!}

  <!-- Scripts para Select2 factores de riesgos -->
  {!!Html::script('js/select2.min.js')!!}

  <!-- Scripts para validacion de formularios -->
  {!!Html::script('assets/examples/js/forms/validation.js')!!}
  {!!Html::script('js/es_ES.js')!!}

  <!-- Scripts para TOUR DE AYUDA -->
  {!!Html::script('js/ayuda/site.js')!!}

  <!-- Plugins For This Page -->
  {!!Html::script('global/vendor/alertify-js/alertify.js')!!}
  {!!Html::script('global/js/components/alertify-js.min.js')!!}

  @section('scripts')  
  @show 
   <script>
  $(window).load(function(){        
    //$('.cssload-preloader').css('display','none');    
  });
</script>
</body>
</html>