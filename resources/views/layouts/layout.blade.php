<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
 
  <title>@yield('title')</title>
  {!!Html::style('css/loader.css')!!}
  <div class="cssload-preloader">
  <div class="cssload-preloader-box">   <div>S</div>    <div>I</div>    <div>N</div>    <div>R</div>    <div>A</div>    <div>I</div>    <div>M</div></div>
</div>
  @include('layouts.menu')
 
</head>
<body>
  @yield('body')  
  <script>$(window).load(function(){
        $('.cssload-preloader').css('display','none');
    });</script>  
</body>
</html>