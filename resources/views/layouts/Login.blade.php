<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>SINRAIM | Login</title>
  <link rel="apple-touch-icon" href="images/login-images/apple-touch-icon.png">
  <link rel="shortcut icon" href="images/login-images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="global/css/bootstrap.min.css">
  <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="assets/css/site.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="assets/examples/css/pages/login-v2.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="global/vendor/modernizr/modernizr.js"></script>
  <script src="global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="page-login-v2 layout-full page-dark">
 <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <!-- Page -->
  <div class="page animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">      
      <div class="page-login-main">
      <div class="panel">                           
          <div class="panel-body">
              <div class="row"> 
                  <div class="col-md-5 col-md-offset-2">
                      <img class="pull-center" src="images/login-images/logosinraim.png" alt="..."> 
                  </div>  
              </div> 



               {!!Form::open(['route'=>'store', 'method'=>'POST'])!!}
               <div class="row row-lg">
                  <div class="form-horizontal">   

                    <div class="form-group">
                        <h3 class="font-size-24">Iniciar Sesión</h3>     
                        <p>Inicie sesión para poder realizar notificaciones online</p>
                        @include('alerts.errors')
                        @include('alerts.request')
                        {!!Form::label('correo','Correo:')!!}
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="icon wb-envelope" aria-hidden="true"></i>
                          </span>
                          {!!Form::email('email',null,['id'=>'email','class'=>'form-control', 'placeholder'=>'Ingrese el correo electronico institucional'])!!}
                        </div>  
                    </div>  
                    <div class="form-group">
                        {!!Form::label('contrasena','Contraseña:')!!}
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="icon wb-lock" aria-hidden="true"></i>
                          </span>
                          {!!Form::password('password',['id'=>'password','class'=>'form-control'])!!}
                        </div>                          
                    </div> 
                    <div class="form-group">
                            <div class="checkbox-custom checkbox-inline checkbox-primary pull-left">
                              <input name="checkbox" id="remember" type="checkbox">
                              <label for="inputCheckbox">Recordarme</label>                          
                            </div> 
                            <a class="pull-right" href="/password/email">Olvidé mi contraseña</a>
                      </div>
                    
                      {!!Form::submit('Iniciar Sesión',['class'=>'btn btn-primary'])!!}
                  </div>
                </div>
                     
               {!!Form::close()!!} 

  
          </div>
      </div>
        <footer class="page-copyright">
          <p>SINRAIM MINSA</p>
          <p>© 2015. All RIGHT RESERVED.</p>
          <div class="social">
            <a class="btn btn-icon btn-round social-twitter" href="javascript:void(0)">
              <i class="icon bd-twitter" aria-hidden="true"></i>
            </a>
            <a class="btn btn-icon btn-round social-facebook" href="javascript:void(0)">
              <i class="icon bd-facebook" aria-hidden="true"></i>
            </a>
            <a class="btn btn-icon btn-round social-google-plus" href="javascript:void(0)">
              <i class="icon bd-google-plus" aria-hidden="true"></i>
            </a>
          </div>
        </footer>
  <!-- End Page -->
  <!-- Core  -->
  <script src="global/vendor/jquery/jquery.js"></script>
  <script src="global/vendor/bootstrap/bootstrap.js"></script>
  <script src="global/vendor/animsition/animsition.js"></script>
  <script src="global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <!-- Plugins -->
  <script src="global/vendor/switchery/switchery.min.js"></script>
  <script src="global/vendor/intro-js/intro.js"></script>
  <script src="global/vendor/screenfull/screenfull.js"></script>
  <script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <!-- Scripts -->
  <script src="global/js/core.js"></script>
  <script src="assets/js/site.js"></script>
  <script src="assets/js/sections/menu.js"></script>
  <script src="assets/js/sections/menubar.js"></script>
  <script src="assets/js/sections/gridmenu.js"></script>
  <script src="assets/js/sections/sidebar.js"></script>
  <script src="global/js/configs/config-colors.js"></script>
  <script src="assets/js/configs/config-tour.js"></script>
  <script src="global/js/components/asscrollable.js"></script>
  <script src="global/js/components/animsition.js"></script>
  <script src="global/js/components/slidepanel.js"></script>
  <script src="global/js/components/switchery.js"></script>
  <script src="global/js/components/jquery-placeholder.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>
</html>
      