@extends('layouts.layout')

@section('title')
  Notificacion | Ayuda
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL DE AYUDA </h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
            
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

 <!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   


 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
      
    @endpush
@endsection
    
@stop

