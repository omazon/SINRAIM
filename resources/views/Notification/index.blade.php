@extends('layouts.layout')

@section('title')
  Notificacion | Ver Notificacion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL PARA VER NOTIFICACIONES ENVIADAS</h1>
           
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">                 
            <!--TABLA DE NOTIFICACION--> 
             @include('alerts.errors')    
              <table class="table table-hover dataTable table-striped  text-center" id="vernotificacion">
                  <thead>
                      <tr>
                          <th>Número</th>
                          <th>Insumo Medico</th>
                          <th>Reaccion Adversa</th>
                          <th id="estadonotiver">Estado</th>
                          <th id="accion">Accion</th>
                      </tr>
                  </thead>   
                  <tbody >                           
                          @foreach ($notifications as $notification)
                                <tr>  
                                  <td>{{$notification->idnotificacion}}</td>            
                                  <td>{{$notification->nombreinsumo}}</td>   
                                  <td>{{$notification->nombrereaccion}}</td>    
                                  <td>
                                        @if ($notification->estado === "PROBADA")
                                            <span class="label label-danger" style="font-size: 100%">{{$notification->estado}}</span>
                                        @elseif ($notification->estado === "PROBABLE")
                                            <span class="label label-warning" style="font-size: 100%">{{$notification->estado}}</span>
                                        @elseif ($notification->estado === "POSIBLE")
                                            <span class="label label-info" style="font-size: 100%">{{$notification->estado}}</span>
                                        @else
                                            <span class="label label-success" style="font-size: 100%">{{$notification->estado}}</span>
                                        @endif                                        
                                  </td>  
                                  <td style="width : 150px;">
                                     <a  href="{{ URL::route('notificacion.show', $notification->idnotificacion) }}" class="btn btn-primary" role="button">Ver</a>
                                     <a  href="{{ URL::route('evaluarnotificacionind', $notification->idnotificacion) }}" class="btn btn-success" role="button">Evaluar</a>
                                  </td>                                            
                                </tr> 
                          @endforeach 
                    </tbody>
              </table></br><!-- TABLA DE BASE DE DATOS PARAVER NOTIFICACION ENVIADA -->
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   



 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
      <script type="text/javascript">  
            $("#vernotificacion").DataTable({
              select:true,
              "paging":true,
              "bProcessing":true,

            });
      
      </script>
      {!!Html::script('js/ayuda/vernotificacion-tour.js')!!} 
    @endpush
@endsection
    
@stop