@extends('layouts.layout')

@section('title')
  Notificacion | Evaluar Notificacion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL PARA EVALUAR NOTIFICACIONES ENVIADAS</h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
            {!!link_to('/notificacion', $title='Regresar a Notificacion', $attributes = ['class'=>'btn btn-success pull-left'], $secure = null)!!} <br><br>           
            <h4 class="text-center">Puede filtrar la evaluación eligiendo los medicamentos y las reacciones adversas guardadas en esta notificación, así podrá saber los posibles cruces y la posibilidad de causa de la reacción sobre el medicamento</h4><br>
            <div class="alert alert-warning fade in">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Algoritmo PRR</strong> Es Un potente algoritmo reconocido mundialmente, que toma como referencia los medicamentos introducidos en la notificación y las reacciones adversas de la misma notificación y los compara con el resto de información en la base de datos que ayudarán al evaluador a sacar una conclusión basada en datos estadisticos.
            </div>
             {!!Form::open(['route'=>['evaluarnotificacionind', $idnotifications], 'method'=>'POST'])!!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="control-label" for="">Insumos Medicos Notificados</label>
                        {!!Form::select('insumoarray', $insumoarray, null, ['name'=>'insumoarray[]', 'id' => 'insumoarray', 'class'=>'form-control', 'onchange'=>'obteneridinsumoarray()'] )!!}          
                    </div> 
                     <div class="col-sm-12">
                         <div class="form-group">
                                 {!!Form::hidden('idinsumoarray',null,['id'=>'idinsumoarray', 'class'=>'form-control'])!!}
                         </div>
                    </div><br>
                    <div class="col-sm-12">
                        <label class="control-label" for="">Reacciones Adversas Notificadas</label>
                        {!!Form::select('reaccionarray', $reaccionarray, null, ['name'=>'reaccionarray[]', 'id' => 'reaccionarray', 'class'=>'form-control', 'onchange'=>'obteneridreaccionarray()'] )!!}          
                    </div>
                    <div class="col-sm-12">
                         <div class="form-group">
                                 {!!Form::hidden('idreaccionarray',null,['id'=>'idreaccionarray', 'class'=>'form-control'])!!}
                         </div>
                    </div><br>
                    <div class="col-sm-12 text-center">
                        {!!Form::submit('Evaluar',['class'=>'btn btn-primary','id'=>'btn-submit'])!!} 
                    </div>  
                </div><br>                  
             {!!Form::close()!!} 
            
             <div class="row">
                <h4 class="text-center">Resultados Algoritmo PRR</h4>
                <br><br>  
                 <table class="espacio ">
                         <tr>
                              <td>
                                  <label class="lead text-muted " style="display: block; color:#3583CA; font-weight: bold;">Insumo Medico Evaluado:  </label>
                              </td>
                              <td>
                                  <h4 class="panel-title ">{{ $nombreinsumoeval }}</h4>
                              </td>
                         </tr>   
                         <tr>
                              <td>
                                  <label class="lead text-muted " style="display: block; color:#3583CA; font-weight: bold;">Reaccion Adversa Evaluada:  </label>
                              </td>
                              <td>
                                  <h4 class="panel-title ">{{ $nombrereaccioneval }}</h4>
                              </td>
                         </tr>     
                  </table><br><br>
                 <div class="col-sm-6 ">           
                          <div class="widget-content">
                                <div class="bg-blue-600 white padding-10">
                                    <div class="font-size-20 text-nowrap text-center">
                                        TOTAL DE NOTIFICACIONES EXISTENTES
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $notifications }}</label>
                                    </div>            
                                </div>
                          </div>          
                  </div> 
                  <div class="col-sm-6 ">          
                          <div class="widget-content">
                              <div class="bg-orange-600 white padding-10">
                                   <div class="font-size-18 text-nowrap text-center">
                                        TOTAL DE NOTIFICACIONES POR INSUMO MEDICO
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $totalnotinsumo }}</label>
                                    </div>                                              
                              </div>
                        </div>          
                   </div>
            </div><br>
            <div class="row"> 
                   <div class="col-sm-6 ">          
                          <div class="widget-content">
                                <div class="bg-red-600 white padding-10">
                                     <div class="font-size-18 text-nowrap text-center">
                                        TOTAL DE NOTIFICACIONES POR REACCION ADVERSA
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $totalnotireaccion }}</label>
                                    </div>    
                                </div>
                          </div>          
                  </div>   
                  <div class="col-sm-6 ">              
                          <div class="widget-content">
                                <div class="bg-green-600 white padding-10">
                                     <div class="font-size-18 text-nowrap text-center">
                                        TOTAL DE COINCIDENCIAS INSUMO Y REACCION
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $totalcoincidencia }}</label>
                                    </div>          
                                </div>
                          </div>          
                  </div> 
             </div><br>
                      
              <div class="row">     
                  <h4 class="text-center">Resultados Algoritmo Lassagna</h4>
                   <div class="col-sm-12 ">             
                          <div class="widget-content">
                                <div class="bg-blue-600 white padding-30">
                                    <div class="font-size-30 text-nowrap">
                                        ESTADO DE LA NOTIFICACION
                                        <span class="pull-right font-size-30 text-nowrap">
                                            <label>"{{ $estadonotificacion }}"</label>
                                        </span>
                                    </div>            
                                </div>
                          </div>          
                  </div> 
              </div><br>
            {!!Form::open(['route'=>['guardarestadonotificacionind', $idnotifications], 'method'=>'POST'])!!}  
                 
                  <h5 class="text-center">Estado de la Notificación</h5>
                   <div class="form-group">
                      <label class="control-label" for="">Estado Final de la Notificación</label><br>  
                      {!!Form::select('estadofinalnoti', 
                          ['DUDOSA' => 'DUDOSA', 
                          'POSIBLE' => 'POSIBLE',
                           'PROBABLE' => 'PROBABLE', 
                           'PROBADA' => 'PROBADA'], 
                           null, ['name' => 'estadofinalnoti[]', 'id' => 'estadofinalnoti',  'class' => 'form-control'])!!}     
                  </div>
                  <h5>Motivo por el cual afirma el estado anterior de la notificación</h5>
                   <div class="form-group">
                       {!!Form::textarea('motivocambioestado',null,['name'=>'motivocambioestado[]', 'id'=>'motivocambioestado', 'class'=>'form-control', 'style'=>'resize:none'])!!}
                    </div>   
                  <div class="col-sm-12 text-center">
                        {!!Form::submit('Guardar',['class'=>'btn btn-primary','id'=>'btn-submit-save'])!!} 
                    </div>
            {!!Form::close()!!}                                 
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   


 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
    {!!Html::script('js/ayuda/evaluar-tour.js')!!} 
        <script type="text/javascript">
         
            insumo = document.getElementById("insumoarray").value;
            document.getElementById("idinsumoarray").value = insumo;

            function obteneridinsumoarray() {              
              insumo = document.getElementById("insumoarray").value;
              document.getElementById("idinsumoarray").value = insumo;
            }

            reaccion = document.getElementById("reaccionarray").value;
              document.getElementById("idreaccionarray").value = reaccion;
            function obteneridreaccionarray() {
              
              reaccion = document.getElementById("reaccionarray").value;
              document.getElementById("idreaccionarray").value = reaccion;
            }


        </script>
      
    @endpush
@endsection
    
@stop