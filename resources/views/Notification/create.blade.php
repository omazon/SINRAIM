
@extends('layouts.layout')

@section('title')
  SINRAIM | Notificacion
@stop
@section('css')

@section('body') 


<div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL ENVIO DE NOTIFICACIONES POR PACIENTE</h1>                    
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">      
				<!-- PANEL ENVIAR NOTIFICACION --> 	          
	          {!!link_to('/paciente', $title = 'Regresar a Paciente', $attributes = ['class'=>'btn btn-success pull-left'], $secure = null)!!}
            <!-- FORM  DE VALIDACION--> 
            <form id="validacionotificacion">   
    	          <div class="panel" id="exampleWizardForm"> 
                    <span class="text-center blue-800 font-size-18"><strong>Envio de Notificaciones</strong> Usando el botón <strong> Click aqui para agregar</strong> puede agregar tantos insumos medicos como sean necesarios, y asi sucesivamente con reacciones adversas hasta completar la notificacion</span> <br>                             
    	                <div class="panel-body">
    	                     <!--<div class="pearls row" data-plugin="matchHeight" data-by-row="true" role="tablist">

    	                        <div class="pearl col-xs-12 col-sm-4 current" data-target="#exampleAccount" role="tab">
    	                          <div class="pearl-icon"><i class="icon wb-rubber" aria-hidden="true"></i></div>
    	                          <span class="pearl-title">Insumos Médicos</span>
    	                        </div>

    	                        <div class="pearl col-xs-12 col-sm-4" data-target="#exampleBilling2" role="tab">
    	                          <div class="pearl-icon"><i class="icon wb-link-broken" aria-hidden="true"></i></div>
    	                          <span class="pearl-title">Reacción Adversa</span>
    	                        </div>
    	                       
    	                        <div class="pearl col-xs-12 col-sm-4" data-target="#exampleGetting" role="tab">
    	                          <div class="pearl-icon"><i class="icon wb-check" aria-hidden="true"></i></div>
    	                          <span class="pearl-title">Confirmación</span>
    	                        </div>
    	                    </div>-->                            
                          <ul class="nav nav-pills">
                              <li id="insumo-medico" class="active"><a href="#exampleAccount" data-toggle="tab">Insumos Médicos</a></li>
                              <li id="reaccion-adversa"><a href="#exampleBilling2" data-toggle="tab">Reacción Adversa</a></li>
                              <li id="confi"><a href="#exampleGetting" data-toggle="tab">Confirmación</a></li>
                          </ul><br><br>


                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    	                      <!-- CONTENIDO DEL WIZARD-->
    	                      <div class="tab-content">                          
    	                           <!--INSUMOS MEDICOS-->               
    	                            <div class="tab-pane active" id="exampleAccount" role="tabpanel">
                                      <div class="row"> 
                                          <div class="col-sm-12 text-center">
                                              <h5>En el Paso 1 debes agregar los Insumos Medicos que consumes y sospechas causaron la reaccion adversa.</h5></br>   
                                          </div>                   
                                          <div class="col-xs-12 col-lg-12 text-center">
                                                <h3>Agregar Insumos Médicos</h3>
                                                <div class="popover-primary" data-title="Ingresar Insumos Médicos" data-content="Aca debe agregar los insumos médicos sospechos como los que consume normalmente. Click aqui para agregar" data-trigger="hover" data-placement="top" data-toggle="popover">
                                                    <button id="add-insumo-medico" type="button" class="btn btn-primary" data-toggle="modal" data-target="#OtrosInsumos" onclick="vaciarform('validacionotificacion');"> Click aqui para agregar</button>
                                                </div>
                                          </div>
                                     </div><br><br>   	                                     

                                      <!-- MODAL PARA AGREGAR NUEVO INSUMO MEDICO -->
                                      <div class="modal fade modal-primary modal-super-scaled" id="OtrosInsumos" aria-hidden="true" aria-labelledby="OtrosInsumos" role="dialog" tabindex="-1">
                                          <div class="modal-dialog modal-center modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>   
                                                        <h4 class="modal-title">Agregue la Información del Insumo Médico</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                      <!--Form que  permite el almacenamiento en DOM-->    
                                                       <div id="addinsumo">                             
                                                             <div class="panel-group panel-group-continuous margin-2" id="AcordionInternoInsumoMedico" aria-multiselectable="true" role="tablist">
                                                               @include('alerts.errors')
                                                               <!--cuerpo del modal--> 
                                                               <h5>Agregar Insumo Médico</h5>
                                                                  <!--Codigo Cabecera UNO-->
                                                                  <div class="panel bg-blue-grey-200">
                                                                     <div class="panel-heading" id="CabeceraSegunda" role="tab">
                                                                         <a href="#AcordionUno" data-parent="#AcordionInternoInsumoMedico" data-toggle="collapse"  aria-controls="AcordionUno" aria-expanded="false" class="panel-title collapsed" data-toggle="tooltip" title="Primer paso, Agregando datos del insumo médico">
                                                                             <i class="md-place"></i>Datos Indispensable del Insumo Médico
                                                                         </a>
                                                                     </div>
                                                                     <div class="panel-collapse collapse" id="AcordionUno" aria-labelledby="CabeceraSegunda" role="tabpanel">
                                                                         @include('Notification.form.medicamentoform1')  
                                                                     </div><!--FIN ACCORDION--> 
                                                                  </div><br><!--FIN PANEL BLUE GREY-->                                      
                                                                  <!--final cabecera uno-->                   
                                                                 <!--codigo cabecera dos-->
                                                                 <div class="panel bg-blue-grey-200">                                        
                                                                     <div class="panel-heading" id="CabeceraPrimero" role="tab">
                                                                         <a href="#AcordionDos" data-parent="#AcordionInternoInsumoMedico" data-toggle="collapse" aria-controls="AcordionDos" aria-expanded="false" class="panel-title collapsed" data-toggle="tooltip" title="Otros datos sobre el medicamento">
                                                                             <i class="md-palette"></i>Otros Datos
                                                                         </a>
                                                                     </div>
                                                                     <div class="panel-collapse collapse" id="AcordionDos" aria-labelledby="CabeceraPrimero" role="tabpanel">
                                                                           @include('Notification.form.medicamentoform2')                                                         
                                                                     </div><!-- FIN DEL ACCORDION-->  
                                                                 </div><br><!-- FIN DEL PANEL BLUE GREY-->  
                                                                 <!--codigo cabecera dos-->
                                                                 <div class="panel bg-blue-grey-200">                                        
                                                                     <div class="panel-heading" id="CabeceraTercero" role="tab">
                                                                         <a href="#AcordionTres" data-parent="#AcordionInternoInsumoMedico" data-toggle="collapse" aria-controls="AcordionTres" aria-expanded="false" class="panel-title collapsed" data-toggle="tooltip" title="Un método de detección reconocido mundialmente, le ayudará a detectar con mayor facilidad, que tan peligroso es el medicamento acorde a la reacción adversa">
                                                                             <i class="md-palette"></i>Algoritmo de Lassagna
                                                                         </a>
                                                                     </div>
                                                                     <div class="panel-collapse collapse bg-grey-200" id="AcordionTres" aria-labelledby="CabeceraTercero" role="tabpanel">
                                                                           @include('Notification.form.medicamentoform3')                                                         
                                                                     </div><!-- FIN DEL ACCORDION-->  
                                                                 </div><br><!-- FIN DEL PANEL BLUE GREY-->  
                                                             </div><!-- FIN DEL PANEL GROUP-->  
                                                             <button id="btnAdd" type="button" class="btn btn-primary pull-rigth" data-dismiss="modal">Añadir a la lista</button>

                                                         </div><!--FIN Form que  permite el almacenamiento en DOM-->   
                                                    </div><!-- FIN DEL MODAL BODY-->
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Cerrar</button>  
                                                    </div><!-- FIN DEL MODAL FOOTER-->
                                                </div><!-- FIN DEL MODAL CONTENT-->      
                                          </div><!-- FIN DEL MODAL DIALOG-->
                                      </div><!-- FIN MODAL PARA AGREGAR NUEVO INSUMO MEDICO -->

                                      <div class="row">                                           
                                          <div class="col-xs-12 col-lg-12">
                                              <!--Div que permite agregar insumos medicos a table en DOM-->  
                                              <div class="panel-body" id="Objetos">
                                                  <!--Aca esta la tabla que se agrega cada vez que se ingresa un nuevo insumo medico--> 
                                              </div> 
                                          </div>
                                     </div>
    	                            </div><!-- FIN DEL WIZARD PANE ACTIVE -->
    	                            <!-- FIN INSUMOS MEDICOS -->

    	                            <!-- REACCIONES ADVERSAS -->
    	                            <div class="tab-pane" id="exampleBilling2" role="tabpanel">
                                       <div class="row"> 
                                            <div class="col-sm-12 text-center">
                                                <h5>En el Paso 2 debes agregar las reacciones adversas que fueron causadas por el insumo medico sospechoso.</h5></br>   
                                            </div>                                                         
                                             <div class="col-xs-12 col-lg-10 col-lg-offset-1 text-center">
                                                    <h3>Agregar Reacciones Adversas</h3>
                                                    <div class="popover-primary" data-title="Ingresar Reacciones Adversas" data-content="Es hora de registrar las reacciones adversas sufridas por el insumo medico para poder evaluar mejor el caso" data-trigger="hover" data-placement="top" data-toggle="popover">
                                                     <button id="add-reaccion" title="Puede agregar tantas reacciones estime necesario"  type="button" class="btn btn-primary" data-toggle="modal" data-target="#OtrasReacciones" onclick="vaciarform('validacionotificacion');"> Click aqui para agregar</button>
                                                    </div><br><br>                                           
                                             </div> 
                                       </div>

                                       <!-- MODAL PARA AGREGAR NUEVA REACCION ADVERSA-->
                                       <div class="modal fade modal-primary modal-super-scaled" id="OtrasReacciones" aria-hidden="true" aria-labelledby="OtrasReacciones" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-center">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>                  
                                                        <h4 class="modal-title">Agregue la Información de la Reacción Adversa</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <!--Form que  permite el almacenamiento en DOM-->    
                                                        <div id="addreaccion">                                
                                                            <div class="panel-group panel-group-continuous margin-2" id="AcordionInternoReaccionAdversa" aria-multiselectable="true" role="tablist">
                                                                @include('alerts.errors')
                                                                <!--cuerpo del modal--> 
                                                                <h5>Agregar Reacciones Adversas presentadas con el medicamento</h5> 
                                                                 @include('Notification.form.reaccionform')
                                                            </div><!-- FINAL PANEL GROUP--> 
                                                            <button id="btnAddreaccion" type="button" class="btn btn-primary pull-rigth" data-dismiss="modal">Añadir a la lista</button>
                                                         </div><!--FIN Form que  permite el almacenamiento en DOM--> 
                                                    </div><!-- FINAL MODAL BODY--> 
                                                </div><!-- FINAL MODAL CONTENT-->                              
                                            </div><!-- FINAL MODAL DIALOG-->   
                                      </div>
                                      <!--FIN  MODAL PARA AGREGAR NUEVA REACCION ADVERSA-->

                                       <div class="row">                          
                                            <div class="col-xs-12 col-lg-12">
                                                <!--Div que permite agregar reacciones a table en DOM-->  
                                                <div class="panel-body" id="Objetos1">
                                                    <!--Aca esta la tabla que se agrega cada vez que se ingresa una nueva reaccion-->  
                                                </div> 
                                            </div>
                                       </div>
                                  </div><!-- FIN DEL WIZARD PANE ACTIVE -->
    	                            <!-- FIN REACCIONES ADVERSAS -->

    	                            <!-- CONFIRMACION DE NOTIFICACION -->
                                  <div class="tab-pane" id="exampleGetting" role="tabpanel">
                                        <div class="row">  
                                            <div class="col-sm-12 text-center">
                                                <h5>Para finalizar solo completa la siguiente informacion, Tambien te queremos agradecer por el envio de la notificacion.</h5></br>    
                                            </div>               
                                             <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                                                <h3>Ultimos detalles para enviar la notificacion</h3>
                                                <div id="last-form" class="col-sm-12">                                                     
                                                   @include('Notification.form.notificacionform')  
                                                    
                                                </div> 
                                             </div> 
                                        </div>
                                  </div>           
    	                            <!-- FIN CONFIRMACION DE NOTIFICACION -->

    	                      </div><!-- FIN CONTENIDO DEL WIZARD -->
    	               </div><!-- FIN PANEL BODY --> 
                      <!-- Previous/Next buttons -->
                      <ul class="pager wizard">
                          <li class="previous"><a href="javascript: void(0);">Atras</a></li>
                          <li class="next"><a href="javascript: void(0);">Siguiente</a></li>
                      </ul>
    	           </div><!-- FIN PANEL WIZARD FORM --> 
            </form> <!-- FIN FORM  DE VALIDACION--> 
	        <!-- FIN PANEL ENVIAR NOTIFICACION --> 
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   






<!-- MODAL PARA TERMINAR LA NOTIFICACION  -->
<div class="modal fade modal-success modal-super-scaled" id="finalizarnotificacion" aria-hidden="true" aria-labelledby="finalizarnotificacion" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center modal-sm">
          <div class="modal-content">
              <div class="modal-header">  
                  <a  href="{{ URL::route('paciente.index') }}" class="close" role="button">X</a>
              </div>
              <div class="modal-body text-center">  
                   <i class="btn btn-pure btn-success icon wb-check pull-center" aria-hidden="true" style="font-size: 50px;"></i>                      
                    <h2>Felicidades!</h2>
                    <p class="lead text-muted" style="display: block;">Haz enviado la notificacion de manera exitosa!</p>
                    </br>
                    <h4>Estado de la notificacion:</h4></br>
                    <label id="estadonoti" class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;"></label>
                    
              </div><!-- FIN DEL MODAL BODY-->
              <div class="modal-footer">                
                   <a  href="{{ URL::route('notificacion.index') }}" id="notificacionenviada" class="btn btn-danger" role="button">Cerrar</a>
              </div><!-- FIN DEL MODAL FOOTER-->
          </div><!-- FIN DEL MODAL CONTENT-->      
    </div><!-- FIN DEL MODAL DIALOG-->
</div>
<!-- FIN MODAL PARA TERMINAR LA NOTIFICACION -->



 @section('javascript')
  @section('scripts')   
    <!-- Scripts para ajax de ver usuarios -->
    {!!Html::script('js/scriptpaciente/verpaciente.js')!!}   
  @endsection

  @section('footer')
    @push('scripts')
      <script type="text/javascript">  
           function vaciarform(item){
            document.getElementById(item).reset();
          }

////////////////////////////////////////////////////////////////////////////////////////////////////////
          //Autocompletado select2 para factor riesgos
          var pathriesgo = "{{URL::route('factoriesgos')}}";
          // inicializamos el plugin
          $('#factoriesgos').select2({
              // Activamos la opcion "Tags" del plugin
              tags: true,
              tokenSeparators: [','],
              ajax: {
                  dataType: 'json',
                  url: pathriesgo,
                  delay: 250,
                  data: function(params) {
                      return {
                          term: params.term
                      }
                  },
                  processResults: function (data, page) {
                    return {
                      results: data
                    };
                  },
              }
          });

////////////////////////////////////////////////////////////////////////////////////////////////////////
          //Autocompletado del Insumo Medico
          var path = "{{URL::route('autocompletemedicamento')}}";
          //Enviado del contenido del textbox al controlador para obtener el autocompletado de la BD
          $('.nombremedicamento').typeahead({
              source:  function (query, process) {
               $.get(path, { query: query }, function (data) {
                        return process(data);
                  }), 'json';
              },
              minLength:2,
              hint: true,
              highlight: true, 
               //Guardar en el mismo texbox el valor seleccionado
              updater: function(item) { 
                    $("#codmedicamento").val(item.id);
                    return item;
                }
             
          });
////////////////////////////////////////////////////////////////////////////////////////////////////////  
          //Autocompletado de las Reacciones Adversas
          var path1 = "{{URL::route('autocomplete')}}";
          //Enviado del contenido del textbox al controlador para obtener el autocompletado de la BD
          $('.nombrereaccion').typeahead({
              source:  function (query, process) {
               $.get(path1, { query: query }, function (data) {
                        return process(data);
                  }), 'json';
              },
              minLength:2,
              hint: true,
              highlight: true, 
              //Guardar en el mismo texbox el valor seleccionado 
              updater: function(item) { 
                    $("#codreaccionadversa").val(item.id);
                    return item;
                }
             
          });  
////////////////////////////////////////////////////////////////////////////////////////////////////////
        //SCRIPT QUE PERMITE GUARDAR EN TABLAS DEL DOM LAS REACCIONES ADVERSAS E INSUMOS MEDICOS
        (function() {
          var objetos = []; // Contenedor de objetos del insumo medico.
          var arraylassagnas = []; // Contenedor de objetos del resultado lassagna
          var objetoslassagnas = []; // Contenedor de objetos de lassagna por medicamento
          var objetosreacciones = []; // Contenedor de objetos de reaccion adversa.
          var btnAdd = document.getElementById("btnAdd");//Variable que guarda id del boton para agregar insumos a la tabla
          var btnAddreaccion = document.getElementById("btnAddreaccion");//Variable que guarda id delboton para agregar reacciones a la tabla
          var btnGuardar = document.getElementById("btnGuardar");
          //var guardarnotificacion = $("*[data-wizard='finish']");
          

          $('#validacionotificacion')
          .formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              // This option will not ignore invisible fields which belong to inactive panels
              excluded: ':disabled',
              locale: 'es_ES',
                             
          })
          .bootstrapWizard({
              tabClass: 'nav nav-pills',
              onTabClick: function(tab, navigation, index) {
                  return validateTab(index);
              },
              onNext: function(tab, navigation, index) {
                  var numTabs    = $('#validacionotificacion').find('.tab-pane').length,
                      isValidTab = validateTab(index - 1);
                  if (!isValidTab) {
                      return false;
                  }

                  if (index === numTabs) {
                      // We are at the last tab

                      // Uncomment the following line to submit the form using the defaultSubmit() method
                      //$('#validacionotificacion').formValidation('defaultSubmit');

                      // For testing purpose
                        guardarnotificacion();
                        function guardarnotificacion(){
                              var token = $("#token").val();
                              var TableDatareaccion = new Array();
                              var TableDatainsumo = new Array();
                              var TableDataconfirmacion = new Array();
                              var TableDatafactoriesgos = new Array();
                              var TableDatalassagnas = new Array();
                              //var i=0;
                              //var s=0;

                              TableDatainsumo= objetos;
                              //TableDatainsumo = JSON.stringify(TableDatainsumo);
                              //alert(TableDatainsumo);
                              TableDatareaccion= objetosreacciones;
                              //TableDatareaccion = JSON.stringify(TableDatareaccion);
                              //alert(TableDatareaccion);
                              TableDatalassagnas= objetoslassagnas;
                              //TableDatalassagnas = JSON.stringify(TableDatalassagnas);
                              //alert(TableDatalassagnas);
                          /*******************************************************************************************************/
                          /*******************************************************************************************************/
                                  //Ingreso de las ultimas opciones del formulario, especificamente de notificacion
                                  var idnotificador = $("#idnotificador").val(); 
                                  var idpaciente = $("#idpaciente").val(); 
                                  var idhospitalnoti = $("#idhospitalnoti").val();
                                  var estadonotificacion = $("#estadonotificacion").val(); 
                                  var valoringresohospital = document.getElementsByName('ingresohospital[]');            
                                  var ingresohospital = '';
                                   for(var i=0;i<valoringresohospital.length;i++)
                                    {
                                       if(valoringresohospital[i].checked)
                                          ingresohospital = valoringresohospital[i].value;                 
                                    }
                                  var pesopaciente = $("#pesopaciente").val(); 
                                  var factoriesgos = $("#factoriesgos").select2('val');
                                  TableDatafactoriesgos.push({factoriesgos}); 

                                  TableDataconfirmacion.push({idnotificador,idpaciente,estadonotificacion,ingresohospital,pesopaciente,idhospitalnoti});
                                  //console.log(TableDataconfirmacion);
                                  //console.log(TableDatafactoriesgos);

                          /*******************************************************************************************************/
                          /*******************************************************************************************************/
                                //var arraynotificacion = TableDatainsumo.concat(TableDatareaccion,TableDataconfirmacion);
                                //arraynotificacion = JSON.stringify(arraynotificacion);

                                //TableDataconfirmacion = JSON.stringify(TableDataconfirmacion);
                                //TableDatainsumo = JSON.stringify(TableDatainsumo);

                                var arraynotificacion = {
                                    TableDataconfirmacion: TableDataconfirmacion,
                                    TableDatafactoriesgos: TableDatafactoriesgos,
                                    TableDatareaccion: TableDatareaccion,
                                    TableDatainsumo: TableDatainsumo,
                                    TableDatalassagnas:TableDatalassagnas
                                };

                                //console.log(arraynotificacion);
                                var route = "http://sinraim.omarboza.com/notificacion";
                                    $.ajax({
                                      url: route,
                                      headers: {'X-CSRF-TOKEN': token},
                                      type: 'POST',
                                      data: JSON.stringify(arraynotificacion),
                                      contentType: "json; charset=utf-8",
                                      processData: false,
                                      //data: {familiar: familiar, edad: edad, patient_id: patient_id},
                                      

                                      success:function(){
                                        //$("#nuevo-historial").modal('toggle');
                                        $('#finalizarnotificacion').modal('show');
                                        console.log(arraynotificacion);
                                        //$("#central").load('inc/gallery.php');
                                        //$("#msj-success").fadeIn();
                                      },
                                      error:function(msj){
                                        $("#msj-error").fadeIn();
                                        console.log(arraynotificacion);
                                      }
                                    });
                          };
                      }

                  return true;
              },
              onPrevious: function(tab, navigation, index) {
                  return validateTab(index + 1);
              },
              onTabShow: function(tab, navigation, index) {
                  // Update the label of Next button when we are at the last tab
                  var numTabs = $('#validacionotificacion').find('.tab-pane').length;
                  $('#validacionotificacion')
                      .find('.next')
                          .removeClass('disabled')    // Enable the Next button
                          .find('a')
                          .html(index === numTabs - 1 ? 'Install' : 'Next');

                  // You don't need to care about it
                  // It is for the specific demo
              }
          });

          function validateTab(index) {
              var fv   = $('#validacionotificacion').data('formValidation'), // FormValidation instance
                  // The current tab
                  $tab = $('#validacionotificacion').find('.tab-pane').eq(index);

              // Validate the container
              fv.validateContainer($tab);

              var isValidStep = fv.isValidContainer($tab);
              if (isValidStep === false || isValidStep === null) {
                  // Do not jump to the target tab
                  return false;
              }

              return true;
          }


          /*******************************************************************************************************/
          //Evento que permite llamar a las funciones de recibir los datos del formulario y enviarlos a la pantalla
          btnAdd.addEventListener("click", function() {
            /*agregarObjetos();// Recibe los datos del formulario insumos medicos 
            listarObjetos("Objetos", objetos);*///Envia los datos a la tabla para mostrarla en la vista
            if($("#nombremedicamento").val().length===0 || $("#dosispaciente").val().length===0 || $("#unidadmedida_id").val().length===0 || $("#fechainiciomedicamento").val().length===0){
                alert("Por favor llenar los datos");
              }
              else{
                  agregarObjetos();// Recibe los datos del formulario insumos medicos 
                  listarObjetos("Objetos", objetos);
                  mostrarnext();
              }
          });
          /******************************************************************************************************/

          //Evento que permite llamar a las funciones de recibir los datos del formulario y enviarlos a la pantalla
           btnAddreaccion.addEventListener("click", function() {
            if($("#nombrereaccion").val().length===0 || $("#fechainicioreaccion").val().length===0){
                alert("Por favor llenar los datos");
              }
              else{
                  agregarObjetosreaccion();// Recibe los datos del formulario Reaccion Adversa 
                  listarObjetosreaccion("Objetos1", objetosreacciones);//Envia los datos a la tabla para mostrarla en la vista
                  mostrarnext();
              }
            
          });
           /****************************************************************************************************/
 
         
         
          /*******************************************************************************************************/
          /*******************************************************************************************************/
          //Permite recibir los datos del formulario de Insumos Medicos
          function agregarObjetos() {              
              var objeto, objetolassagna, nombremedicamento, codmedicamento, dosispaciente, unidadmedida_id, frecuenciapaciente, tipoinsumo, viadministracion_id,
                  fechainiciomedicamento, fechafinmedicamento, fechavencimiento, fabricante, numlote, motivoprescripcion;


              var valorlassagna1 = document.getElementsByName('lassagna1[]');  
              var valorlassagna2 = document.getElementsByName('lassagna2[]'); 
              var valorlassagna3 = document.getElementsByName('lassagna3[]');  
              var valorlassagna4 = document.getElementsByName('lassagna4[]');  
              var valorlassagna5 = document.getElementsByName('lassagna5[]');  
              var valorlassagna6 = document.getElementsByName('lassagna6[]');  
              var valorlassagna7 = document.getElementsByName('lassagna7[]');  
              var valorlassagna8 = document.getElementsByName('lassagna8[]');  
              var valorlassagna9 = document.getElementsByName('lassagna9[]');  
              var valorlassagna10 = document.getElementsByName('lassagna10[]');            
              var lassagna1 = 0;
              var lassagna2 = 0;
              var lassagna3 = 0;
              var lassagna4 = 0;
              var lassagna5 = 0;
              var lassagna6 = 0;
              var lassagna7 = 0;
              var lassagna8 = 0;
              var lassagna9 = 0;
              var lassagna10 =0;

              var resultadolassagna = 0;
              //var estadolassagna = "";
              for(var i=0;i<valorlassagna1.length;i++)
              {
                 if(valorlassagna1[i].checked)
                    lassagna1 = valorlassagna1[i].value; 
                if(valorlassagna2[i].checked)
                    lassagna2 = valorlassagna2[i].value;     
                if(valorlassagna3[i].checked)
                    lassagna3 = valorlassagna3[i].value; 
                if(valorlassagna4[i].checked)
                    lassagna4 = valorlassagna4[i].value;    
                if(valorlassagna5[i].checked)
                    lassagna5 = valorlassagna5[i].value; 
                if(valorlassagna6[i].checked)
                    lassagna6 = valorlassagna6[i].value;    
                if(valorlassagna7[i].checked)
                    lassagna7 = valorlassagna7[i].value; 
                if(valorlassagna8[i].checked)
                    lassagna8 = valorlassagna8[i].value;    
                if(valorlassagna9[i].checked)
                    lassagna9 = valorlassagna9[i].value; 
                if(valorlassagna10[i].checked)
                    lassagna10 = valorlassagna10[i].value; 

                resultadolassagna= parseInt(lassagna1)+parseInt(lassagna2)+parseInt(lassagna3)+parseInt(lassagna4)+parseInt(lassagna5)+parseInt(lassagna6)+parseInt(lassagna7)+parseInt(lassagna8)+parseInt(lassagna9)+parseInt(lassagna10);
                
              }
              var resultadolassagnaind = resultadolassagna.toString();
              arraylassagnas.push(resultadolassagna);
              //console.log(JSON.stringify(arraylassagnas));
              var numeromayor;
              numeromayor=arraylassagnas[0];
              for(var i=0;i<arraylassagnas.length;i++)
                    {
                       if(arraylassagnas[i]>numeromayor)
                          numeromayor = arraylassagnas[i]; 
                    }
              if (numeromayor >= 9) {
                  $("#estadonotificacion").val("PROBADA"); 
                  $("#estadonoti").text("PROBADA");
                  //estadolassagna="PROBADA";
              } else if (numeromayor >= 5 && numeromayor <= 8) {
                  $("#estadonotificacion").val("PROBABLE");
                  $("#estadonoti").text("PROBABLE");
                  //estadolassagna="PROBABLE";
              } else if (numeromayor >= 1 && numeromayor <= 4){
                  $("#estadonotificacion").val("POSIBLE");
                  $("#estadonoti").text("POSIBLE");
                  //estadolassagna="POSIBLE";
              } else if (numeromayor <= 0){
                  $("#estadonotificacion").val("DUDOSA");
                  $("#estadonoti").text("DUDOSA");
                  //estadolassagna="DUDOSA";
              }

              objetolassagna = {};
              objetolassagna.lassagna1 = lassagna1;
              objetolassagna.lassagna2 = lassagna2;
              objetolassagna.lassagna3 = lassagna3;
              objetolassagna.lassagna4 = lassagna4;
              objetolassagna.lassagna5 = lassagna5;
              objetolassagna.lassagna6 = lassagna6;
              objetolassagna.lassagna7 = lassagna7;
              objetolassagna.lassagna8 = lassagna8;
              objetolassagna.lassagna9 = lassagna9;
              objetolassagna.lassagna10 = lassagna10;
              objetolassagna.resultadolassagnaind = resultadolassagnaind;
              //objetolassagna.estadolassagna = estadolassagna;
              objetoslassagnas.push(objetolassagna);               
              console.log(objetolassagna);



              //Recibir del formulario los id
              nombremedicamento = document.getElementById("nombremedicamento");
              codmedicamento = document.getElementById("codmedicamento");
              dosispaciente = document.getElementById("dosispaciente");
              unidadmedida_id = document.getElementById("unidadmedida_id");
              frecuenciapaciente = document.getElementById("frecuenciapaciente");
              tipoinsumo = document.getElementById("tipoinsumo");
              viadministracion_id = document.getElementById("viadministracion_id");
              fechainiciomedicamento = document.getElementById("fechainiciomedicamento");
              fechafinmedicamento = document.getElementById("fechafinmedicamento");
              fechavencimiento = document.getElementById("fechavencimiento");
              fabricante = document.getElementById("fabricante");
              numlote = document.getElementById("numlote");
              motivoprescripcion = document.getElementById("motivoprescripcion");


              // El objeto «objeto» contendrá los datos del formulario.
              objeto = {};
              objeto.nombremedicamento = nombremedicamento.value;
              objeto.codmedicamento = codmedicamento.value;
              objeto.dosispaciente = dosispaciente.value;
              objeto.unidadmedida_id = unidadmedida_id.value;
              objeto.frecuenciapaciente = frecuenciapaciente.value;
              objeto.tipoinsumo = tipoinsumo.value;
              objeto.viadministracion_id = viadministracion_id.value;
              objeto.fechainiciomedicamento = fechainiciomedicamento.value;
              objeto.fechafinmedicamento = fechafinmedicamento.value;
              objeto.fechavencimiento = fechavencimiento.value;
              objeto.fabricante = fabricante.value;
              objeto.numlote = numlote.value;
              objeto.motivoprescripcion = motivoprescripcion.value;

              

              // Se añade un objeto al array «objetos».
              objetos.push(objeto);
              //console.log(JSON.stringify(objetos));
              //return objetos;
              //console.log(objetos);
          }

    
          //Permite recibir los datos del formulario de Insumos Medicos
          function listarObjetos(id, data) {
              var divData, contenido, i;
              divData = document.getElementById(id);
              //encabezado de la tabla
              contenido = "<h4>Medicamentos Agregados</h4></br><table id='tableinsumo', class='table table-bordered'><thead><tr><th>Nombre Insumo Médico</th><th class='hidden'>Codigo Insumo Médico</th><th> Dosis</th><th>Unidad de Medida</th><th class='hidden'>Frecuencia</th><th class='hidden'> Tipo de Insumo</th><th class='hidden'>Via de Administracion</th><th class='hidden'> Fecha de Inicio</th><th class='hidden'>Fecha Fin</th><th class='hidden'>Fecha de Vencimiento</th><th class='hidden'>Fabricante</th><th class='hidden'>Numero de Lote </th><th class='hidden'>Motivo Prescripcion</th><th>Accion</th></tr></thead>";

                //Guardar cada valor ingresado en la tabla
                contenido += "<tbody id='tablebodyinsumo'>";
                for (i = 0; i < data.length; i++) {
                  contenido += "<tr class='tblinsumo'><td id='nombremedicamento"+i+"'>" + data[i].nombremedicamento + 
                              "</td><td class='hidden' id='codmedicamento"+i+"'>" + data[i].codmedicamento + 
                              "</td><td class='hidden' id='dosispaciente"+i+"'>" + data[i].dosispaciente + 
                              "</td><td id='unidadmedida_id"+i+"'>" + data[i].unidadmedida_id + 
                              "</td><td class='hidden' id='frecuenciapaciente"+i+"'>" + data[i].frecuenciapaciente + 
                              "</td><td id='tipoinsumo"+i+"'>" + data[i].tipoinsumo + 
                              "</td><td class='hidden' id='viadministracion_id"+i+"'>" + data[i].viadministracion_id + 
                              "</td><td class='hidden' id='fechainiciomedicamento"+i+"'>" + data[i].fechainiciomedicamento + 
                              "</td><td class='hidden' id='fechafinmedicamento"+i+"'>" + data[i].fechafinmedicamento +
                              "</td><td class='hidden' id='fechavencimiento"+i+"'>" + data[i].fechavencimiento + 
                              "</td><td class='hidden' id='fabricante"+i+"'>" + data[i].fabricante + 
                              "</td><td class='hidden' id='numlote"+i+"'>" + data[i].numlote + 
                              "</td><td class='hidden' id='motivoprescripcion"+i+"'>" + data[i].motivoprescripcion + 
                              "</td><td> <button class='btn btn-danger btndelete'>Eliminar</button>"  +
                              "</td></tr>";
                }
                contenido += "</tbody></table>";
                divData.innerHTML = contenido;

                //Funcion para eliminar los valores de la tabla
                 $(".btndelete").click(function(){
                     var fila=this.parentNode.parentNode;
                     data.splice(fila.rowIndex-1,1);
                     $(this).closest('tr').remove();
                     escondernext2();
                 });
          }

          /*******************************************************************************************************/
          /*******************************************************************************************************/
           function agregarObjetosreaccion() {
              var objetoreaccion, nombrereaccion, codreaccionadversa, intensidadreaccion, fechainicioreaccion, fechafinreaccion, desenlacereaccion;  

                //Recibir del formulario los id
                nombrereaccion = document.getElementById("nombrereaccion"); 
                codreaccionadversa = document.getElementById("codreaccionadversa"); 
                intensidadreaccion = document.getElementById("intensidadreaccion"); 
                fechainicioreaccion = document.getElementById("fechainicioreaccion"); 
                fechafinreaccion = document.getElementById("fechafinreaccion"); 
                desenlacereaccion = document.getElementById("desenlacereaccion"); 

                // El objeto «objeto» contendrá los datos del formulario.
                objetoreaccion = {};
                objetoreaccion.nombrereaccion = nombrereaccion.value;
                objetoreaccion.codreaccionadversa = codreaccionadversa.value;
                objetoreaccion.intensidadreaccion = intensidadreaccion.value;
                objetoreaccion.fechainicioreaccion = fechainicioreaccion.value;
                objetoreaccion.fechafinreaccion = fechafinreaccion.value;
                objetoreaccion.desenlacereaccion = desenlacereaccion.value;

                // Se añade un objeto al array «objetos».
                objetosreacciones.push(objetoreaccion);
                //console.log(objetosreacciones);
          }


          function listarObjetosreaccion(id, data) {
            var divData, contenido, i;
            divData = document.getElementById(id);
            //encabezado de la tabla
            contenido = "<h4>Reacciones Adversas Agregadas</h4></br><table id='tablereaccion', class='table table-bordered'><thead><tr><th>Nombre Reaccion Adversa</th><th class='hidden'>Codigo Reaccion Adversa</th><th> Intensidad </th><th class='hidden'>Fecha de Inicio</th><th class='hidden'>Fecha de Fin</th><th> Desenlace</th><th>Accion</th></tr></thead>";

            //Guardar cada valor ingresado en la tabla
            contenido += "<tbody id='tablebodyreaccion'>";
            for (i = 0; i < data.length; i++) {
              contenido += "<tr class='tblreaccion'><td id='nombrereaccion"+i+"'>" + data[i].nombrereaccion +  
                          "</td><td class='hidden' id='codreaccionadversa"+i+"'>" + data[i].codreaccionadversa +
                          "</td><td id='intensidadreaccion"+i+"'>" + data[i].intensidadreaccion + 
                          "</td><td class='hidden' id='fechainicioreaccion"+i+"'>" + data[i].fechainicioreaccion + 
                          "</td><td class='hidden' id='fechafinreaccion"+i+"'>" + data[i].fechafinreaccion + 
                          "</td><td id='desenlacereaccion"+i+"'>" + data[i].desenlacereaccion + 
                          "</td><td> <button class='btn btn-danger btndelete2'>Eliminar</button>"  +
                          "</td></tr>";
            }
            contenido += "</tbody></table>";
            divData.innerHTML = contenido;

            //Funcion para eliminar los valores de la tabla
             $(".btndelete2").click(function(){
               var fila=this.parentNode.parentNode;
               data.splice(fila.rowIndex-1,1);
               $(this).closest('tr').remove();
               escondernext();
             });
          }
        })();  

      
      
      </script>
{!!Html::script('js/ayuda/notificacion-tour.js')!!}   
<!-- Scripts para validacion de notificacion -->
{!!Html::script('js/scriptnotificacion/main.js')!!}  
    @endpush
@endsection

    
@stop