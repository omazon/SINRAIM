@extends('layouts.layout')

@section('title')
  Notificacion | Ver Detalles de Notificacion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL PARA VER DETALLES DE NOTIFICACIONES ENVIADAS</h1>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
            {!!link_to('/notificacion', $title='Regresar a Notificacion', $attributes = ['class'=>'btn btn-success pull-left'], $secure = null)!!}             
            <button id="descargarnotificacion" class='btn btn-success pull-right' data-toggle='modal' data-target='#descarga'>Descargar</button>           
            <br><br>    

            <ul class="nav nav-tabs nav-tabs-line" data-plugin="nav-tabs" role="tablist" id="paneltab">
                  <li class="active" role="presentation">
                      <a data-toggle="tab" href="#verNoti_1" aria-controls="verNoti_1" role="tab">Todo
                      </a>
                  </li>
                  <li role="presentation">
                      <a data-toggle="tab" href="#verNoti_2" aria-controls="verNoti_2" role="tab">Paciente
                      </a>
                  </li>
                  <li role="presentation">
                      <a data-toggle="tab" href="#verNoti_3" aria-controls="verNoti_3" role="tab">Reacción
                      </a>
                  </li>
                  <li role="presentation">
                      <a data-toggle="tab" href="#verNoti_4" aria-controls="verNoti_4" role="tab">Medicamento
                      </a>
                  </li>
                  <li role="presentation">
                      <a data-toggle="tab" href="#verNoti_5" aria-controls="verNoti_5" role="tab">Médico
                      </a>
                  </li>
                   <li role="presentation">
                      <a data-toggle="tab" href="#verNoti_6" aria-controls="verNoti_6" role="tab">Factores de riegos asociados
                      </a>
                  </li>
              </ul>                     
               <div class="tab-content" id="panelprincipal">
                    <div class="tab-pane active" id="verNoti_1">
                         <br>
                         <h3 class="black-600">Datos De Notificacion</h3>
                         <div class="row">
                            <div class="col-sm-12 col-sm-offset-1">
                                <table class="espacio">
                                   @foreach ($notifications as $notification)
                                         <tr>
                                              <td>
                                                  <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Numero de Notificacion:  </label>
                                              </td>
                                              <td>
                                                  <h4 class="panel-title"><?php echo $notification->id; ?></h4>
                                              </td>
                                         </tr>                                              
                                         <tr>
                                              <td>
                                                  <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Estado de la Notificacion:  </label>
                                              </td>
                                              <td>
                                                  @if ($notification->estado === "PROBADA")
                                                      <span class="label label-danger" style="font-size: 100%">{{$notification->estado}}</span>
                                                  @elseif ($notification->estado === "PROBABLE")
                                                      <span class="label label-warning" style="font-size: 100%">{{$notification->estado}}</span>
                                                  @elseif ($notification->estado === "POSIBLE")
                                                      <span class="label label-info" style="font-size: 100%">{{$notification->estado}}</span>
                                                  @else
                                                      <span class="label label-success" style="font-size: 100%">{{$notification->estado}}</span>
                                                  @endif     
                                              </td>
                                         </tr>
                                         <tr>
                                              <td>
                                                  <label class="lead text-muted" style="display: block; color:#3583CA; font-weight: bold;">Observaciones:  </label>
                                              </td>
                                              <td>
                                                  <h4 class="panel-title"><?php echo $notification->observaciones; ?></h4>
                                              </td>
                                         </tr>       
                                                                              
                                    @endforeach 
                                 </table>
                             </div>
                          </div>
                          <h3 class="black-600">Datos Del Paciente</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                        @foreach ($pacientes as $paciente)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Apellido  </label>
                                                </td>
                                                 <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Edad  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Género  </label>
                                                </td>
                                                
                                           </tr>                                              
                                           <tr>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $paciente->name; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $paciente->last_name; ?></h4>
                                                </td>
                                                  <td>
                                                    <h4 class="panel-title"><?php echo $paciente->edad; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $paciente->genero; ?></h4>
                                                </td>
                                           </tr>                                                                              
                                      @endforeach                                          
                                   </table>
                               </div>
                            </div>
                            <h3 class="black-600">Datos Del Notificador</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                        @foreach ($usuarios as $usuario)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre  </label>
                                                </td>
                                                 <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Apellido  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Email  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Teléfono  </label>
                                                </td>
                                                
                                           </tr>                                              
                                           <tr>                                                     
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->name; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->last_name; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->email; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->telefono; ?></h4>
                                                </td>
                                           </tr>                                                                              
                                      @endforeach                                          
                                   </table>
                               </div>
                            </div> 
                          <h3 class="black-600">Datos Del Insumo Medico</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                         @foreach ($insumomedicos as $insumomedico)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre del Insumo Medico:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title" style="display: block; color:#3583CA; font-weight: bold;"><?php echo $insumomedico->descripcion; ?></h4>
                                                </td>
                                           </tr>  
                                            <tr>
                                              <td>
                                                  <label class="lead text-muted" style="display: block;  color:#3583CA;">Estado del Insumo Medico:  </label>
                                              </td>
                                                 
                                              <td>
                                                  <!--@if ($insumomedico->estadolassagna === "PROBADA")
                                                      <span class="label label-danger" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @elseif ($insumomedico->estadolassagna === "PROBABLE")
                                                      <span class="label label-warning" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @elseif ($insumomedico->estadolassagna === "POSIBLE")
                                                      <span class="label label-info" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @else
                                                      <span class="label label-success" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @endif -->    
                                              </td>
                                           </tr>                                                      
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Dosis Tomada:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->dosis; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Unidad de Medida:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->unidadmedida; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Frecuencia de Consumo:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->frecuencia; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Tipo de Insumo Médico:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->tipoinsumo; ?></h4>
                                                </td>
                                           </tr>      
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Via de Administracion:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->viadministracion; ?></h4>
                                                </td>
                                           </tr>       
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Inicio de toma del medicamento:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fechainicio; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fin de toma del medicamento:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fechafin; ?></h4>
                                                </td>
                                           </tr>   
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fecha de Vencimiento:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fechavencimiento; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fabricante:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fabricante; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Numero de Lote:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->numlote; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Motivo de la Prescripcion:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->motivo; ?></h4>
                                                </td>
                                           </tr>    
                                                                                                        
                                      @endforeach
                                    </table>
                               </div>
                            </div> 
                            <h3 class="black-600">Datos De la Reaccion Adversa</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                        @foreach ($reaccionadversas as $reaccionadversa)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre de la Reaccion Adversa:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title" style="display: block; color:#3583CA; font-weight: bold;"><?php echo $reaccionadversa->descripcion; ?></h4>
                                                </td>
                                           </tr>                                              
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Intensidad:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->intensidad; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Inicio de la Reaccion Adversa:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->fechainicio; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fin de la Reaccion Adversa:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->fechafin; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Desenlace:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->desenlace; ?></h4>
                                                </td>
                                           </tr>                                                                                   
                                      @endforeach                                          
                                   </table>
                               </div>
                            </div>                                   
                    </div>
                    <div class="tab-pane" id="verNoti_2">
                       <br>
                       <h3 class="black-600">Datos Del Paciente</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                        @foreach ($pacientes as $paciente)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Apellido  </label>
                                                </td>
                                                 <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Edad  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Género  </label>
                                                </td>
                                                
                                           </tr>                                              
                                           <tr>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $paciente->name; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $paciente->last_name; ?></h4>
                                                </td>
                                                  <td>
                                                    <h4 class="panel-title"><?php echo $paciente->edad; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $paciente->genero; ?></h4>
                                                </td>
                                           </tr>                                                                              
                                      @endforeach                                          
                                   </table>
                               </div>
                            </div>
                    </div>
                    <div class="tab-pane" id="verNoti_3">
                          <br>
                          <h3 class="black-600">Datos De la Reaccion Adversa</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                        @foreach ($reaccionadversas as $reaccionadversa)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre de la Reaccion Adversa:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title" style="display: block; color:#3583CA; font-weight: bold;"><?php echo $reaccionadversa->descripcion; ?></h4>
                                                </td>
                                           </tr>                                              
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Intensidad:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->intensidad; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Inicio de la Reaccion Adversa:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->fechainicio; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fin de la Reaccion Adversa:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->fechafin; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Desenlace:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $reaccionadversa->desenlace; ?></h4>
                                                </td>
                                           </tr>                                                                                   
                                      @endforeach                                          
                                   </table>
                               </div>
                            </div>                           
                    </div>
                     <div class="tab-pane" id="verNoti_4">
                        <br>
                        <h3 class="black-600">Datos Del Isumo Medico</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                         @foreach ($insumomedicos as $insumomedico)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre del Insumo Medico:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title" style="display: block; color:#3583CA; font-weight: bold;"><?php echo $insumomedico->descripcion; ?></h4>
                                                </td>
                                           </tr> 
                                           <tr>
                                              <td>
                                                  <label class="lead text-muted" style="display: block;  color:#3583CA;">Estado del Insumo Medico:  </label>
                                              </td>
                                              <td>
                                                  @if ($insumomedico->estadolassagna === "PROBADA")
                                                      <span class="label label-danger" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @elseif ($insumomedico->estadolassagna === "PROBABLE")
                                                      <span class="label label-warning" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @elseif ($insumomedico->estadolassagna === "POSIBLE")
                                                      <span class="label label-info" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @else
                                                      <span class="label label-success" style="font-size: 100%">{{$insumomedico->estadolassagna}}</span>
                                                  @endif     
                                              </td>
                                           </tr>                                                      
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Dosis Tomada:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->dosis; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Unidad de Medida:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->unidadmedida; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Frecuencia de Consumo:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->frecuencia; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Tipo de Insumo Médico:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->tipoinsumo; ?></h4>
                                                </td>
                                           </tr>      
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Via de Administracion:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->viadministracion; ?></h4>
                                                </td>
                                           </tr>       
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Inicio de toma del medicamento:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fechainicio; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fin de toma del medicamento:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fechafin; ?></h4>
                                                </td>
                                           </tr>   
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fecha de Vencimiento:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fechavencimiento; ?></h4>
                                                </td>
                                           </tr>
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Fabricante:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->fabricante; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Numero de Lote:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->numlote; ?></h4>
                                                </td>
                                           </tr>  
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Motivo de la Prescripcion:  </label>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $insumomedico->motivo; ?></h4>
                                                </td>
                                           </tr>    
                                                                                
                                      @endforeach                                           
                                   </table>
                               </div>
                            </div> 
                    </div>
                     <div class="tab-pane" id="verNoti_5">
                        <br>
                        <h3 class="black-600">Datos Del Notificador</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  <table class="espacio">
                                        @foreach ($usuarios as $usuario)
                                           <tr>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Nombre  </label>
                                                </td>
                                                 <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Apellido  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Email  </label>
                                                </td>
                                                <td>
                                                    <label class="lead text-muted" style="display: block; color:#3583CA; ">Teléfono  </label>
                                                </td>
                                                
                                           </tr>                                              
                                           <tr>                                                     
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->name; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->last_name; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->email; ?></h4>
                                                </td>
                                                <td>
                                                    <h4 class="panel-title"><?php echo $usuario->telefono; ?></h4>
                                                </td>
                                           </tr>                                                                              
                                      @endforeach                                          
                                   </table>
                               </div>
                            </div>           
                     </div>
                      <div class="tab-pane" id="verNoti_6">
                       <br>
                       <h3 class="black-600">Factores de riegos asociados</h3>
                           <div class="row">
                              <div class="col-sm-12 col-sm-offset-1">
                                  @foreach ($factoresderiesgos as $factoresderiesgo)      
                                      <label class="panel-title" style="display: block; color:#3583CA;"> * {{$factoresderiesgo->factoresderiesgos}}</label>
                                  @endforeach  
                               </div>
                            </div>
                    </div>


                </div><!-- FIN TAB CONTENT -->   
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

 <!--MODAL PARA DESCARGAR-->
<div class="modal fade modal-success" id="descarga" aria-hidden="true" aria-labelledby="descarga" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">X</span>
                </button>
                <h4 class="modal-title">Descarga la Notificación</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6 text-center">
                         @foreach ($notifications as $notification)
                              <a href="{{ URL::route('exportarexcelnotificacion', $notification->id) }}">
                                  <img src="https://upload.wikimedia.org/wikipedia/commons/8/86/Microsoft_Excel_2013_logo.svg" >
                              </a>
                         @endforeach                           
                    </div>
                    <div class="col-xs-6 text-left">
                        @foreach ($notifications as $notification)
                              <a href="{{ URL::route('exportarpdfnotificacion', $notification->id) }}">
                                  <img width="100" src="https://upload.wikimedia.org/wikipedia/commons/7/7d/Adobe_PDF.svg">
                              </a>
                         @endforeach                           
                    </div>
                </div>           
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>        
</div>
<!--FIN MODAL PARA DESCARGAR-->

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   

 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
        {!!Html::script('js/ayuda/vernotind-tour.js')!!} 
    @endpush
@endsection
    
@stop

