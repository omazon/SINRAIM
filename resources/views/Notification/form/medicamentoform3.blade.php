<div class="panel-body">
   <form>                                                      
        <div class="row">
            <div class="col-sm-12">
                <div style="overflow-y: scroll; height: 500px; overflow-x: hidden ">

                    <!-- PREGUNTA NUEMRO 1 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">1. ¿Existen evidencias previas sobre que este medicamento causa esta reaccion?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 1, false, ['name' => 'lassagna1[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi1', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 0, false, ['name' => 'lassagna1[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano1', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna1[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce1', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>   
                     <!-- FIN PREGUNTA NUEMRO 1 DEL ALGORITMO LASSAGNA-->

                     <!-- PREGUNTA NUEMRO 2 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">2. ¿La Reacción Adversa apareció después de administrar el medicamento sospechoso?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 2, false, ['name' => 'lassagna2[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi2', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', -1, false, ['name' => 'lassagna2[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano2', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna2[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce2', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 2 DEL ALGORITMO LASSAGNA-->

                     <!-- PREGUNTA NUEMRO 3 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">3. ¿La Reacción Adversa Mejoro al suspender el medicamento?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 1, false, ['name' => 'lassagna3[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi3', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 0, false, ['name' => 'lassagna3[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano3', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna3[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce3', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 3 DEL ALGORITMO LASSAGNA-->

                     <!-- PREGUNTA NUEMRO 4 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">4. ¿La Reacción Adversa reapareció al re administrar el medicamento?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 2, false, ['name' => 'lassagna4[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi4', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', -1, false, ['name' => 'lassagna4[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano4', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna4[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce4', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 4 DEL ALGORITMO LASSAGNA-->

                    <!-- PREGUNTA NUEMRO 5 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">5. ¿Existen causas alternativas para explicar la reacción adversa?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', -1, false, ['name' => 'lassagna5[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi5', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 2, false, ['name' => 'lassagna5[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano5', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna5[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce5', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 5 DEL ALGORITMO LASSAGNA-->

                    <!-- PREGUNTA NUEMRO 6 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">6. ¿Se presento la reacción adversa después de administrar un placebo?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', -1, false, ['name' => 'lassagna6[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi6', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 1, false, ['name' => 'lassagna6[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano6', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna6[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce6', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 6 DEL ALGORITMO LASSAGNA-->

                    <!-- PREGUNTA NUEMRO 7 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">7. ¿Después de realizar exámenes, se determino la presencia del fármaco en la sangre u otros líquidos biológicos?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 1, false, ['name' => 'lassagna7[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi7', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 0, false, ['name' => 'lassagna7[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano7', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna7[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce7', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 7 DEL ALGORITMO LASSAGNA-->

                     <!-- PREGUNTA NUEMRO 8 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">8. ¿La reacción adversa fue más intensa al aumentar la dosis?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 1, false, ['name' => 'lassagna8[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi8', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 0, false, ['name' => 'lassagna8[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano8', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna8[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce8', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 8 DEL ALGORITMO LASSAGNA-->

                     <!-- PREGUNTA NUEMRO 9 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">9. ¿El paciente ha tenido reacciones similares al medicamento sospechoso o a medicamentos similares?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 1, false, ['name' => 'lassagna9[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi9', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 0, false, ['name' => 'lassagna9[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano9', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna9[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce9', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 9 DEL ALGORITMO LASSAGNA-->

                     <!-- PREGUNTA NUEMRO 10 DEL ALGORITMO LASSAGNA-->
                    <div class="col-sm-12">
                         <h4 class="control-label">10. ¿La Reacción Adversa mejoro al quitar el medicamento o al suministrar otro?</h4>
                         <div class="form-group">
                             <div class="row">
                                 <div class="col-xs-4 text-center">
                                    {!! Form::label('ingresosi', 'SI') !!}
                                    {!! Form::radio('radio', 1, false, ['name' => 'lassagna10[]',  'class'=>'icheckbox-primary', 'id' => 'lassagnasi10', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('ingresono', 'NO') !!}
                                     {!! Form::radio('radio', 0, false, ['name' => 'lassagna10[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnano10', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                                 <div class="col-xs-4">
                                     {!! Form::label('desconocido', 'SE DESCONOCE') !!}
                                     {!! Form::radio('radio', 0, true, ['name' => 'lassagna10[]', 'class'=>'icheckbox-primary', 'id' => 'lassagnasedesconoce10', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                                 </div>
                             </div>  
                         </div>
                     </div>
                     <!-- FIN PREGUNTA NUEMRO 10 DEL ALGORITMO LASSAGNA-->                     
                 </div>                 
            </div>  
        </div>     
   </form>                              
</div><!-- FIN DEL BODY--> 