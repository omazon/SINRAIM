<div class="panel-body">    

         <div class="form-group">
           <label class="control-label" for="">Fecha de Vencimiento</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="icon wb-calendar" aria-hidden="true"></i>
                </span>
                {!!Form::date('fechavencimiento','value', ['id' => 'fechavencimiento', 'class' => 'form-control'])!!} 
            </div> 
         </div>

        <div class="form-group">
              <label class="control-label" for="inputfabricante2">Fabricante</label>
               {!!Form::text('fabricante',null,['name'=>'fabricante[]', 'id'=>'fabricante', 'class'=>'form-control', 'data-fv-regexp'=>'true', 'data-fv-regexp-regexp'=>'^[A-Za-z0-9\s]+$', 'data-fv-regexp-message'=>'El campo solo puede tener campos alfanuméricos'])!!}
        </div> 

    
        <div class="form-group">
            <label class="control-label" for="inputfabricante2">Número de Lote</label>
            {!!Form::text('numlote',null,['name'=>'numlote[]', 'id'=>'numlote', 'class'=>'form-control'])!!}
        </div>

          <div class="form-group">
              <label class="control-label" for="">Motivo de la Prescripción</label>
             {!!Form::textarea('motivoprescripcion',null,['name'=>'motivoprescripcion[]', 'id'=>'motivoprescripcion', 'class'=>'form-control', 'style'=>'resize:none'])!!}
          </div>                 
</div><!-- FIN DEL BODY--> 