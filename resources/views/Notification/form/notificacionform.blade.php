 <div class="panel-body panel bg-blue-grey-200">
   <form>                                                      
        <div class="row">
            <div class="col-sm-12">
                 <div class="form-group">
                     {!!Form::hidden('idnotificador',$idnotificador,['name'=>'idnotificador[]', 'id'=>'idnotificador', 'class'=>'form-control'])!!}
                 </div>
            </div>
            <div class="col-sm-12">
                 <div class="form-group">
                     {!!Form::hidden('idpaciente',$idpaciente,['name'=>'idpaciente[]', 'id'=>'idpaciente', 'class'=>'form-control'])!!}
                 </div>
            </div>
            <div class="col-sm-12">
                 <div class="form-group">
                     {!!Form::hidden('estadonotificacion',null,['name'=>'estadonotificacion[]', 'id'=>'estadonotificacion', 'class'=>'form-control'])!!}
                 </div>
            </div>
             <div class="col-sm-12">
                 <div class="form-group">
                     {!!Form::hidden('idhospitalnoti',$idhospitalnoti,['name'=>'idhospitalnoti[]', 'id'=>'idhospitalnoti', 'class'=>'form-control'])!!}
                 </div>
            </div>

            <div class="col-sm-12">
                 <div class="form-group">
                    <label class="control-label" for="">¿Requirió ingreso hospitalario?</label>
                     <div class="row">
                         <div class="col-xs-4 text-center">
                            {!! Form::label('ingresosi', 'SI') !!}
                            {!! Form::radio('radio', 'SI', true, ['name' => 'ingresohospital[]',  'class'=>'icheckbox-primary', 'id' => 'ingresosi', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!} 
                         </div>
                         <div class="col-xs-4">
                             {!! Form::label('ingresono', 'NO') !!}
                             {!! Form::radio('radio', 'NO', false, ['name' => 'ingresohospital[]', 'class'=>'icheckbox-primary', 'id' => 'ingresono', 'data-plugin'=>'iCheck', 'data-radio-class'=>'iradio_flat-blue']) !!}        
                         </div>
                     </div>  
                </div>
            </div>

            <div class="form-group">
                 <div class="col-sm-12">
                       <label for="">Peso Del Paciente</label>
                       <div class="input-group">
                          {!!Form::number('pesopaciente', 'value',['name' => 'pesopaciente[]', 'id' => 'pesopaciente', 'class' => 'form-control', 'min'=>'1', 'max'=>'600', 'required', 'data-fv-between-message'=>'Debe Introducir un valor entre 1 y 600'])!!}
                           <span class="input-group-addon">
                                <i>kg</i>
                            </span>
                       </div> 
                </div>  
            </div> 
              <div class="row">
                 <div class="col-sm-12">
                    <div class="form-group">
                          <label for="factoriesgos" class="control-label">Factores de Riesgo que Asocia</label>
                          <select name="factoriesgos[]" class="form-control" multiple="multiple" id="factoriesgos" style="width: 100%;"></select>
                      </div>
                 </div>
              </div> 

        </div>     
   </form>                              
</div><!-- FIN DEL BODY--> 