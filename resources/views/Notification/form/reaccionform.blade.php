<div class="panel-body">
        <div class="form-group">
            <label class="control-label" for="inputreaccion">Nombre Reacción Adversa</label>
            {!!Form::text('nombrereaccion',null,['name'=>'nombrereaccion[]', 'id'=>'nombrereaccion', 'class'=>'form-control nombrereaccion', 'autocomplete'=>'off', 'data-fv-notempty'=>'true', 'data-fv-notempty-message'=>'Este campo no puede estar vacio'])!!}
            {!!Form::hidden('codreaccionadversa',null,['name'=>'codreaccionadversa[]', 'id'=>'codreaccionadversa', 'class'=>'form-control'])!!}
        </div>

        <div class="form-group">
            <label class="control-label" for="">Intensidad</label><br>  
            {!!Form::select('intensidadreaccion', 
            ['LEVE' => 'LEVE', 
            'MODERADA' => 'MODERADA',
            'SEVERA' => 'SEVERA'],
             null, ['name' => 'intensidadreaccion[]', 'id' => 'intensidadreaccion',  'class' => 'form-control'])!!}    
        </div>

         <label class="control-label" for="">Fechas de Inicio y Fin de toma del medicamento</label><br>  
         <div class="input-daterange" data-plugin="datepicker">
            <div class="input-group">
                <span class="input-group-addon">
                  <i class="icon wb-calendar" aria-hidden="true"></i>
                </span>
                {!!Form::text('fechainicioreaccion','', ['name' => 'fechainicioreaccion[]', 'id' => 'fechainicioreaccion', 'class' => 'form-control','placeholder'=>'DD/MM/YYYY', 'data-fv-date'=>'true', 'data-fv-date-format'=>'DD/MM/YYYY', 'data-fv-date-message'=>'Fecha no válida', 'required'])!!}
            </div>
            <div class="input-group">
                <span class="input-group-addon">hasta</span>
                {!!Form::text('fechafinreaccion','Continua medicamento', ['name' => 'fechafinreaccion[]', 'id' => 'fechafinreaccion', 'class' => 'form-control'])!!}  
            </div>
          </div>  

         
          <div class="form-group">
              <label class="control-label" for="">Desenlace</label><br>  
              {!!Form::select('desenlacereaccion', 
              ['RECUPERADO' => 'RECUPERADO', 
              'SECUELAS' => 'SECUELAS',
              'MORTAL' => 'MORTAL'],
               null, ['name' => 'desenlacereaccion[]', 'id' => 'desenlacereaccion',  'class' => 'form-control'])!!}    
          </div>  
</div><!--FIN PANEL BODY-->  