 <div class="panel-body"> 
        <div class="form-group">
            <label class="control-label" for="inputmedicamento2">Nombre Genérico Insumo Médico</label>
            {!!Form::text('nombremedicamento',null,['name'=>'nombremedicamento[]', 'id'=>'nombremedicamento', 'class'=>'form-control nombremedicamento', 'autocomplete'=>'off', 'data-fv-notempty'=>'true', 'data-fv-notempty-message'=>'Este campo no puede estar vacio'])!!}
            {!!Form::hidden('codmedicamento',null,['name'=>'codmedicamento[]', 'id'=>'codmedicamento', 'class'=>'form-control'])!!}
        </div>

        <div class="form-group">
            <div class="col-sm-6">
                   <label for="">Dósis Tomada</label>
                   {!!Form::number('dosispaciente', 'value',['name' => 'dosispaciente[]', 'id' => 'dosispaciente', 'class' => 'form-control', 'min'=>'1', 'required', 'data-fv-between-message'=>'Debe Introducir un valor entre 1 y 110'])!!}
            </div>  

            <div class="col-sm-6">
                <label class="control-label" for="">Unidad de Medida</label>
                {!!Form::select('unidadmedida_id', $unidadmedida_id, null, ['name'=>'unidadmedida_id[]', 'id' => 'unidadmedida_id', 'class'=>'form-control'] )!!}          
            </div> 
        </div> 
               
        <div class="form-group">
            <label class="control-label" for="">Frecuencia de Consumo</label><br>  
            {!!Form::select('frecuenciapaciente', 
            ['Cada 24 Horas (1 vez al dia)' => 'Cada 24 Horas (1 vez al dia)', 
            'Cada 12 Horas (2 veces al dia)' => 'Cada 12 Horas (2 veces al dia)',
             'Cada 8 Horas (3 veces al dia)' => 'Cada 8 Horas (3 veces al dia)', 
             'Cada 6 Horas (4 veces al dia)' => 'Cada 6 Horas (4 veces al dia)', 
             'Cada 4 Horas (6 veces al dia)' => 'Cada 4 Horas (6 veces al dia)'], 
             null, ['name' => 'frecuenciapaciente[]', 'id' => 'frecuenciapaciente',  'class' => 'form-control'])!!}     
        </div>
    
        <div class="form-group">
            <label class="control-label" for="">Tipo de Insumo Médico</label><br>  
            {!!Form::select('tipoinsumo', 
            ['Insumo Medico Sospechoso' => 'Insumo Medico Sospechoso', 
            'Insumo Medico Concumitante' => 'Insumo Medico Concumitante'],
             null, ['name' => 'tipoinsumo[]', 'id' => 'tipoinsumo',  'class' => 'form-control'])!!}    
        </div>
        
        <div class="form-group">
           <label class="control-label" for="">Via de Administracion</label><br>         
           {!!Form::select('viadministracion_id', $viadministracion_id, null, ['name'=>'viadministracion_id[]', 'id' => 'viadministracion_id', 'class'=>'form-control'] )!!}   
        </div>

          <label class="control-label" for="">Fechas de Inicio y Fin de toma del medicamento</label><br>  
         <div class="input-daterange" data-plugin="datepicker">
            <div class="input-group">
                <span class="input-group-addon">
                  <i class="icon wb-calendar" aria-hidden="true"></i>
                </span>
                {!!Form::text('fechainiciomedicamento','', ['name'=>'fechainiciomedicamento[]', 'id' => 'fechainiciomedicamento', 'class' => 'form-control', 'placeholder'=>'DD/MM/YYYY', 'data-fv-date'=>'true', 'data-fv-date-format'=>'DD/MM/YYYY', 'data-fv-date-message'=>'Fecha no válida', 'required'])!!} 
            </div>
            <div class="input-group">
                <span class="input-group-addon">hasta</span>
                {!!Form::text('fechafinmedicamento','Continua medicamento', ['name'=>'fechafinmedicamento[]', 'id' => 'fechafinmedicamento', 'class' => 'form-control'])!!} 
            </div>
          </div>
</div><!--FIN PANEL BODY-->      
