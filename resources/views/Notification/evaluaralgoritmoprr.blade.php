@extends('layouts.layout')

@section('title')
  Notificacion | Evaluar Notificacion
@stop
@section('css')


@section('body')
  <!-- Page -->
  <div id="tabla-medico" class="col-sm-12">
    <div class="panel col-sm-8">
        <header class="panel-heading">
            <div class="panel-actions"></div>
            <h1 class="panel-title">PANEL PARA VER DETALLES DE NOTIFICACIONES ENVIADAS</h1>
             <div class="alert alert-warning fade in">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Algoritmo PRR</strong> Es Un potente algoritmo reconocido mundialmente, que toma como referencia los medicamentos introducidos en la notificación y las reacciones adversas de la misma notificación y los compara con el resto de información en la base de datos que ayudarán al evaluador a sacar una conclusión basada en datos estadisticos.
            </div>
        </header> 
        <!-- PANEL DATOS DEL PACIENTE -->     
        <div class="panel-body">
            {!!Form::open(['route'=>['evaluaralgoritmoprr'], 'method'=>'POST'])!!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                 <div class="form-group">
                      <label class="control-label" for="inputmedicamento2">Nombre Insumo Médico</label>
                      {!!Form::text('nombremedicamento',null,['name'=>'nombremedicamento[]', 'id'=>'nombremedicamento', 'class'=>'form-control nombremedicamento', 'autocomplete'=>'off', 'data-fv-notempty'=>'true', 'data-fv-notempty-message'=>'Este campo no puede estar vacio'])!!}
                      {!!Form::hidden('codmedicamento',null,['id'=>'codmedicamento', 'class'=>'form-control'])!!}
                  </div>
                   <div class="form-group">
                        <label class="control-label" for="inputreaccion">Nombre Reacción Adversa</label>
                        {!!Form::text('nombrereaccion',null,['name'=>'nombrereaccion[]', 'id'=>'nombrereaccion', 'class'=>'form-control nombrereaccion', 'autocomplete'=>'off', 'data-fv-notempty'=>'true', 'data-fv-notempty-message'=>'Este campo no puede estar vacio'])!!}
                        {!!Form::hidden('codreaccionadversa',null,['id'=>'codreaccionadversa', 'class'=>'form-control'])!!}
                    </div></br>
                    <div class="col-sm-12 text-right">
                        {!!Form::submit('EVALUAR',['id'=>'btnevaluar', 'class'=>'btn btn-primary'])!!} 
                    </div> 
                {!!Form::close()!!} 
             <div class="row">
                <h4 class="text-center">Resultados Algoritmo PRR</h4><br><br>  
                 <table class="espacio " id="tblnombresinsumoreaccion">
                         <tr>
                              <td>
                                  <label class="lead text-muted " style="display: block; color:#3583CA; font-weight: bold;">Insumo Medico Evaluado:  </label>
                              </td>
                              <td>
                                  <h4 class="panel-title ">{{ $nombreinsumoeval }}</h4>
                              </td>
                         </tr>   
                         <tr>
                              <td>
                                  <label class="lead text-muted " style="display: block; color:#3583CA; font-weight: bold;">Reaccion Adversa Evaluada:  </label>
                              </td>
                              <td>
                                  <h4 class="panel-title ">{{ $nombrereaccioneval }}</h4>
                              </td>
                         </tr>     
                  </table><br><br>
                 <div class="col-sm-12 ">           
                          <div class="widget-content">
                                <div class="bg-blue-600 white padding-10">
                                    <div class="font-size-18 text-nowrap text-center">
                                        TOTAL DE NOTIFICACIONES EXISTENTES
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $notifications }}</label>
                                    </div>            
                                </div>
                          </div>          
                  </div>
            </div><br>
            <div class="row" id="panelevaluacion">
                  <div class="col-sm-6 ">              
                          <div class="widget-content">
                                <div class="bg-green-600 white padding-10">
                                     <div class="font-size-18 text-nowrap text-center">
                                        TOTAL DE COINCIDENCIAS INSUMO Y REACCION
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $totalcoincidencia }}</label>
                                    </div>          
                                </div>
                          </div>          
                  </div>
                  <div class="col-sm-6 ">          
                          <div class="widget-content">
                              <div class="bg-orange-600 white padding-10">
                                   <div class="font-size-18 text-nowrap text-center">
                                        TOTAL DE NOTIFICACIONES POR INSUMO MEDICO
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $totalnotinsumo }}</label>
                                    </div>                                              
                              </div>
                        </div>          
                   </div>
            </div><br>
            <div class="row"> 
                   <div class="col-sm-6 ">          
                          <div class="widget-content">
                                <div class="bg-red-600 white padding-10">
                                     <div class="font-size-16 text-nowrap text-center">
                                        TOTAL DE NOTIFICACIONES POR REACCION ADVERSA
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $totalnotireaccion }}</label>
                                    </div>    
                                </div>
                          </div>          
                  </div>
                   <div class="col-sm-6 ">           
                          <div class="widget-content">
                                <div class="bg-brown-600 white padding-10">
                                    <div class="font-size-18 text-nowrap text-center">
                                        TOTAL NOTIFICACION SIN REACCION E INSUMO
                                    </div>
                                    <div class="text-center font-size-70">
                                        <label>{{ $conteoresultadonocoincidencias }}</label>
                                    </div>            
                                </div>
                          </div>          
                  </div>  
             </div><br> 
              
                 @if ($algoritmoPRR >= 1 and $algoritmoPRR <= 2 and $chicuadrado >=4 and $notifications>3)
                       <div class="col-sm-12" id="resultadofinal">           
                              <div class="widget-content">
                                    <div class="bg-orange-600 white padding-10">
                                        <div class="font-size-18 text-nowrap text-center">                                        
                                             <label class="lead text-muted " style="display: block; color:#424242; font-weight: bold;">Señal Media</label>
                                        </div>
                                        <div class="text-center font-size-70">
                                            <i class="icon fa-eye" aria-hidden="true"></i>
                                            <label class="lead text-muted " style="display: block; color:#424242; font-weight: bold;">La probabilidad de causa del insumo con la reaccion adversa se encuentra por encima de la media. </label>
                                        </div>            
                                    </div>
                              </div>          
                      </div>
                  @elseif ($algoritmoPRR > 2 and $chicuadrado >=4 and $notifications>3)
                      <div class="col-sm-12" id="resultadofinal">           
                              <div class="widget-content">
                                    <div class="bg-red-600 white padding-10">
                                         <div class="font-size-18 text-nowrap text-center">                                        
                                             <label class="lead text-muted " style="display: block; color:#424242; font-weight: bold;">Señal Fuerte</label>
                                        </div>
                                        <div class="text-center font-size-70">
                                            <i class="icon fa-ambulance" aria-hidden="true"></i>
                                            <label class="lead text-muted " style="display: block; color:#424242; font-weight: bold;">La probabilidad de causa del insumo con la reaccion adversa se encuentra en niveles muy altos favor tomar medidas rapidamente. </label>
                                        </div>           
                                    </div>
                              </div>          
                      </div>
                  @else
                      <div class="col-sm-12" id="resultadofinal">           
                              <div class="widget-content">
                                    <div class="bg-green-600 white padding-10">
                                        <div class="font-size-18 text-nowrap text-center">                                        
                                             <label class="lead text-muted " style="display: block; color:#424242; font-weight: bold;">Señal Debil</label>
                                        </div>
                                        <div class="text-center font-size-70">
                                            <i class="icon fa-thumbs-o-up" aria-hidden="true"></i>
                                            <label class="lead text-muted " style="display: block; color:#424242; font-weight: bold;">La probabilidad de causa del insumo con la reaccion adversa se encuentra en niveles muy bajos. </label>
                                        </div>           
                                    </div>
                              </div>          
                      </div>
                  @endif               
                
       </div><!-- FIN PANEL BODY -->         
    </div><!-- FIN PANEL BOOTSTRAP -->  
 </div><!-- FIN PAGE PRINCIPAL -->  

<!--AYUDA POR TOUR-->
<div class="site-action">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon wb-help-circle animation-scale-up" aria-hidden="true"></i>
    </button>
</div>         
<!--FIN AYUDA POR TOUR-->   

 @section('javascript')
  @section('scripts')   
  @endsection

  @section('footer')
    @push('scripts')
        {!!Html::script('js/ayuda/evaluarglobal-tour.js')!!} 
         <script type="text/javascript">
////////////////////////////////////////////////////////////////////////////////////////////////////////
          //Autocompletado del Insumo Medico
          var path = "{{URL::route('autocompletemedicamento')}}";
          //Enviado del contenido del textbox al controlador para obtener el autocompletado de la BD
          $('.nombremedicamento').typeahead({
              source:  function (query, process) {
               $.get(path, { query: query }, function (data) {
                        return process(data);
                  }), 'json';
              },
              minLength:2,
              hint: true,
              highlight: true, 
               //Guardar en el mismo texbox el valor seleccionado
              updater: function(item) { 
                    $("#codmedicamento").val(item.id);
                    return item;
                }
             
          });
////////////////////////////////////////////////////////////////////////////////////////////////////////  
          //Autocompletado de las Reacciones Adversas
          var path1 = "{{URL::route('autocomplete')}}";
          //Enviado del contenido del textbox al controlador para obtener el autocompletado de la BD
          $('.nombrereaccion').typeahead({
              source:  function (query, process) {
               $.get(path1, { query: query }, function (data) {
                        return process(data);
                  }), 'json';
              },
              minLength:2,
              hint: true,
              highlight: true, 
              //Guardar en el mismo texbox el valor seleccionado 
              updater: function(item) { 
                    $("#codreaccionadversa").val(item.id);
                    return item;
                }
             
          });  
         </script>
      
    @endpush
@endsection
    
@stop

