/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function($) {
    Site.run();
  });

  // Example Validataion Full
  // ------------------------
  (function() {
    $('#exampleFullForm').formValidation({
      framework: "bootstrap",
      button: {
        selector: '#validateButton1',
        disabled: 'disabled'
      },
      icon: null,
      fields: {
        username: {
          validators: {
            notEmpty: {
              message: 'El usuario es requerido'
            },
            stringLength: {
              min: 6,
              max: 30
            },
            regexp: {
              regexp: /^[a-zA-Z0-9]+$/
            }
          }
        },
        email: {
          validators: {
            notEmpty: {
              message: 'El usuario es requerido'
            },
            emailAddress: {
              message: 'El correo electrónico no es válido'
            }
          }
        },
        password: {
          validators: {
            notEmpty: {
              message: 'La contraseña es requerida'
            },
            stringLength: {
              min: 8
            }
          }
        },
        birthday: {
          validators: {
            notEmpty: {
              message: 'La fecha de nacimiento es requerida'
            },
            date: {
              format: 'YYYY/MM/DD'
            }
          }
        },
        github: {
          validators: {
            url: {
              message: 'La URL no es válida'
            }
          }
        },
        skills: {
          validators: {
            notEmpty: {
              message: 'Las habilidades son requeridas'
            },
            stringLength: {
              max: 300
            }
          }
        },
        porto_is: {
          validators: {
            notEmpty: {
              message: 'Favor, especifique al menos una'
            }
          }
        },
        'for[]': {
          validators: {
            notEmpty: {
              message: 'Favor, especifique al menos una'
            }
          }
        },
        company: {
          validators: {
            notEmpty: {
              message: 'Compañia por favor'
            }
          }
        },
        browsers: {
          validators: {
            notEmpty: {
              message: 'Favor, especifique al menos un navegador de uso diario como desarrollador'
            }
          }
        }
      }
    });
  })();

  // Example Validataion Constraints
  // -------------------------------
  (function() {
    $('#exampleConstraintsForm, #exampleConstraintsFormTypes').formValidation({
      framework: "bootstrap",
      icon: null,
      fields: {
        type_email: {
          validators: {
            emailAddress: {
              message: 'El dirección de correo no es válida'
            }
          }
        },
        type_url: {
          validators: {
            url: {
              message: 'La URL no es válida'
            }
          }
        },
        type_digits: {
          validators: {
            digits: {
              message: 'Los valores no son dígitos'
            }
          }
        },
        type_numberic: {
          validators: {
            integer: {
              message: 'El valor no es un número'
            }
          }
        },
        type_phone: {
          validators: {
            phone: {
              message: 'El valor no es un teléfono'
            }
          }
        },
        type_credit_card: {
          validators: {
            creditCard: {
              message: 'La tarjeta de crédito no es válida'
            }
          }
        },
        type_date: {
          validators: {
            date: {
              format: 'YYYY/MM/DD'
            }
          }
        },
        type_color: {
          validators: {
            color: {
              type: ['hex', 'hsl', 'hsla', 'keyword', 'rgb', 'rgba'], // The default value for type
              message: 'El valor no es un color válido'
            }
          }
        },
        type_ip: {
          validators: {
            ip: {
              ipv4: true,
              ipv6: true,
              message: 'El valor no es una IP válida(v4 o v6)'
            }
          }
        }
      }
    });
  })();

  // Example Validataion Standard Mode
  // ---------------------------------
  (function() {
    $('#exampleStandardForm').formValidation({
      framework: "bootstrap",
      button: {
        selector: '#validateButton2',
        disabled: 'disabled'
      },
      icon: null,
      fields: {
        standard_fullName: {
          validators: {
            notEmpty: {
              message: 'El nombre completo es requerido y no puede estar vacío'
            }
          }
        },
        standard_email: {
          validators: {
            notEmpty: {
              message: 'La correo electrónico es requerido y no puede estar vacío'
            },
            emailAddress: {
              message: 'El dirección de correo no es válida'
            }
          }
        },
        standard_content: {
          validators: {
            notEmpty: {
              message: 'El contenido es requerido y no puede estar vacío'
            },
            stringLength: {
              max: 500,
              message: 'El contenido debe tener menos de 500 caracteres'
            }
          }
        }
      }
    });
  })();

  // Example Validataion Summary Mode
  // -------------------------------
  (function() {
    $('.summary-errors').hide();

    $('#exampleSummaryForm').formValidation({
      framework: "bootstrap",
      button: {
        selector: '#validateButton3',
        disabled: 'disabled'
      },
      icon: null,
      fields: {
        summary_fullName: {
          validators: {
            notEmpty: {
              message: 'El nombre completo es requerido y no puede estar vacío'
            }
          }
        },
        summary_email: {
          validators: {
            notEmpty: {
              message: 'La dirección de correo es requerida y no puede estar vacía'
            },
            emailAddress: {
              message: 'El dirección de correo no es válida'
            }
          }
        },
        summary_content: {
          validators: {
            notEmpty: {
              message: 'El contenido es requerido y no puede estar vacío'
            },
            stringLength: {
              max: 500,
              message: 'El contenido debe tener menos de 500 caracteres'
            }
          }
        }
      }
    })

    .on('success.form.fv', function(e) {
      // Reset the message element when the form is valid
      $('.summary-errors').html('');
    })

    .on('err.field.fv', function(e, data) {
      // data.fv     --> The FormValidation instance
      // data.field  --> The field name
      // data.element --> The field element
      $('.summary-errors').show();

      // Get the messages of field
      var messages = data.fv.getMessages(data.element);

      // Remove the field messages if they're already available
      $('.summary-errors').find('li[data-field="' + data.field + '"]').remove();

      // Loop over the messages
      for (var i in messages) {
        // Create new 'li' element to show the message
        $('<li/>')
          .attr('data-field', data.field)
          .wrapInner(
            $('<a/>')
            .attr('href', 'javascript: void(0);')
            // .addClass('alert alert-danger alert-dismissible')
            .html(messages[i])
            .on('click', function(e) {
              // Focus on the invalid field
              data.element.focus();
            })
          ).appendTo('.summary-errors > ul');
      }

      // Hide the default message
      // $field.data('fv.messages') returns the default element containing the messages
      data.element
        .data('fv.messages')
        .find('.help-block[data-fv-for="' + data.field + '"]')
        .hide();
    })

    .on('success.field.fv', function(e, data) {
      // Remove the field messages
      $('.summary-errors > ul').find('li[data-field="' + data.field + '"]').remove();
      if ($('#exampleSummaryForm').data('formValidation').isValid()) {
        $('.summary-errors').hide();
      }
    });
  })();
})(document, window, jQuery);
