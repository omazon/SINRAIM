/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
$.components.register("asSpinner", {
  mode: "default",
  defaults: {
    namespace: "spinnerUi",
    skin: null,
    min: "0",
    max: 200,
    mousewheel: true
  }
});