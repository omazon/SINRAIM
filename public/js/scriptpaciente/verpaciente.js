//var urlsite ="http://localhost:8000";
var urlsite ='http://sinraim.omarboza.com';

function Eliminar(btn){
	var route = urlsite+"/paciente/"+btn+"";
	var token = $("#token").val();
	var table = $('#leerpacientes').DataTable();
	$('#leerpacientes tbody').on('click',"button[id='eliminarpaciente']",function() {
	    console.log(table.row($(this).parents('tr')));
	                
	    table
	        .row( $(this).parents('tr'))
	        .remove()
	        .draw();
	});

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'DELETE',
		dataType: 'json',
		
		success:function(){
			$("#msj-success-elim").fadeIn();

		}
	});

};


function Mostrar(btn){
	console.log(btn);
	var route = urlsite+"/paciente/"+btn+"/edit";

	$.get(route, function(res){
		$("#name").val(res.name);
		$("#id").val(res.id);
		$("#last_name").val(res.last_name);
		$("#fechanacimiento").val(res.fechanacimiento);
		$("#edad").val(res.edad);		
		$("#genero").val(res.genero);

	});
};


$("#actualizar").click(function(){
	var value = $("#id").val();
	var name = $("#name").val();
	var last_name = $("#last_name").val();
	var fechanacimiento = $("#fechanacimiento").val();
	var edad = $("#edad").val();	
	var genero = $("#genero").val();

	var route = urlsite+"/paciente/"+value+"";
	var token = $("#token").val();

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: {name: name, last_name: last_name, fechanacimiento: fechanacimiento, edad: edad, genero: genero},

		success:function(){			
			$("#pacientesidebar").modal('toggle');
			location.reload();
			$("#msj-success-act").fadeIn();
		}
	});

});

(function ($) {
  $.fn.serializeFormJSON = function () {
      var o = {};
      var a = this.serializeArray();
      $.each(a, function () {
          if (o[this.name]) {
              if (!o[this.name].push) {
                  o[this.name] = [o[this.name]];
              }
              o[this.name].push(this.value || '');
          } else {
              o[this.name] = this.value || '';
          }
      });
      return o;
  };
})(jQuery);

$('#mostrarmedicamento').submit(function (e) {
  e.preventDefault();
 
  var token = $("#token").val();
  var data= $(this).serializeFormJSON();
  console.log(data);
  /* Object
      email: "value"
      name: "value"
      password: "value"
   */
   	var data = JSON.stringify(data);
   	//console.log(data);
	var route = urlsite+"/vermedicamento";
	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'post',
		data: data,
		contentType: "json",
    	processData: false,
		//data: {familiar: familiar, edad: edad, patient_id: patient_id},
		

		success:function(){
			$("#OtrosInsumos").modal('toggle');
			console.log(data);
			//$("#central").load('inc/gallery.php');
			$("#msj-success").fadeIn();
		},
		error:function(msj){
			$("#msj-error").fadeIn();
		}
	});
});










