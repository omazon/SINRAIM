//JavaScript para controlar CRUD de Antecedentes Familiares

//var urlsite ="http://localhost:8000";
//alert(urlsite);
var urlsite ='http://sinraim.omarboza.com';

function Eliminar(btn){
	var route = urlsite+"/historia/"+btn+"";
	var token = $("#token").val();
	var table = $('#verhistoria').DataTable();
	$('#verhistoria tbody').on('click',"button[id='eliminarantecedente']",function() {
	    console.log(table.row($(this).parents('tr')));
	                
	    table
	        .row( $(this).parents('tr'))
	        .remove()
	        .draw();
	});

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'DELETE',
		dataType: 'json',
		
		success:function(){
			//$("#msj-success-elim").fadeIn();

		}
	});

};


function Mostrar(btn){
	console.log(btn);
	var route = urlsite+"/historia/"+btn+"/edit";

	$.get(route, function(res){
		$("#familiar").val(res.familiar);
		$("#id").val(res.id);
		$("#patient_id").val(res.patient_id);
		$("#edad").val(res.edad);
		$("#vida").val(res.vida);
		$("#descripcion_id").val(res.descripcion_id);

	});
};


$("#actualizar").click(function(){
	var value = $("#id").val();
	var patient_id = $("#patient_id").val();
	var familiar = $("#familiar").val();
	var edad = $("#edad").val();
	var vida = $("#vida").val();
	var descripcion_id = $("#descripcion_id").val();

	var route = urlsite+"/historia/"+value+"";
	var token = $("#token").val();

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: {familiar: familiar, edad: edad, vida: vida, descripcion_id: descripcion_id},

		success:function(){			
			$("#antecedentesidebar").modal('toggle');
			location.reload();
			$("#msj-success-act").fadeIn();
		}
	});

});



$("#registroantecedentes").click(function(){

	var token = $("#token").val();
	var TableData = new Array();
	    $('#tablantecedentes #leerhistoria tr').each(function(row, tr){
	        TableData[row]={
	            "patient_id" : $(tr).find('#patient_id').val()
	            , "familiar" :$(tr).find('#familiar').val()
	            , "edad" : $(tr).find('#edad').val()
	            , "vida" : $(tr).find('#vida').val()
	            , "descripcion_id" : $(tr).find('.descripcion_id').val()
	        }    
	    }); 
	    TableData.shift();  // first row will be empty - so remove
	    //console.log(TableData);

	var TableData = JSON.stringify(TableData);
	//console.log(TableData);
    //alert(TableData);
	//var familiar = $("#familiar").val();
	//var edad = $("#edad").val();
	//var patient_id = $("#patient_id").val();
	var route = urlsite+"/historia";
	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'POST',
		data: TableData,
		contentType: "json",
    	processData: false,
		//data: {familiar: familiar, edad: edad, patient_id: patient_id},
		

		success:function(){
			$("#nuevo-historial").modal('toggle');
			location.reload();
			$("#msj-success").fadeIn();
		},
		error:function(msj){
			$("#msj-error").fadeIn();
		}
	});
});


