/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('#ayudausuarios').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [
    {
      element: "#graf1",
      intro: "Informes Gráficos<p class='content'>Puedes visualizar cada uno de los graficos presentados y en la parte superior derecha puedes descargarlo en el archivo de tu preferencia.</p>"
    },
    {
      element: "#graf3",
      position:"left",
      intro: "Informes Gráficos <p class='content'>Cada grafico puede ser agregado a cualquier presentacion para proyectar la rapida informacion que brinda el sistema.</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
