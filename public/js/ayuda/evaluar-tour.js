/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [{
      element: "#insumoarray",
      intro: "Insumo médico notificado <p class='content'>Lo insumos médicos que fueron introducidos en la notificacion</p>"
    },{
      element: "#reaccionarray",
      intro: "Reacción notificada <p class='content'>Las reacciones adversas que fueron introducidas en la notificación</p>"
    },{
      element: "#btn-submit",
      intro: "Evaluar <p class='content'>Luego de escoger las reacciones y medicamentos, haz click en evaluar para obtener los resultados</p>"
    },{
      element: "#estadofinalnoti",
      intro: "Decisión final <p class='content'>Aunque el sistema le indique el posible estado de esa reacción, al final es el evaluador el que tiene la última palabra</p>"
    },{
      element: "#motivocambioestado",
      intro: "¿Qué paso? <p class='content'>Un descripción del porque cambio de estado, usando uno diferente al que el sismte proporcionó</p>"
    },{
      element: "#btn-submit-save",
      position: 'top',
      intro: "Guardar<p class='content'>AL guardar damos por terminada la evaluación de la notificación</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
