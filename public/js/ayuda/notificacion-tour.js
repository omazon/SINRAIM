/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [{
      element: "#insumo-medico",
      intro: "Primer Paso <p class='content'>Primero debes llenar el formulario correspondiente a la información del insumo médico</p>"
    },{
      element: "#add-insumo-medico",
      intro: "Agrega <p class='content'>Puedes agregar tantos insumos médicos como te sean necesario</p>"
    },{
      element: "#reaccion-adversa",
      intro: "Segundo paso <p class='content'>Ahora toca la información sobre la reaccion adversa </p>"
    },{
      element: "#confi",
      intro: "Último paso <p class='content'>Solo llena este formulario para que tu nueva notificación este lista</p>"
    }    ],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
