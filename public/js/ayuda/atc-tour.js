/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [{
      element: "#nombremedicamento",
      intro: "Ingresar Nombre Insumo Medico <p class='content'>Agregar el nombre del insumo del cual desea buscar el codigo ATC estableccido por la OMS</p>"
    },{
      element: "#btnbuscaratc",
      position: "top",
      intro: "Realizar busqueda <p class='content'>Al presionar el boton se buscara el insumo medico ingresado en la lista de medicamentos del ATC</p>"
    },{
      element: "#leeratc",
      position: "left",
      intro: "Ver informacion <p class='content'>En esta seccion puedes ver el resultado de la busqueda de los insumos anteriores</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
