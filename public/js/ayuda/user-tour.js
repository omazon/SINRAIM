/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('#ayudausuarios').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [
    {
      element: "#importarusuario",
      intro: "Importar usuarios masivamente <p class='content'>Al seleccionar esta opcion debes elegir el archivo CSV que contenga las columnas indicadas para importar usuarios masivamente</p>"
    },
    {
      element: "#exampleGroupDrop1",
      intro: "Descargar lista de usuarios <p class='content'>Elige esta opcion si deseas descargar en formato de excel o PDF la lista de los usuarios con permisos para usar el sistema</p>"
    },
    {
      element: "#addnewuser",
      intro: "Agregar un nuevo usuario <p class='content'>Este boton te permite agregar un usuario de manera individual, agregando cada uno de los campos del formulario</p>"
    },
    {
      element: "#accion",
      intro: "Botones de acción <p class='content'>Este panel permite editar y eliminar el usuario de manera individual</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
