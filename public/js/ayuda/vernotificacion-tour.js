/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('#ayudausuarios').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [
    {
      element: "#vernotificacion",
      position: "left",
      intro: "Tabla de Notificaciones enviadas <p class='content'>Aca podemos tener acceso a las notificaciones enviadas por los profesionales de la salud, se puede realizar busquedas, ver mas detalles de manera individual, etc</p>"
    },
    {
      element: "#estadonotiver",
      intro: "Estado de la notificacion <p class='content'>Este panel permite ver el estado que se encuentra la notificacion , ya sea cuando es enviada por primera vez, asi como cuando es cambiado por el evaluador, cada etiqueta varia en color en dependencia de su estado</p>"
    },
    {
      element: "#accion",
      intro: "Botones de acción <p class='content'>Este panel permite ver y evaluar cada notificacion de manera individual, es decir ver mas detalles y evaluar mediante datos estadisticos la notificacion</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
