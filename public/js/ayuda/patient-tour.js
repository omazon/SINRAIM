/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [
    {
      element: "#add-patient",
      intro: "Agregar nuevo paciente <p class='content'>Despliegas el formulario con la información para un nuevo paciente</p>"
    },
    {
      element: "#accion",
      intro: "Botones de acción <p class='content'>Esto permite editar el paciente, agregar los antecedentes familiares, eliminar o enviar un notificacioón usando como datos el paciente seleccionado</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
