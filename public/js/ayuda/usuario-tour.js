/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})

  $.configs.set('tour', {
    steps: [{
      element: "#cabecera-perfil",
      intro: "Foto de portada <p class='content'>Una estática que adorna tu perfil</p>"
    },{
      element: ".widget-header",
      position: 'right',
      intro: "Tú <p class='content'>Imagen de perfil que te identifica en todo el sistema</p>"
    },{
      element: ".widget-footer",
      position: 'right',
      intro: "Información básica <p class='content'>Información básica sobre ti como Nombre, Profesiones y Especialidades</p>"
    },{
      element: "#btn-config",
      position: 'top',
      intro: "Configuraciones <p class='content'>Botones de configuración tales como cambiar la foto de perfil o la de portada</p>"
    },{
      element: ".nav.nav-tabs li:nth-child(1)",
      intro: "Datos Básicos<p class='content'>Modificar el password</p>"
    },{
      element: ".nav.nav-tabs li:nth-child(2)",
      intro: "Más Información<p class='content'>Ver y Modificar Información como email y centro de salud</p>"
    },{
      element: ".nav.nav-tabs li:nth-child(3)",
      intro: "Profesión<p class='content'>Modificar las profesiones y Especialidades </p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
