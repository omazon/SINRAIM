/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [{
      element: "#importarusuario",
      intro: "Importar Usuarios CSV <p class='content'>De click en este espacio o arrastre un archivo CSV para importar masivamente usuarios a la base de datos, recuerde que el archivo debe cumplir con los datos necesarios para ser guardado con exito</p>"
    },{
      element: "#btnguardarcsvusuario",
      intro: "Guardar Usuario Masivamente <p class='content'>Luego de tener el archivo CSV presione guardar para almacenar masivamente los usuarios</p>"
    },{
      element: "#importarATC",
      intro: "Importar ATC CSV <p class='content'>De click en este espacio o arrastre un archivo CSV para importar masivamente insumos medicos a la base de datos, recuerde que el archivo debe cumplir con los datos necesarios para ser guardado con exito</p>"
    },{
      element: "#btnguardarcsvatc",
      position: 'top',
      intro: "Guardar ATC Masivamente <p class='content'>Luego de tener el archivo CSV presione guardar para almacenar masivamente los insumos medicos nuevos</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
