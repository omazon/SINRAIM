/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
 $.configs.set('tour', {
    steps: [{
      element: "#nombrereaccion",
      intro: "Ingresar Nombre Reaccion Adversa <p class='content'>Agregar el nombre de la reaccion adversa de la cual se desea buscar el codigo Meddra estableccido por la OMS</p>"
    },{
      element: "#btnbuscarmeddra",
      position: "top",
      intro: "Realizar busqueda <p class='content'>Al presionar el boton se buscara la reaccion adversa ingresada en la lista de reacciones del Meddra</p>"
    },{
      element: "#leermeddra",
      position: "left",
      intro: "Ver informacion <p class='content'>En esta seccion puedes ver el resultado de la busqueda de las reacciones adversas anteriores</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });


})(window, document, $);
