/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('#ayudausuarios').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [
    {
      element: "#descargarnotificacion",
      intro: "Descargar notificacion<p class='content'>Al seleccionar esta opcion puedes descargar la notificacion que elegiste en formato de excel</p>"
    },
    {
      element: "#paneltab",
      intro: "Seleccionar entre las opciones <p class='content'>Puedes elegir ver mas detalles de la notificacion seleccionando cualquiera de estas opciones, puedes ver el insumo medico, la reaccion adversa, paciente, profesional que notifico y factores de riesgos asociados a la notificacion</p>"
    },
    {
      element: "#panelprincipal",
      position:"left",
      intro: "Vista de los detalles <p class='content'>Al seleccionar uno de los tab anteriores puedes ver el detalle de cada uno en esta seccion</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
