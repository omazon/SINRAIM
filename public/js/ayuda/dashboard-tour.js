/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [{
      element: "#mapa",
      position: "right",
      intro: "Mapa de los Departamentos <p class='content'>Puedes hacer click a los diferentes Departamentos para saber las notificaciones registradas en ese lugar</p>"
    }, {
      element: "#btndepartamento",
      intro: "BUSCA <p class='content'>Despues de haber seleccionado un departamento, haz click acá paraobtener los datos relacionados</p>"
    }, {
      element: "#vistaprobabilidad",
      intro: "Información relevante <p class='content'>Aquí se encuentra Información importante acerca de las notificaciones del departamento seleccionado</p>"
    },{
      element: "#topinsumosnoti",
      position: 'left',
      intro: "Top de Insumos Médicos<p class='content'>Se encuentran cuales son los medicamentos más notificados en orden descendente</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
