/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [{
      element: "#btnveratc",
      intro: "Ver Diccionario ATC <p class='content'>Necesitas referencias de como suministrar un insumo medico, puedes ver esta seccion para obtener informacion del diccionario de medicamentos ATC</p>"
    },{
      element: "#btnvermeddra",
      position: "top",
      intro: "Ver Diccionario Meddra<p class='content'>Necesitas referencias de alguna reaccion adversa, puedes ver esta seccion para obtener informacion del diccionario de reacciones adversas Meddra</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
