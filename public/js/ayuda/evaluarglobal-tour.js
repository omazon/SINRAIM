/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';
$('.site-action-toggle').click(function(){alert('Bienvenido al Modo de Ayuda');})
  $.configs.set('tour', {
    steps: [{
      element: "#nombremedicamento",
      intro: "Insumo Médico <p class='content'>Elija cualquier insumo medico del cual sospeche que causa una reaccion adversa</p>"
    },{
      element: "#nombrereaccion",
      intro: "Reacción Adversa <p class='content'>Elija cualquier reaccion adversa que quiera evaluar con el insumo medico anterior</p>"
    },{
      element: "#btnevaluar",
      intro: "Evaluar <p class='content'>Luego de escoger las reacciones e insumos, haz click en evaluar para obtener los resultados</p>"
    },{
      element: "#tblnombresinsumoreaccion",
      intro: "Insumo Medico Y Reaccion Adversa Evaluada <p class='content'>Se muestran las reacciones e insumos que fueron elegidos para ser evaluados</p>"
    },{
      element: "#panelevaluacion",
      position: 'top',
      intro: "Paneles de Evaluacion <p class='content'>Se muestran los paneles de la informacion contenida en la base de datos para realizar los calculos de evaluacion</p>"
    },{
      element: "#resultadofinal",
      position: 'top',
      intro: "Deteccion de señal <p class='content'>Se muestran los resultados de la evaluacion estadistica mediante el tipo de señal detectada verde para señales debiles, amarillo para señales medias y rojo para señales fuertes</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
