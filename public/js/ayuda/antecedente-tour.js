/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(window, document, $) {
  'use strict';

  $.configs.set('tour', {
    steps: [{
      element: "#familiar",
      intro: "Parentesco <p class='content'>El tipo de Parentesco que tiene con el paciente seleccionado</p>"
    },{
      element: "#padecimiento",
      intro: "padecimiento <p class='content'>Que tipo de enfermedad tenia/tiene el familiar</p>"
    },{
      element: "#vivo",
      intro: "¡Está con vida? <p class='content'>¿Este familiar se encuentre vivo?</p>"
    },{
      element: "#accion",
      intro: "Eliminar <p class='content'>Permite borrar el familiar</p>"
    }],
    skipLabel: "<i class='wb-close'></i>",
    doneLabel: "<i class='wb-close'></i>",
    nextLabel: "Siguiente <i class='wb-chevron-right-mini'></i>",
    prevLabel: "<i class='wb-chevron-left-mini'></i>Anterior",
    showBullets: false
  });

})(window, document, $);
