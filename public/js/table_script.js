
var counter = 0;

 function addRow(tableID) {

            var table = document.getElementById(tableID);

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            var colCount = table.rows[0].cells.length;


            for(var i=0; i<colCount; i++) {

                var newcell = row.insertCell(i);

                newcell.innerHTML = table.rows[0].cells[i].innerHTML;
                //alert(newcell.childNodes);
                
                switch(newcell.childNodes[0].type) {
                    case "text":
                            newcell.childNodes[0].value = "";
                            break;
                    case "checkbox":
                            newcell.childNodes[0].checked = false;
                            break;
                    case "select-one":
                            newcell.childNodes[0].selectedIndex = 0;
                            break;
                }  

            }

                counter++;
                addrowclick(counter); 
               
                
            
        }



        function deleteRow(tableID) {
            try {
            var table = document.getElementById(tableID);
            var Count = table.rows.length;

            for(var i=0; i<Count; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if(null != chkbox && true == chkbox.checked) {
                   
                    table.deleteRow(i);
                    Count--;
                    i--;
                }


            }
            }catch(e) {
                alert(e);
            }
        }



