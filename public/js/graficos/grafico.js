$(function () {
    // Create the chart
 var url = "informespacientes";
   $.get(url,function(resul)
   {
        var datos =jQuery.parseJSON(resul);
        var nombre = datos.name;    
        //options.title.text.push(nombre);
        //options.series[0].data[0].y.push(datos.edad);
        var options = {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Cantidad de Pacientes por Género'
        },
        xAxis: {
            title:{
                text: 'Género'
            }
        },
        yAxis: {
            title: {
                text: 'Total de Notificaciones'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
        },

        series: [{
            name: 'Sexos',
            colorByPoint: true,
            data: [{
                name: 'Mujer',
                y: datos[0].cantidad
            }, {
                name: 'Hombre',
                y: datos[1].cantidad
            }]
        }],
   };
   $('#graf1').highcharts(options);
   });
});
$(function () {
    var url = 'informesdepartamentos';
     $.get(url,function(resul){
        var datos =jQuery.parseJSON(resul); 
        var options ={
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Notificaciones por Departamento'
            },
            xAxis: {
                categories: [],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Notificaciones recibidas',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' notificaciones'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Cantidad',
                data: []
            }],
        };
         for (var i = 0; i < datos.length; i++) {
            options.xAxis.categories.push(datos[i].name);
            options.series[0].data.push(datos[i].nombre);
        }
        $('#graf2').highcharts(options);
    });   
});
$(function () {
     var url = 'informesanhos';
     $.get(url,function(resul){
        var datos =jQuery.parseJSON(resul);
        //console.log(datos);
        var options ={
                 chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Notificaciones por Mes'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: []
            }]
        };

        for (var i = 0; i < datos.length; i++) {
            var objetos = {name:datos[i].mes,y:datos[i].cuenta};
           options.series[0].data.push(objetos);
        };
        $('#graf3').highcharts(options);    
    });
});
$(function () {
    var url = 'informescuatro';
    $.get(url,function(resul){
        var datos=jQuery.parseJSON(resul);
        var options={
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: 0,
                plotShadow: false
            },
            title: {
                text: 'Mayores medicamentos recetados en el mes actual',
                align: 'center',
                verticalAlign: 'middle',
                y: 100
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        distance: -50,
                        style: {
                            fontWeight: 'bold',
                            color: 'white'
                        }
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%']
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                innerSize: '50%',
                data: [ ]
                }] 
            };
             for (var i = 0; i < datos.length; i++) {
                var objetos = [datos[i].descripcion,datos[i].conteo];
               options.series[0].data.push(objetos);
            };
             $('#graf4').highcharts(options);
        });
});

