//var urlsite ="http://localhost:8000";
var urlsite ='http://sinraim.omarboza.com';


function Eliminar(btn){
	var route = urlsite+"/usuario/"+btn+"";
	var token = $("#token").val();
	var table = $('#leerusuarios').DataTable();
	$('#leerusuarios tbody').on('click',"button[id='eliminarusuario']",function() {
	    console.log(table.row($(this).parents('tr')));
	                
	    table
	        .row( $(this).parents('tr'))
	        .remove()
	        .draw();
	});

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'DELETE',
		dataType: 'json',
		
		success:function(){
			$("#msj-success-elim").fadeIn();
		}
	});

};


function Mostrar(btn){
	console.log(btn);
	var route = urlsite+"/usuario/"+btn+"/edit";

	$.get(route, function(res){
		$("#name").val(res.name);
		$("#id").val(res.id);
		$("#last_name").val(res.last_name);
		$("#username").val(res.username);
		$("#password").val(res.password);		
		$("#genero").val(res.genero);
		$("#telefono").val(res.telefono);
		$("#email").val(res.email);
		$("#hospital_id").val(res.hospital_id);
		$("#roles_id").val(res.roles_id);

	});
};


$("#actualizar").click(function(){
	var value = $("#id").val();
	var name = $("#name").val();
	var last_name = $("#last_name").val();
	var username = $("#username").val();
	var password = $("#password").val();	
	var genero = $("#genero").val();
	var telefono = $("#telefono").val();
	var email = $("#email").val();
	var hospital_id = $("#hospital_id").val();
	var roles_id = $("#roles_id").val();

	var route = urlsite+"/usuario/"+value+"";
	var token = $("#token").val();

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'PUT',
		dataType: 'json',
		data: {name: name, last_name: last_name, username: username, password: password, genero: genero, telefono: telefono, email: email, hospital_id: hospital_id, roles_id: roles_id},

		success:function(){			
			$("#medicosidebar").modal('toggle');
			location.reload();
			$("#msj-success-act").fadeIn();
		}
	});

});


