$("#registro").click(function(){
	var name = $("#name").val();
	var last_name = $("#last_name").val();
	var username = $("#username").val();
	var password =  $("#password").val();
	var genero_id = $("#genero_id").val();
	var telefono = $("#telefono").val();
	var email = $("#email").val();
	var hospital_id = $("#hospital_id").val();

	var route = "http://sinraim.omarboza.com/usuario";
	var token = $("#token").val();

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': token},
		type: 'POST',
		dataType: 'json',
		data: {name: name, last_name: last_name, username: username, password: password, genero_id: genero_id, telefono: telefono, email: email, hospital_id: hospital_id},

		success:function(){
			$("#msj-success").fadeIn();
		},
		error:function(msj){
			$("#msj-error").fadeIn();
		}
	});
});