<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('edad');
            $table->string('familiar');
            $table->string('vida');
            $table->timestamps(); 
            $table->string('descripcion_id');
            $table->foreign('descripcion_id')->references('iddescripcion')->on('medical_diccionaries');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('histories');
    }
}
