<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('last_name');
			$table->string('username');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('telefono');
			$table->string('genero');
			$table->string('path');
			$table->string('portada');
			$table->rememberToken();
			$table->timestamps();
			$table->integer('hospital_id')->unsigned();
			$table->foreign('hospital_id')->references('id')->on('hospitals');	
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
