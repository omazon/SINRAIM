<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodatcAtcs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atcs', function (Blueprint $table) {
             $table->string('codatc')->after('idatc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('atcs', function (Blueprint $table) {
             $table->dropColumn('codatc');
        });
    }
}
