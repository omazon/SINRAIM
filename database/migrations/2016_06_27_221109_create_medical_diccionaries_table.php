<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalDiccionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_diccionaries', function (Blueprint $table) {          
            $table->engine = 'InnoDB';
            $table->string('iddescripcion')->index();             
            $table->primary('iddescripcion');            
            $table->string('descripcion');
            $table->string('idcategoria');  
            $table->string('categoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_diccionaries');
    }
}
