<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdverseReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverse_reactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fechainicio');
            $table->date('fechafin');
            $table->string('desenlace');
            $table->string('intensidad');
            $table->integer('notificacion_id')->unsigned();
            $table->foreign('notificacion_id')->references('id')->on('notifications'); 
            $table->string('descripcion_id');
            $table->foreign('descripcion_id')->references('iddescripcion')->on('medical_diccionaries'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adverse_reactions');
    }
}
