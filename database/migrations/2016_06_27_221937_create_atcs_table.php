<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atcs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('idatc')->index();             
            $table->primary('idatc');  
            $table->string('descripcion');
            $table->string('viadministracion');
            $table->string('principioactivo',300);
            $table->string('unidadmedida');
            $table->string('unidadreferencia');
            $table->string('formafarmaceutica');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('atcs');
    }
}
