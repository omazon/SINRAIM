<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMedicalSupplies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medical_supplies', function (Blueprint $table) {            
            $table->integer('viadministracion_id')->unsigned();
            $table->foreign('viadministracion_id')->references('id')->on('via_administracions'); 
            $table->integer('unidadmedida_id')->unsigned();
            $table->foreign('unidadmedida_id')->references('id')->on('unidad_medidas'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medical_supplies', function (Blueprint $table) {    
            $table->dropForeign('viadministracion_id'); 
            $table->dropForeign('unidadmedida_id');        
        });
    }
}
