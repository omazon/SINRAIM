<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicaLassagnasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medica_lassagnas', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('Pregunta 1');
            $table->tinyInteger('Pregunta 2');
            $table->tinyInteger('Pregunta 3');
            $table->tinyInteger('Pregunta 4');
            $table->tinyInteger('Pregunta 5');
            $table->tinyInteger('Pregunta 6');
            $table->tinyInteger('Pregunta 7');
            $table->tinyInteger('Pregunta 8');
            $table->tinyInteger('Pregunta 9');
            $table->tinyInteger('Pregunta 10');
            $table->tinyInteger('Resultado valor');
            $table->string('estadolassagna');
            $table->integer('medicalsupplie_id')->unsigned();
            $table->foreign('medicalsupplie_id')->references('id')->on('medical_supplies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medica_lassagnas');
    }
}
