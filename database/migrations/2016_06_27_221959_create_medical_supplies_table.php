<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_supplies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fabricante');
            $table->string('numlote');
            $table->longText('motivo');
            $table->integer('dosis');
            $table->date('fechavencimiento');
            $table->date('fechainicio');
            $table->date('fechafin');  
            $table->string('tipoinsumo');
            $table->string('frecuencia');          
            $table->string('atc_id');
            $table->foreign('atc_id')->references('idatc')->on('atcs');
            $table->integer('notificacion_id')->unsigned();
            $table->foreign('notificacion_id')->references('id')->on('notifications'); 
            /*$table->integer('viadministracion_id')->unsigned();
            $table->foreign('viadministracion_id')->references('id')->on('via_administracions'); 
            $table->integer('unidadmedida_id')->unsigned();
            $table->foreign('unidadmedida_id')->references('id')->on('unidad_medidas'); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_supplies');
    }
}
